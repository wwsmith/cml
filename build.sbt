
name := "Carnac Machine Learning toolkit"

version := "0.1"

libraryDependencies += "com.weiglewilczek.slf4s" % "slf4s_2.9.1" % "1.0.7"

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.6"

libraryDependencies += "gov.nist.math" % "jama" % "1.0.3"

libraryDependencies += "com.typesafe.akka" % "akka-actor_2.10" % "2.2.3"

unmanagedSourceDirectories in Compile += baseDirectory.value / "src" / "example" / "scala"
