
package cml.learn

import scala.io.Source
import scala.util.{Try,Success,Failure}

import java.io.{BufferedReader,FileReader}

import cml.learn.ensemble._
import cml.learn.io._
import cml.learn.ibl.distance.Weights
import cml.learn.tree._
import cml.search._

/*********************************************************************************************************************/

object Autos {
  def schema(): ExperienceSchema = {
    val source = TransformExperienceSource(List[Symbol](),Symbol("symboling"),
      new ArffExperienceSource(Source.fromFile("data/uci/autos/imports-85.arff")))
    source.schema
  }

  def trainTest(): (Vector[Experience],Vector[Experience]) = {
    val source = TransformExperienceSource(List[Symbol](),Symbol("symboling"),
      new ArffExperienceSource(Source.fromFile("data/uci/autos/imports-85.arff")))

    val random = new scala.util.Random(347890)
    val experiences = random.shuffle(source.toVector)
    experiences.splitAt((experiences.size * 0.75).toInt)
  }
}

/*********************************************************************************************************************/

object AutosKnn {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Autos.trainTest()

    AutosKnnTrainer(Autos.schema).train(trainExps) match {
      case Success(predictor) => {
        test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }

  def test(predictor: Predictor, experiences: Vector[Experience]): Unit = {
    var count = 0
    for (exp <- experiences) {
      val query = Query(exp) // default levelOfConfidence is 90, which is fine
      predictor.predict(query) match {
        case Success(pred) => //print(pred.toString)
        case Failure(error) => //print(error.toString)
      }
      count += 1
      if (count % 1000 == 0) {
        println("  at item %d".format(count))
      }
    }
  }
}

object AutosKnnTrainer {
  def apply(schema: ExperienceSchema): Trainer = {
    val featureWeights = Map[String,Float](
      "normalized-loses" -> 1.0f,
      "make" -> 0.5f,
      "fuel-type" -> 0.0f,
      "aspiration" -> 0.0f,
      "num-of-doors" -> 0.75f,
      "body-style" -> 0.75f,
      "drive-wheels" -> 0.5f,
      "engine-location" -> 0.5f,
      "wheel-base" -> 1.0f,
      "length" -> 0.25f,
      "width" -> 0.0f,
      "height" -> 1.0f,
      "curb-weight" -> 0.0f,
      "engine-type" -> 0.75f,
      "num-of-cylinders" -> 0.75f,
      "engine-size" -> 0.25f,
      "fuel-system" -> 0.5f,
      "bore" -> 0.75f,
      "stroke" -> 0.0f,
      "compression-ratio" -> 0.75f,
      "horsepower" -> 0.0f,
      "peak-rpm" -> 0.0f,
      "city-mpg" -> 0.0f,
      "highway-mpg" -> 0.25f,
      "price" -> 0.0f)
    val numNeighbors = 30
    val kernelWidth = 0.3f
    new AutosKnnTrainer(schema,featureWeights,numNeighbors,kernelWidth)
  }
}

class AutosKnnTrainer(schema: ExperienceSchema,
                      val featureWeights: Map[String,Float],
                      val numNeighbors: Int,
                      val kernelWidth: Float) extends cml.learn.Trainer("autos-knn",schema) {
  def train(experiences: Vector[Experience]): Try[Predictor] = {
    val scale = cml.learn.ibl.distance.ExperiencesScaleFactor(experiences)
    val distCalc = new cml.learn.ibl.distance.HeomDistanceCalculator(schema,Weights(featureWeights,schema),scale)
    val predictor = new cml.learn.ibl.NearestNeighborsPredictor("autos-knn",
      new cml.learn.ibl.exb.BasicExperienceBase(distCalc),
      distCalc,
      numNeighbors,
      new cml.learn.ibl.kernel.GaussianKernel(1.0f,kernelWidth,false))

    for (exp <- experiences) {
      predictor.insert(exp)
    }

    Success(predictor)
  }
}

/*********************************************************************************************************************/

object AutosKnnValidate {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Autos.trainTest()

    val validator = new cml.learn.CrossValidate()
    val trainer = AutosKnnTrainer(Autos.schema)

    val stats = validator.validate(trainer,trainExps,10)
    print(stats)

    trainer.train(trainExps) match {
      case Success(predictor) => {
        test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }

  def test(predictor: Predictor, experiences: Vector[Experience]): Unit = {
    var count = 0
    for (exp <- experiences) {
      val query = Query(exp) // default levelOfConfidence is 90, which is fine
      predictor.predict(query) match {
        case Success(pred) => //print(pred.toString)
        case Failure(error) => //print(error.toString)
      }
      count += 1
      if (count % 1000 == 0) {
        println("  at item %d".format(count))
      }
    }
  }
}

/*********************************************************************************************************************/

class AutosKnnIndividual(val schema: ExperienceSchema,
                         increments: Vector[Int],
                         properties: Vector[SearchProperty]) extends PropertiesIndividual(increments,properties) {

  var stats: PredictionStatistics = null

  def trainer: Trainer = {
    val weights = properties.drop(2).map(prop => (prop.name,getFloat(prop.name))).toMap
    new AutosKnnTrainer(schema,weights,getInteger("numNeighbors"),getFloat("kernelWidth"))
  }
}

object AutosKnnIndividualFactory {
  def apply(schema: ExperienceSchema): AutosKnnIndividualFactory = {

    var properties = Vector[SearchProperty]()
    properties = properties :+ new IntegerSearchProperty("numNeighbors",10,100,10)
    properties = properties :+ new FloatSearchProperty("kernelWidth",0.1f,1.0f,0.1f)

    for (feature <- schema.features.dropRight(1)) {
      properties = properties :+ new FloatSearchProperty(feature.name.name,0.0f,1.0f,0.25f)
    }

    new AutosKnnIndividualFactory(schema,properties)
  }
}

class AutosKnnIndividualFactory(val schema: ExperienceSchema, properties: Vector[SearchProperty])
    extends PropertiesIndividualFactory[AutosKnnIndividual](properties) {

  protected def create(increments: Vector[Int]): AutosKnnIndividual = {
    new AutosKnnIndividual(schema,increments,properties)
  }
}

class AutosKnnScoreCalculator(val experiences: Vector[Experience]) extends ScoreCalculator[AutosKnnIndividual] {
  def calculate(individuals: Vector[AutosKnnIndividual]): Unit = {
    for (individual <- individuals) {
      val validator = new cml.learn.CrossValidate()
      individual.stats = validator.validate(individual.trainer,experiences,10)
      individual.score = individual.stats match {
        case stats: RegressionStatistics => stats.rootMeanSquaredError
        case _ => throw new PredictException("expected RegressionStatistics")
      }
    }
  }
}

object AutosKnnSearch {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Autos.trainTest()

    val start = new java.util.Date()
    val search = new RandomSearchMinimum[AutosKnnIndividual](AutosKnnIndividualFactory(Autos.schema),
                                                             new AutosKnnScoreCalculator(trainExps),
                                                             1000)
    val best = search.search()
    val end = new java.util.Date()
    println("searched in %d seconds".format((end.getTime() - start.getTime()) / 1000))
    print(best.stats)

    best.trainer.train(trainExps) match {
      case Success(predictor) => {
        test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }

  def test(predictor: Predictor, experiences: Vector[Experience]): Unit = {
    var count = 0
    for (exp <- experiences) {
      val query = Query(exp) // default levelOfConfidence is 90, which is fine
      predictor.predict(query) match {
        case Success(pred) => //print(pred.toString)
        case Failure(error) => //print(error.toString)
      }
      count += 1
      if (count % 1000 == 0) {
        println("  at item %d".format(count))
      }
    }
  }
}

/*********************************************************************************************************************/

object AutosTree {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Autos.trainTest()

    val trainer = new AutosTreeTrainer(Autos.schema)
    trainer.train(trainExps) match {
      case Success(predictor) => {
        test(predictor,testExps)
        print(predictor)
        print(predictor.stats)

        predictor match {
          case fi: cml.learn.feature.FeatureImportance => fi.printImportance(Autos.schema)
          case _ =>
        }
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }

  def test(predictor: Predictor, experiences: Vector[Experience]): Unit = {
    val start = new java.util.Date()
    var count = 0
    for (exp <- experiences) {
      val query = Query(exp) // default levelOfConfidence is 90, which is fine
      predictor.predict(query) match {
        case Success(pred) => //print(pred.toString)
        case Failure(error) => //print(error.toString)
      }
      count += 1
      if (count % 1000 == 0) {
        println("  at item %d".format(count))
      }
    }
    val end = new java.util.Date()
    println("predicted in %d seconds".format((end.getTime() - start.getTime()) / 1000))
  }
}

class AutosTreeTrainer(schema: ExperienceSchema) extends cml.learn.Trainer("autos-tree",schema) {
  def train(experiences: Vector[Experience]): Try[Predictor] = {
    val start = new java.util.Date()

    val loss = new cml.learn.SquaredErrorLoss()
    val maxDepth=Int.MaxValue
    val minLossFraction=0.01f
    val minNodeSize=5
    val builder = new BinaryCategoryBuilder(loss,maxDepth,minLossFraction,minNodeSize,10000,schema)

    val pruneFraction = 0.0f
    val root = builder.construct(experiences,pruneFraction)
    val end = new java.util.Date()
    println("trained in %d seconds".format((end.getTime() - start.getTime()) / 1000))

    Success(new TreePredictor("autos-tree",schema,root))
  }
}

/*********************************************************************************************************************/

object AutosTreeValidate {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Autos.trainTest()

    val validator = new cml.learn.CrossValidate()
    val trainer = new AutosTreeTrainer(Autos.schema)

    val stats = validator.validate(trainer,trainExps,10)
    print(stats)

    trainer.train(trainExps) match {
      case Success(predictor) => {
        test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }

  def test(predictor: Predictor, experiences: Vector[Experience]): Unit = {
    var count = 0
    for (exp <- experiences) {
      val query = Query(exp) // default levelOfConfidence is 90, which is fine
      predictor.predict(query) match {
        case Success(pred) => //print(pred.toString)
        case Failure(error) => //print(error.toString)
      }
      count += 1
      if (count % 1000 == 0) {
        println("  at item %d".format(count))
      }
    }
  }
}

/*********************************************************************************************************************/

object AutosGradientBoost {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Autos.trainTest()
    val schema = Autos.schema
    val loss = new cml.learn.SquaredErrorLoss()

    val trainer = new GradientBoostTrainer("autos-gboost",
                                           schema,
                                           0.1f,
                                           200,
                                           1.0f,
                                           loss,
                                           new AutosGradientBoostTreeTrainer(schema))

    trainer.train(trainExps) match {
      case Success(predictor) => {
        test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }

  def test(predictor: Predictor, experiences: Vector[Experience]): Unit = {
    val start = new java.util.Date()
    var count = 0
    for (exp <- experiences) {
      val query = Query(exp) // default levelOfConfidence is 90, which is fine
      predictor.predict(query) match {
        case Success(pred) => //print(pred.toString)
        case Failure(error) => //print(error.toString)
      }
      count += 1
      if (count % 1000 == 0) {
        println("  at item %d".format(count))
      }
    }
    val end = new java.util.Date()
    println("predicted in %d seconds".format((end.getTime() - start.getTime()) / 1000))
  }
}

class AutosGradientBoostTreeTrainer(schema: ExperienceSchema) extends cml.learn.Trainer("autos-gboost-tree",schema) {
  def train(experiences: Vector[Experience]): Try[Predictor] = {
    val start = new java.util.Date()

    val loss = new cml.learn.SquaredErrorLoss()
    val maxDepth=3
    val minLossFraction=0.01f
    val minNodeSize=3
    val builder = new BinaryCategoryBuilder(loss,maxDepth,minLossFraction,minNodeSize,10000,schema)

    val pruneFraction = 0.0f
    val root = builder.construct(experiences,pruneFraction)
    val end = new java.util.Date()
    println("trained in %d seconds".format((end.getTime() - start.getTime()) / 1000))

    Success(new TreePredictor("autos-tree",schema,root))
  }
}

/*********************************************************************************************************************/
