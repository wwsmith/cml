
package cml.learn

import scala.io.Source
import scala.util.{Try,Success,Failure}

import java.io.{BufferedReader,FileReader}

import cml.learn.io._
import cml.learn.ibl.distance.Weights
import cml.search._

/*********************************************************************************************************************/

object Iris {
  def schema(): ExperienceSchema = {
    val source = new ArffExperienceSource(Source.fromFile("data/uci/iris/iris.arff"))
    source.schema
  }

  def trainTest(): (Vector[Experience],Vector[Experience]) = {
    val source = new ArffExperienceSource(Source.fromFile("data/uci/iris/iris.arff"))

    val random = new scala.util.Random(347890)
    val experiences = random.shuffle(source.toVector)
    experiences.splitAt((experiences.size * 0.75).toInt)
  }

  def test(predictor: Predictor, experiences: Vector[Experience]): Unit = {
    var count = 0
    for (exp <- experiences) {
      val query = Query(exp)
      predictor.predict(query) match {
        case Success(pred) => //print(pred.toString)
        case Failure(error) => //print(error.toString)
      }
      count += 1
      if (count % 1000 == 0) {
        println("  at item %d".format(count))
      }
    }
  }
}

/*********************************************************************************************************************/

object IrisKnn {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Iris.trainTest()

    IrisKnnTrainer(Iris.schema).train(trainExps) match {
      case Success(predictor) => {
        Iris.test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }
}

object IrisKnnTrainer {
  def apply(schema: ExperienceSchema): Trainer = {
    val featureWeights = Map[String,Float](
      "sepal_length" -> 0.5f,
      "sepal_width" -> 0.5f,
      "petal_length" -> 0.5f,
      "petal_width" -> 0.5f)
    val numNeighbors = 30
    val kernelWidth = 0.3f
    new IrisKnnTrainer(schema,featureWeights,numNeighbors,kernelWidth)
  }
}

class IrisKnnTrainer(schema: ExperienceSchema,
                      val featureWeights: Map[String,Float],
                      val numNeighbors: Int,
                      val kernelWidth: Float) extends cml.learn.Trainer("iris-knn",schema) {
  def train(experiences: Vector[Experience]): Try[Predictor] = {
    val scale = cml.learn.ibl.distance.ExperiencesScaleFactor(experiences)
    val distCalc = new cml.learn.ibl.distance.HeomDistanceCalculator(schema,Weights(featureWeights,schema),scale)
    val predictor = new cml.learn.ibl.NearestNeighborsPredictor("iris-knn",
      new cml.learn.ibl.exb.BasicExperienceBase(distCalc),
      distCalc,
      numNeighbors,
      new cml.learn.ibl.kernel.GaussianKernel(1.0f,kernelWidth,false))

    for (exp <- experiences) {
      predictor.insert(exp)
    }

    Success(predictor)
  }
}

/*********************************************************************************************************************/

object IrisKnnValidate {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Iris.trainTest()

    val validator = new cml.learn.CrossValidate()
    val trainer = IrisKnnTrainer(Iris.schema)

    val stats = validator.validate(trainer,trainExps,10)
    print(stats)

    trainer.train(trainExps) match {
      case Success(predictor) => {
        Iris.test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }
}

/*********************************************************************************************************************/

class IrisKnnIndividual(val schema: ExperienceSchema,
                        increments: Vector[Int],
                        properties: Vector[SearchProperty]) extends PropertiesIndividual(increments,properties) {

  var stats: PredictionStatistics = null

  def trainer: Trainer = {
    val weights = properties.drop(2).map(prop => (prop.name,getFloat(prop.name))).toMap
    new IrisKnnTrainer(schema,weights,getInteger("numNeighbors"),getFloat("kernelWidth"))
  }
}

object IrisKnnIndividualFactory {
  def apply(schema: ExperienceSchema): IrisKnnIndividualFactory = {

    var properties = Vector[SearchProperty]()
    properties = properties :+ new IntegerSearchProperty("numNeighbors",10,100,10)
    properties = properties :+ new FloatSearchProperty("kernelWidth",0.1f,1.0f,0.1f)

    for (feature <- schema.features.dropRight(1)) {
      properties = properties :+ new FloatSearchProperty(feature.name.name,0.0f,1.0f,0.25f)
    }

    new IrisKnnIndividualFactory(schema,properties)
  }
}

class IrisKnnIndividualFactory(val schema: ExperienceSchema, properties: Vector[SearchProperty])
    extends PropertiesIndividualFactory[IrisKnnIndividual](properties) {

  protected def create(increments: Vector[Int]): IrisKnnIndividual = {
    new IrisKnnIndividual(schema,increments,properties)
  }
}

class IrisKnnScoreCalculator(val experiences: Vector[Experience]) extends ScoreCalculator[IrisKnnIndividual] {
  def calculate(individuals: Vector[IrisKnnIndividual]): Unit = {
    for (individual <- individuals) {
      val validator = new cml.learn.CrossValidate()
      individual.stats = validator.validate(individual.trainer,experiences,10)
      individual.score = individual.stats match {
        case stats: ClassificationStatistics => stats.percentCorrect
        case _ => throw new PredictException("expected ClassificationStatistics")
      }
    }
  }
}

object IrisKnnSearch {
  def main(args: Array[String]): Unit = {
    val (trainExps, testExps) = Iris.trainTest()

    val start = new java.util.Date()
    val search = new RandomSearchMaximum[IrisKnnIndividual](IrisKnnIndividualFactory(Iris.schema),
                                                            new IrisKnnScoreCalculator(trainExps),
                                                            1000)
    val best = search.search()
    val end = new java.util.Date()
    println("searched in %d seconds".format((end.getTime() - start.getTime()) / 1000))
    print(best.stats)

    best.trainer.train(trainExps) match {
      case Success(predictor) => {
        Iris.test(predictor,testExps)
        print(predictor)
        print(predictor.stats)
      }
      case Failure(e) => println("failed to train predictor: %s".format(e))
    }
  }
}

/*********************************************************************************************************************/
