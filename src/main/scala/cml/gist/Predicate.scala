/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.gist

/*********************************************************************************************************************/

// Predicates used in the Pages of a tree must all be of the same size
abstract class Predicate {
  def numBytes(): Int
  def toBytes(): Array[Byte]
  def fromBytes(bytes: Array[Byte]): Unit
}

/*********************************************************************************************************************/

class UnsatisfiablePredicate extends Predicate {
  def numBytes(): Int = {
    0
  }

  def toBytes(): Array[Byte] = {
    Array[Byte](0)
  }

  def fromBytes(bytes: Array[Byte]): Unit = {
  }
}

/*********************************************************************************************************************/

