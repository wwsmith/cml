/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.gist

import scala.collection.immutable.HashMap

/*********************************************************************************************************************/

class MemoryGistStorage[P <: Predicate]() extends GistStorage[P] {

  var rootPage: Long = -1
  var curPageId: Long = 1
  var pages = HashMap[Long,Page[P]]()

  def rootPageId(): Long = {
    rootPage
  }

  def read(id: Long): Page[P] = {
    if (!pages.contains(id)) {
      throw new GistError("page "+id+" is unknown")
    }
    pages(id)
  }

  def write(page: Page[P]): Unit = {
    if (page.id == -1) {
      page.id = curPageId
      curPageId += 1
      //println("  MemoryGS created new page "+page.id)
    }
    pages += (page.id -> page)
    if (rootPage == -1) {
      rootPage = page.id
      //println("MemoryGS updating root page to be "+rootPage)
    }

    if (page.internal && page.entries.map(entry => (entry.id == rootPage)).fold(false)((b1,b2) => b1 || b2)) {
      rootPage = page.id
      //println("MemoryGS updating root page to be "+rootPage)
    }
  }

  def delete(id: Long): Unit = {
    pages -= id
  }

  def begin(): Unit = {
    // no op
  }

  def commit(): Unit = {
    // no op
  }

  def shutdown(): Unit = {
    // no op
  }

}
