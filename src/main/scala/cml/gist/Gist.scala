/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.gist

import scala.collection.immutable.SortedMap
import scala.collection.immutable.TreeMap

/*********************************************************************************************************************/

abstract class Gist[D <: Datum,
                    P <: Predicate,
                    Penalty <: Ordered[Penalty],
                    Distance <: Ordered[Distance]](val branchFactor: Int,
                                                   val dataStore: DataStorage[D],
                                                   val gistStore: GistStorage[P]) {


  // false if p1 AND p2 can't be true, true otherwise
  def consistent(p1: P, p2: P): Boolean

  // return Predicate is true for all entries
  def union(entries: Vector[P]): P

  //def penalty(entry1: Entry[P], entry2: Entry[P]): Penalty
  def penalty(p1: P, p2: P): Penalty

  // the shortest possible distance between p1 and p2
  def distance(p1: P, p2: P): Distance

  def pickSplit(entries: Vector[Entry[P]]): (Vector[Entry[P]], Vector[Entry[P]])

  def compress(entry: Entry[P]): Entry[P] = {
    entry
  }

  def decompress(entry: Entry[P]): Entry[P] = {
    entry
  }



  // datum is not yet in the store
  def insert(key: P, datum: D): Unit = {
    if (datum.id != -1) {
      throw new GistError("datum is already in Storage")
    }

    //store.begin()
    dataStore.write(datum)
    gistStore.rootPageId() match {
      case -1 => {
        val page = new Page[P](key,false)
        page.entries = page.entries :+ new Entry[P](key,datum.id)
        gistStore.write(page)
      }
      case id => {
        val predPage = insert(id,new Entry[P](key,datum.id))
        // don't care if the predicate has been updated
        if (predPage._2 != -1) {
          println("creating new root page")
          // a new root page is needed
          val p1 = gistStore.read(id)
          val p2 = gistStore.read(predPage._2)
          val entries = Vector[Entry[P]](new Entry[P](p1.predicate,p1.id),new Entry[P](p2.predicate,p2.id))
          val root = new Page[P](union(entries.map(e => e.key)),true)
          root.entries = entries
          gistStore.write(root)
          gistStore.write(p1)
          gistStore.write(p2)
        }
      }
    }
    //store.commit()
  }

  // assumes datum is in the store
  // returns true if predicate for the Page has changed
  // returns the id of any new child page created
  private def insert(pageId: Long, insEntry: Entry[P]): (Boolean, Long) = {
    val page = gistStore.read(pageId)
    page.internal match {
      case true => {
        val bestChildId = page.entries.map(e => (e.id,penalty(insEntry.key,e.key))).reduce((pair1,pair2) => {
          if (pair1._2 <= pair2._2) {
            pair1
          } else {
            pair2
          }
        })._1

        insert(bestChildId,insEntry) match {
          case (false,-1) => {
            (false,-1)
          }
          case (true,-1) => {
            (adjustPredicate(page),-1)
          }
          case (dontCare,newChildId) => {
            val childEntry = new Entry[P](gistStore.read(newChildId).predicate,newChildId)
            page.entries = page.entries :+ childEntry
            if (page.entries.length > branchFactor) {
              (true,split(page))
            } else {
              (adjustPredicate(page),-1)
            }
          }
        }
      }
      case false => {
        page.entries = page.entries :+ insEntry
        if (page.entries.length > branchFactor) {
          (true,split(page))
        } else {
          (adjustPredicate(page),-1)
        }
      }
    }
  }

  // this could probably be optimized a bit with a little more info (e.g. one predicate contains the other)
  private def adjustPredicate(page: Page[P]): Boolean = {
    val newPredicate = union(page.entries.map(e => e.key))
    if (newPredicate != page.predicate) {
      page.predicate = newPredicate
      gistStore.write(page)
      true
    } else {
      false
    }
  }

  private def split(page: Page[P]): Long = {
    //println("splitting page "+page.id+" with "+page.entries.length+" entries")
    val entriesPair = pickSplit(page.entries)
    //println("  split into "+entriesPair._1.length+" and "+entriesPair._2.length)

    page.predicate = union(entriesPair._1.map(e => e.key))
    page.entries = entriesPair._1

    var newPage = new Page[P](union(entriesPair._2.map(e => e.key)),page.internal)
    newPage.entries = entriesPair._2

    gistStore.write(page)
    gistStore.write(newPage)

    newPage.id
  }

  def delete(key: P): Int = {
    //store.begin()
    val data = delete(gistStore.rootPageId(),key)
    for (datum <- data) {
      dataStore.delete(datum.id)
    }
    //store.commit()

    data.length
  }

  // this is half assed
  def delete(pageId: Long, key: P): Vector[D] = {
    val page = gistStore.read(pageId)
    page.internal match {
      case true => {
        val e1: Vector[Entry[P]] = page.entries.filter(e => consistent(e.key,key))
        val e2: Vector[Vector[D]] = e1.map(e => delete(e.id,key))
        val e3: Vector[D] = e2.flatten

        page.entries.filter(e => consistent(e.key,key)).map(e => delete(e.id,key)).flatten
      }
      case false => {
        val toDelete = page.entries.filter(e => consistent(e.key,key)).map(e => dataStore.read(e.id))
        page.entries = page.entries.filter(e => !consistent(e.key,key))
        if (toDelete.length > 0) {
          if (!adjustPredicate(page)) {
            gistStore.write(page)
          }
        }
        toDelete
      }
    }
  }


  def search(query: P): Vector[D] = {
    println("this isn't tested")
    search(gistStore.rootPageId(),query)
  }

  private def search(pageId: Long, query: P): Vector[D] = {
    val page = gistStore.read(pageId)
    page.internal match {
      case true => {
        page.entries.filter(e => consistent(e.key,query)).map(e => search(e.id,query)).flatten
      }
      case false => {
        page.entries.filter(e => consistent(e.key,query)).map(e => dataStore.read(e.id))
      }
    }
  }

  def best(query: P, max: Int): SortedMap[Distance,D] = {
    println("this doesn't fully work")
    var b = TreeMap[Distance,D]()
    for ((dist,entry) <- best(gistStore.rootPageId(),query,max,TreeMap[Distance,Entry[P]]())) {
      b += (dist -> dataStore.read(entry.id))
    }
    b
  }

  private def best(pageId: Long,
                   query: P,
                   max: Int,
                   soFar: SortedMap[Distance,Entry[P]]): SortedMap[Distance,Entry[P]] = {
    val page = gistStore.read(pageId)
    page.internal match {
      case true => {
        var sf = soFar
        var children = TreeMap[Distance,Entry[P]]()
        for (e <- page.entries) {
          children += (distance(query,e.key) -> e)
        }
        for ((dist,entry) <- children) {
          sf = sf.lastOption match {
            case None => best(entry.id,query,max,sf)
            case Some((d,e)) => {
              if (distance(query,entry.key) < distance(query,e.key)) {
                best(entry.id,query,max,sf)
              } else {
                sf
              }
            }
          }
          //sf = best(entry.id,query,max,sf)
        }
        sf
      }
      case false => {
        (soFar ++ page.entries.map(e => (distance(query,e.key),e))).take(max)
      }
    }
  }

  // this is going to be slow!
  def size(): Int = {
    size(gistStore.rootPageId())
  }

  private def size(pageId: Long): Int = {
    pageId match {
      case -1 => 0
      case id => {
        val page = gistStore.read(id)
        page.internal match {
          case true => {
            page.entries.map(entry => size(entry.id)).fold(0)((size1,size2) => size1 + size2)
          }
          case false => {
            page.entries.length
          }
        }
      }
    }
  }

  def shutdown(): Unit = {
    //store.shutdown()
  }

}
