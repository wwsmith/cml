/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search

/*********************************************************************************************************************/

abstract class SearchProperty(val name: String) {
  def size(): Int

  def randomIncrement(): Int = {
    Random.nextInt(size())
  }

  def neighborIncrement(increment: Int): Int = {
    val neighbors = neighborIncrements(increment)
    neighbors(Random.nextInt(neighbors.length))
  }

  def neighborIncrements(increment: Int): Vector[Int] = {
    if (increment == 0) {
      Vector[Int](1)
    } else if (increment == size() - 1) {
      Vector[Int](size() - 2)
    } else {
      Vector[Int](increment-1,increment+1)
    }
  }

  def toString(indent: String, value: Int): String
}
