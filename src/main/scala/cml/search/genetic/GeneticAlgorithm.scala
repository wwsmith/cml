/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search.genetic

import cml.search._

/*********************************************************************************************************************/

class GeneticAlgorithm[T <: Individual](val factory: IndividualFactory[T],
                                        val calculator: ScoreCalculator[T],
                                        val selector: ParentSelector[T],
                                        val evaluator: GenerationEvaluator[T],
                                        val numPopulations: Int = 3,
                                        val populationSize: Int = 100,
                                        val eliteSize: Int = 2,
                                        val newSize: Int = 2,
                                        val migrationFraction: Float = 0.02f,
                                        val migrationInterval: Int = 2,
                                        val mutationProb: Float = 0.01f,
                                        val numBestToPrint: Int = 1) {

  def search(): T = {
    search(1, Vector.fill(numPopulations)(new Population[T](factory,
                                                            selector,
                                                            populationSize,
                                                            eliteSize,
                                                            newSize,
                                                            mutationProb)))
  }

  def search(generation: Int, populations: Vector[Population[T]]): T = {

    calculateFitness(populations)

    if (numBestToPrint > 0) {
      println("---------- best "+numBestToPrint+" in generation "+generation+" ----------");
    }
    for (ind <- getBestIndividuals(populations,numBestToPrint)) {
      print(ind)
    }

    if (evaluator.stopSearching(generation,getIndividuals(populations))) {
      getBestIndividuals(populations,1).head
    } else {
      search(generation+1,createNextGeneration(generation+1,populations))
    }
  }

  private def getIndividuals(populations: Vector[Population[T]]): Vector[T] = {
    val all = populations.foldLeft(Vector[T]())((vect,pop) => vect ++ pop.individuals)
    all.sortWith((ind1,ind2) => ind1.score > ind2.score)
  }

  private def getBestIndividuals(populations: Vector[Population[T]], max: Int): Vector[T] = {
    getIndividuals(populations).take(max)
  }

  private def calculateFitness(populations: Vector[Population[T]]): Unit = {
    calculator.calculate(getIndividuals(populations))
    for (population <- populations) {
      population.sortByFitness()
    }
  }

  private def createNextGeneration(generation: Int, populations: Vector[Population[T]]): Vector[Population[T]] = {
    val pops = shouldMigrate(generation) match {
      case true => migrate(populations)
      case false => populations
    }
    pops.map(pop => pop.createNextGeneration())
  }

  private def shouldMigrate(generation: Int): Boolean = {
    if (numPopulations == 1) {
      false
    } else if ((generation-1) % migrationInterval == 0) {
      true
    } else {
      false
    }
  }

  private def migrate(populations: Vector[Population[T]]): Vector[Population[T]] = {
    var numToMigrate = (migrationFraction * populationSize).toInt
    if ((migrationFraction > 0.0f) && (numToMigrate == 0)) {
      numToMigrate = 1
    }

    for (source <- 0 until populations.length) {
      val individualsToMigrate = populations(source).selectToMigrate(numToMigrate)
      for (individual <- individualsToMigrate) {
        val destination = Random.nextInt(populations.length-1) match {
          case dest if dest < source => dest
          case dest if dest >= source => dest + 1
        }
        populations(destination).add(individual)
      }
    }

    for (population <- populations) {
      population.sortByFitness()
      population.trimSize()
    }

    populations
  }
}
