/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search.genetic

import cml.search._

/*********************************************************************************************************************/

class Population[T <: Individual](val factory: IndividualFactory[T],
                                  val selector: ParentSelector[T],
                                  val populationSize: Int,
                                  val eliteSize: Int,
                                  val newSize: Int,
                                  val mutationProb: Float,
                                  var individuals: Vector[T]) {

  def this(factory: IndividualFactory[T],
           selector: ParentSelector[T],
           populationSize: Int,
           eliteSize: Int,
           newSize: Int,
           mutationProb: Float) = {
    
    this(factory,
         selector,
         populationSize,
         eliteSize,
         newSize,
         mutationProb,
         Vector.fill[T](populationSize)(factory.random()))
  }

  val migrationSelector = new StochasticSamplingWithReplacementSelector[T]()

  def selectToMigrate(count: Int): Vector[T] = {
    migrationSelector.selectVector(individuals,count)
  }

  def add(individual: T): Unit = {
    individuals = individuals :+ individual
  }

  /**
    Sorts by decreasing fitness
  */
  def sortByFitness(): Unit = {
    individuals = individuals.sortWith((ind1,ind2) => ind1.score > ind2.score)
  }

  def trimSize(): Unit = {
    individuals = individuals.take(populationSize)
  }

  def createNextGeneration(): Population[T] = {

    var newIndividuals = Vector[T]()

    // save the elite individuals
    for (i <- 0 until eliteSize) {
      newIndividuals = newIndividuals :+ individuals(i)
    }

    // pick parents and do crossover
    var pairs = selector.select(individuals, populationSize-eliteSize-newSize)
    for ((parent1,parent2) <- pairs) {
      val pair = factory.cross(parent1,parent2)
      newIndividuals = newIndividuals :+ pair._1
      newIndividuals = newIndividuals :+ pair._2
    }

    // mutate
    for (i <- eliteSize until newIndividuals.length) {
      newIndividuals.updated(i,factory.mutate(newIndividuals(i),mutationProb))
    }

    // add new individuals
    for (i <- 0 until newSize) {
      newIndividuals = newIndividuals :+ factory.random()
    }

    new Population[T](factory,
                      selector,
                      populationSize,
                      eliteSize,
                      newSize,
                      mutationProb,
                      newIndividuals)
  }

  override def toString(): String = {
    val str = individuals.map(ind => ind.toString()).foldLeft("")((s1,s2) => s1 + s2)
    "population:\n"+str.split("\n").mkString("  ","\n  ","\n")
  }
}
