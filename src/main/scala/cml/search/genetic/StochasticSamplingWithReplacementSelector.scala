/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search.genetic

import cml.search._

/*********************************************************************************************************************/

class StochasticSamplingWithReplacementSelector[T <: Individual] extends ParentSelector[T] {

  def select(individuals: Vector[T], size: Int): Vector[Tuple2[T,T]] = {
    var totalFitness = individuals.foldLeft(0.0f)((fitness,ind) => fitness + ind.score)

    var sumFitness = Vector[(Float,T)]()
    var curFitness = 0.0f
    for (ind <- individuals) {
      curFitness += ind.score
      sumFitness = sumFitness :+ (curFitness,ind)
    }

    var parents = Vector[Tuple2[T,T]]()
    for (i <- 0 until size/2) {
      val selected1 = Random.nextFloat()*totalFitness
      val parent1 = sumFitness.filter(pair => selected1 < pair._1).head._2
      val selected2 = Random.nextFloat()*totalFitness
      val parent2 = sumFitness.filter(pair => selected2 < pair._1).head._2
      parents = parents :+ (parent1,parent2)
    }
    parents
  }
}
