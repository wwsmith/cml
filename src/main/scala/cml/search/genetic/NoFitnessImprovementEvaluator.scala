/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search.genetic

import cml.search._

/*********************************************************************************************************************/

class NoFitnessImprovementEvaluator[T <: Individual](val maxNumGen: Int = 2) extends GenerationEvaluator[T] {
  var numGen: Int = 0
  var bestFitness: Float = 0.0f

  def stopSearching(generation: Int, individuals: Vector[T]): Boolean = {
    if (individuals.head.score > bestFitness) {
      bestFitness = individuals.head.score
      numGen = 0
    } else {
      numGen += 1
    }

    if (numGen > maxNumGen) {
      true
    } else {
      false
    }
  }
}
