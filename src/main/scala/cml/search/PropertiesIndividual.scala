/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search

/*********************************************************************************************************************/

class PropertiesIndividual(val increments: Vector[Int],
                           val properties: Vector[SearchProperty]) extends Individual {

  if (increments.length != properties.length) {
    throw new SearchException("increments not of same length as properties")
  }

  def getBoolean(name: String): Boolean = {
    val pair = getPropAndInc(name)
    pair._1 match {
      case prop: BooleanSearchProperty => prop.value(pair._2)
      case _ => throw new SearchException("property "+name+" is not a BooleanSearchProperty")
    }
    //pair._1.asInstanceOf[BooleanSearchProperty].value(pair._2)
  }

  def getChoice(name: String): String = {
    val pair = getPropAndInc(name)
    pair._1 match {
      case prop: ChoiceSearchProperty => prop.value(pair._2)
      case _ => throw new SearchException("property "+name+" is not a ChoiceSearchProperty")
    }
    //pair._1.asInstanceOf[ChoiceSearchProperty].value(pair._2)
  }

  def getInteger(name: String): Int = {
    val pair = getPropAndInc(name)
    pair._1 match {
      case prop: IntegerSearchProperty => prop.value(pair._2)
      case _ => throw new SearchException("property "+name+" is not a IntegerSearchProperty")
    }
    //pair._1.asInstanceOf[IntegerSearchProperty].value(pair._2)
  }

  def getFloat(name: String): Float = {
    val pair = getPropAndInc(name)
    pair._1 match {
      case prop: FloatSearchProperty => prop.value(pair._2)
      case _ => throw new SearchException("property "+name+" is not a FloatSearchProperty")
    }
    //pair._1.asInstanceOf[FloatSearchProperty].value(pair._2)
  }

  private def getPropAndInc(name: String): Tuple2[SearchProperty,Int] = {
    val namedPropIncs = properties.zip(increments).filter(propInc => propInc._1.name == name)
    if (namedPropIncs.length != 1) {
      throw new SearchException("found "+namedPropIncs.length+" search properties named "+name)
    }
    namedPropIncs.head
  }

  override def toString(): String = {
    var istr = "PropertiesIndividual with score "+score+":\n"
    for (i <- 0 until properties.length) {
      istr += properties(i).toString("  ",increments(i))
    }
    istr
  }
}
