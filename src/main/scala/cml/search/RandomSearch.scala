/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search

import com.weiglewilczek.slf4s._

import cml.search._

/*********************************************************************************************************************/

class RandomSearchMinimum[T <: Individual](val factory: IndividualFactory[T],
                                           val calculator: ScoreCalculator[T],
                                           val count: Int,
                                           val random: scala.util.Random) extends Logging {

  def this(factory: IndividualFactory[T], calculator: ScoreCalculator[T], count: Int) = 
    this(factory,calculator,count,new scala.util.Random(389923))

  def search(): T = {
    // don't worry about repeats for now
    val individuals = (0 until count).toVector.map(i => factory.random())
    logger.info("evaluating %d individuals".format(individuals.size))
    calculator.calculate(individuals)
    individuals.minBy(ind => ind.score)
  }
}

/*********************************************************************************************************************/

class RandomSearchMaximum[T <: Individual](val factory: IndividualFactory[T],
                                           val calculator: ScoreCalculator[T],
                                           val count: Int,
                                           val random: scala.util.Random) extends Logging {

  def this(factory: IndividualFactory[T], calculator: ScoreCalculator[T], count: Int) = 
    this(factory,calculator,count,new scala.util.Random(389923))

  def search(): T = {
    // don't worry about repeats for now
    val individuals = (0 until count).toVector.map(i => factory.random())
    logger.info("evaluating %d individuals".format(individuals.size))
    calculator.calculate(individuals)
    individuals.maxBy(ind => ind.score)
  }
}

/*********************************************************************************************************************/
