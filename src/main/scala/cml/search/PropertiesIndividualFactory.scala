/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search

/*********************************************************************************************************************/

abstract class PropertiesIndividualFactory[T <: PropertiesIndividual](val properties: Vector[SearchProperty])
extends IndividualFactory[T] {

  protected def create(increments: Vector[Int]): T

  def random(): T = {
    var increments = Vector[Int]()
    for (prop <- properties) {
      increments = increments :+ prop.randomIncrement()
    }
    create(increments)
  }

  def cross(ind1: T, ind2: T): (T, T) = {
    val count = 1 + Random.nextInt(properties.length - 1)
    val incs1 = ind1.increments.take(count) ++ ind2.increments.takeRight(properties.length-count)
    val incs2 = ind2.increments.take(properties.length-count) ++ ind1.increments.takeRight(count)
    (create(incs1),create(incs2))
  }

  def mutate(ind: T, mutationProb: Float): T = {
    val incs = properties.zip(ind.increments).map(propInc =>
      if (Random.nextFloat() < mutationProb) {
        propInc._1.randomIncrement()
      } else {
        propInc._2
      })
    create(incs)
  }

  def neighbor(ind: T): T = {
    val index = Random.nextInt(properties.length)
    val inc = properties(index).neighborIncrement(ind.increments(index))
    create(ind.increments.updated(index,inc))
  }

  def neighbors(ind: T): Vector[T] = {
    //val neighborIncrements = properties.zip(ind.increments).map(propInc => propInc._1.neighborIncrements(propInc._2))
    val neighborIncrements = properties.zip(ind.increments).map({case (property,increment) =>
      property.neighborIncrements(increment)})
    val combs = combinations(neighborIncrements)
    combs.map(comb => create(comb))
  }


  private def combinations(combs: Vector[Vector[Int]]): Vector[Vector[Int]] = {
    println("combination of "+combs.size)
    combs.size match {
      case 0 => Vector[Vector[Int]]()
      case _ => {
        val prefix = combs.head
        val suffixes = combinations(combs.drop(1))
        println("  prefix with %d, %d suffixes".format(prefix.size,suffixes.size))
        val c= suffixes.size match {
          case 0 => prefix.map(p => Vector[Int](p))
          case _ => prefix.map(p => suffixes.map(s => p +: s)).reduce((v1,v2) => v1 ++ v2)
        }
        println("    returning %d".format(c.size))
        c
      }
    }
  }

/*
  private def combinations(combs: Vector[Vector[Int]],
                           prefix: Vector[Int] = Vector[Int]()): Vector[Vector[Int]] = {
    println(combs.size+" "+prefix.size)
    if (combs.size == 0) {
      Vector[Vector[Int]]()
    } else {
      //print("combinations: "+combs)
      //print("prefix: "+prefix)
      val vvv = combs.head.map(increment => combinations(combs.drop(1),prefix :+ increment))
      vvv.foldLeft(Vector[Vector[Int]]())((v1,v2) => v1 ++ v2)
    }
  }
 */

  def neighbors(ind: T, maxNeighbors: Int): Vector[T] = {
    var remaining = neighbors(ind)
    if (remaining.length <= maxNeighbors) {
      remaining
    } else {
      var returning = Vector[T]()
      for (i <- 0 until maxNeighbors) {
        val index = Random.nextInt(remaining.length)
        returning = returning :+ remaining(index)
        remaining = remaining.take(index) ++ remaining.drop(index+1)
      }
      returning
    }
  }

  def all(): Vector[T] = {
    for (property <- properties) {
      println("  %d values for property %s".format(property.size,property.name))
    }
    println("search size: "+properties.foldLeft(1)((count,prop) => count * prop.size))

    val combs = combinations(properties.map(property => (0 until property.size()).toVector))
    val all = combs.map(comb => create(comb))

    println("all has size: "+all.size)

    all
  }

}
