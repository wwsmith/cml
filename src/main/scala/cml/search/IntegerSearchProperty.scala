/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.search

/*********************************************************************************************************************/

class IntegerSearchProperty(name: String,
                            val min: Int, val max: Int, val increment: Int) extends SearchProperty(name) {
  def size(): Int = {
    (max - min) / increment + 1
  }

  def value(increments: Int): Int = {
    min + increment * increments
  }

  def toString(indent: String, increments: Int): String = {
    indent + name + ": " + value(increments) + "\n"
  }
}
