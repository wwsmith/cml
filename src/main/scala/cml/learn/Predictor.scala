/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

import scala.util.Try

/*********************************************************************************************************************/

/**
  Assumes that the last feature is the output
 */
abstract class Predictor(val name: String, val schema: ExperienceSchema) {
  var stats = PredictionStatistics(schema)

  def clearStats(): Unit = {
    stats = PredictionStatistics(schema)
  }

  def insert(exp: Experience): Unit

  def predict(query: Query): Try[Prediction]

  def shutdown(): Unit

  protected def createPrediction(query: Query, value: AnyVal, lower: AnyVal, upper: AnyVal): Prediction = {
    val output = schema.features.last match {
      case fs: IntFeatureSchema => Some(value.asInstanceOf[Number].intValue)
      case fs: LongFeatureSchema => Some(value.asInstanceOf[Number].longValue)
      case fs: FloatFeatureSchema => Some(value.asInstanceOf[Number].floatValue)
      case fs: DoubleFeatureSchema => Some(value.asInstanceOf[Number].doubleValue)
      case _ => None
    }
    val interval = schema.features.last match {
      case fs: IntFeatureSchema => new Interval(query.levelOfConfidence,
                                                lower.asInstanceOf[Number].intValue,
                                                upper.asInstanceOf[Number].intValue)
      case fs: LongFeatureSchema => new Interval(query.levelOfConfidence,
                                                 lower.asInstanceOf[Number].longValue,
                                                 upper.asInstanceOf[Number].longValue)
      case fs: FloatFeatureSchema => new Interval(query.levelOfConfidence,
                                                  lower.asInstanceOf[Number].floatValue,
                                                  upper.asInstanceOf[Number].floatValue)
      case fs: DoubleFeatureSchema => new Interval(query.levelOfConfidence,
                                                   lower.asInstanceOf[Number].doubleValue,
                                                   upper.asInstanceOf[Number].doubleValue)
      case _ => new Interval(query.levelOfConfidence,lower,upper)
    }
    new Prediction(query.dropRight(1) :+ output,None,Some(interval))
  }

  def createPrediction(query: Query, value: Symbol, confidence: Float): Prediction = {
    new Prediction(query.dropRight(1) :+ Some(value),
                   Some(confidence),
                   None)
  }

}

/*********************************************************************************************************************/
