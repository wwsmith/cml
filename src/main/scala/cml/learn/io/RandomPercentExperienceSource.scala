/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import scala.util.Random

import cml.learn.Experience
import cml.learn.ExperienceSchema

/*********************************************************************************************************************/

class RandomPercentExperienceSource(val seed: Long,
                                    val lowerPercent: Integer,
                                    val upperPercent: Integer,
                                    val source: ExperienceSource) extends ExperienceSource {

  def schema: ExperienceSchema = source.schema

  val random = new Random(seed)

  var nextExp: Option[Experience] = read()

  def hasNext: Boolean = {
    nextExp match {
      case None => false
      case Some(_) => true
    }
  }

  def next: Experience = {
    nextExp match {
      case None => throw new cml.learn.PredictException("no next experience")
      case Some(exp) => {
        nextExp = read()
        exp
      }
    }
  }

  protected def read(): Option[Experience] = {
    source.hasNext match {
      case false => None
      case true => {
        val exp = source.next
        val r = random.nextInt() % 100
        if ((r >= lowerPercent) && (r < upperPercent)) {
          Some(exp)
        } else {
          read()
        }
      }
    }
  }
}


/*********************************************************************************************************************/
