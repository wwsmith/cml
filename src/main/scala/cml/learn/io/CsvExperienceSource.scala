
package cml.learn.io

import scala.io.Source

import cml.learn._

/*********************************************************************************************************************/

/**
Reads experiences in a comma-separated value format.
*/
class CsfExperienceSource(val source: Source, val csvSchema: ExperienceSchema) extends ExperienceSource {

  val lineIterator = source.getLines()

  var nextExp: Option[Experience] = read()

  def schema: ExperienceSchema = csvSchema

  def hasNext: Boolean = {
    nextExp match {
      case None => false
      case Some(_) => true
    }
  }

  def next: Experience = {
    nextExp match {
      case None => throw new PredictException("no next experience")
      case Some(exp) => {
        nextExp = read()
        exp
      }
    }
  }

  protected def read(): Option[Experience] = {
    lineIterator.hasNext match {
      case false => None
      case true => {
        var line = lineIterator.next
        line = line.indexOf("#") match {
          case -1 => line
          case index: Int => line.substring(index)
        }

        val values = line.split(",")
        values.length match {
          case 0 => read()
          case len => {
            if (len == schema.features.length) {
              Some(getExperience(values))
            } else {
              throw new PredictException("invalid line: "+line)
            }
          }
        }
      }
    }
  }

  protected def getExperience(values: Array[String]): Experience = {
    val features = schema.features.zip(values).map({case (fschema,strValue) => strValue match {
      case "?" => None
      case str => Some(fschema.value(str))
    }})
    val exp = new Experience(features)
    schema.validate(exp)
    exp
  }

}

/*********************************************************************************************************************/
