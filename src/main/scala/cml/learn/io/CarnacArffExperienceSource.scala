/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import scala.io.Source

import cml.learn._

/*********************************************************************************************************************/

object CarnacArffExperienceSource {
  def main(args: Array[String]) = {
    if (args.length != 1) {
      println("usage: CarnacArffExperienceSource <trace file>")
      sys.exit(1)
    }
    val source = new CarnacArffExperienceSource(Source.fromFile(args(0)))
    while (source.hasNext) {
      val exp = source.next
      println(exp)
    }
  }

  def readSchema(lineIterator: ArffLineIterator): ExperienceSchema = {
    var attributes = Vector[FeatureSchema]()

    val s = new ExperienceSchema(ArffExperienceSource.readRelationName(lineIterator))
    var continue = true
    while (continue) {
      ArffExperienceSource.readAttributeLine(lineIterator) match {
        case None => continue = false
        case Some(line) => s.features = s.features :+ featureSchema(line)
      }
    }
    s
  }

  def featureSchema(line: String): FeatureSchema = {
    val noQuotesLine = line.replace("'","").replace("\"","")

    val optRegex = """@attribute_optional\s+(.*)\s*""".r
    val reqRegex = """@attribute_required\s+(.*)\s*""".r
    val attrRegex = """@attribute\s+(.*)\s*""".r

    val (required,attrDesc) = noQuotesLine match {
      case optRegex(attrDesc) => (false,attrDesc)
      case reqRegex(attrDesc) => (true,attrDesc)
      case attrRegex(attrDesc) => (false,attrDesc)
      case _ => throw new java.io.IOException("can't parse start of feature schema: "+line)
    }

    // arff ones
    val stringAttrRegex = """(\S+) string""".r
    val numericAttrRegex = """(\S+) numeric""".r
    val realAttrRegex = """(\S+) real""".r
    val intAttrRegex = """(\S+) integer""".r
    val catAttrRegex = """(\S+) \{([^\}]+)\}""".r
    val dateAttrRegex = """(\S+) date (\S+)""".r

    // carnac extensions
    val longAttrRegex = """(\S+) long""".r
    val floatAttrRegex = """(\S+) float""".r
    val doubleAttrRegex = """(\S+) double""".r
    val booleanAttrRegex = """(\S+) boolean""".r

    attrDesc match {
      case stringAttrRegex(name) => new StringFeatureSchema(Symbol(name),required)
      case numericAttrRegex(name) => new FloatFeatureSchema(Symbol(name),required)
      case realAttrRegex(name) => new FloatFeatureSchema(Symbol(name),required)
      case intAttrRegex(name) => new IntFeatureSchema(Symbol(name),required)
      case catAttrRegex(name,cats) => {
        var categories = Set[Symbol]()
        for (categoryStr <- cats.split(",")) {
          categories = categories + Symbol(categoryStr.trim)
        }
        new CategoryFeatureSchema(Symbol(name),required,categories)
      }
      case dateAttrRegex(name,format) => new DateFeatureSchema(Symbol(name),required,format)
      case longAttrRegex(name) => new LongFeatureSchema(Symbol(name),required)
      case floatAttrRegex(name) => new FloatFeatureSchema(Symbol(name),required)
      case doubleAttrRegex(name) => new DoubleFeatureSchema(Symbol(name),required)
      case booleanAttrRegex(name) => new BooleanFeatureSchema(Symbol(name),required)
      case _ => throw new java.io.IOException("can't parse feature schema: "+line)
    }      
  }
}

/*********************************************************************************************************************/

/**
Carnac supports an extended version of the Weka Attribute-Relation File Fomat. The extensions are additional
data types and identifying attributes as required or optional.
*/
class CarnacArffExperienceSource(source: Source) extends ArffExperienceSource(source) {

  override def readSchema(lineIterator: ArffLineIterator) = CarnacArffExperienceSource.readSchema(lineIterator)

  /*
  override protected def featureSchema(line: String): FeatureSchema = {
    CarnacArffExperienceSource.featureSchema(line)
  }
   */
}

/*********************************************************************************************************************/
