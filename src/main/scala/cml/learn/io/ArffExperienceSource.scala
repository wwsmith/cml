/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import scala.io.Source

import cml.learn._

/*********************************************************************************************************************/

object ArffExperienceSource {
  def main(args: Array[String]) = {
    if (args.length != 1) {
      println("usage: ArffExperienceSource <trace file>")
      sys.exit(1)
    }
    val source = new ArffExperienceSource(Source.fromFile(args(0)))
    while (source.hasNext) {
      val exp = source.next
      println(exp)
    }
  }

  def readSchema(lineIterator: ArffLineIterator): ExperienceSchema = {
    var attributes = Vector[FeatureSchema]()

    val s = new ExperienceSchema(ArffExperienceSource.readRelationName(lineIterator))
    var continue = true
    while (continue) {
      ArffExperienceSource.readAttributeLine(lineIterator) match {
        case None => continue = false
        case Some(line) => s.features = s.features :+ featureSchema(line)
      }
    }
    s
  }

  protected def featureSchema(line: String): FeatureSchema = {
    val required = false   // not provided in arff

    val stringAttrRegex = """@attribute (\S+) string\s*""".r
    val numericAttrRegex = """@attribute (\S+) numeric\s*""".r
    val realAttrRegex = """@attribute (\S+) real\s*""".r
    val intAttrRegex = """@attribute (\S+) integer\s*""".r
    val catAttrRegex = """@attribute (\S+) \{([^\}]+)\}\s*""".r
    val dateAttrRegex = """@attribute (\S+) date (\S+)\s*""".r

    val noQuotesLine = line.replace("'","").replace("\"","")
    noQuotesLine match {
      case stringAttrRegex(name) => new StringFeatureSchema(Symbol(name),required)
      case numericAttrRegex(name) => new FloatFeatureSchema(Symbol(name),required)
      case realAttrRegex(name) => new FloatFeatureSchema(Symbol(name),required)
      case intAttrRegex(name) => new IntFeatureSchema(Symbol(name),required)
      case catAttrRegex(name,cats) => {
        var categories = Set[Symbol]()
        for (categoryStr <- cats.split(",")) {
          categories = categories + Symbol(categoryStr.trim)
        }
        new CategoryFeatureSchema(Symbol(name),required,categories)
      }
      case dateAttrRegex(name,format) => new DateFeatureSchema(Symbol(name),required,format)
      case _ => throw new java.io.IOException("can't parse feature schema: "+line)
    }      
  }

  def readRelationName(lineIterator: ArffLineIterator): String = {
    val relationRegex = """@(?i)relation\s+(\S+)\s*""".r
    lineIterator.next match {
      case relationRegex(name) => name
      case _ => throw new PredictException("didn't find @relation")
    }
  }

  def readAttributeLine(lineIterator: ArffLineIterator): Option[String] = {
    val attributeRegex = """@(?i)attribute.*""".r
    val line = lineIterator.next
    line match {
      case attributeRegex() => Some(line)
      case _ => None
    }
  }

}

/*********************************************************************************************************************/

/**
Reads experiences in the Weka Attribute-Relation File Format.
  * See http://www.cs.waikato.ac.nz/ml/weka/arff.html
*/
class ArffExperienceSource(val source: Source) extends ExperienceSource {

  val lineIterator = new ArffLineIterator(source.getLines())

  val schema: ExperienceSchema = readSchema(lineIterator)
  //println(schema)

  var nextExp: Option[Experience] = read()

  def readSchema(lineIterator: ArffLineIterator) = ArffExperienceSource.readSchema(lineIterator)

  def hasNext: Boolean = {
    nextExp match {
      case None => false
      case Some(_) => true
    }
  }

  def next: Experience = {
    nextExp match {
      case None => throw new PredictException("no next experience")
      case Some(exp) => {
        nextExp = read()
        exp
      }
    }
  }

  protected def read(): Option[Experience] = {
    lineIterator.hasNext match {
      case false => None
      case true => {
        var line = lineIterator.next
        line = line.indexOf("#") match {
          case -1 => line
          case index: Int => line.substring(index)
        }

        val values = line.split(",")
        values.length match {
          case 0 => read()
          case len => {
            if (len == schema.features.length) {
              Some(getExperience(values))
            } else {
              throw new PredictException("invalid line: "+line)
            }
          }
        }
      }
    }
  }

  protected def getExperience(values: Array[String]): Experience = {
    val features = schema.features.zip(values).map({case (fschema,strValue) => strValue match {
      case "?" => None
      case str => Some(fschema.value(str))
    }})
    val exp = new Experience(features)
    schema.validate(exp)
    exp
  }

}

/*********************************************************************************************************************/

/**
  Strips out comments and blank lines
  **/
class ArffLineIterator(val lineIterator: Iterator[String]) extends Iterator[String] {

  var nextLine = read()

  def hasNext: Boolean = {
    nextLine match {
      case None => false
      case Some(_) => true
    }
  }

  def next: String = {
    nextLine match {
      case None => throw new PredictException("no next line")
      case Some(line) => {
        nextLine = read()
        line
      }
    }
  }

  def read(): Option[String] = {
    lineIterator.hasNext match {
      case false => None
      case true => {
        var line = lineIterator.next
        line = line.indexOf("%") match {
          case -1 => line
          case index: Int => line.substring(0,index)
        }
        line = line.trim
        line.length match {
          case 0 => read()
          case _ => Some(line)
        }
      }
    }
  }
}

/*********************************************************************************************************************/
