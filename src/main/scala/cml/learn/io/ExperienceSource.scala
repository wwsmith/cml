/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import scala.io.Source

import cml.learn.Experience
import cml.learn.ExperienceSchema

/*********************************************************************************************************************/

object ExperienceSource {
  def apply(path: String): ExperienceSource = {
    val exp = """\S+\.exp""".r
    val arff = """\S+\.arff""".r
    val carff = """\S+\.carff""".r
    path match {
      //case exp() => new SimpleExperienceSource(Source.fromFile(path))
      case arff() => new ArffExperienceSource(Source.fromFile(path))
      case carff() => new CarnacArffExperienceSource(Source.fromFile(path))
      case _ => throw new java.io.IOException("no reader known for "+path)
    }
  }

  def apply(path: String, drop: List[Symbol], output: Symbol): ExperienceSource = {
    val unfilteredSource = apply(path)
    TransformExperienceSource(drop,output,unfilteredSource)
  }
}

/*********************************************************************************************************************/

abstract class ExperienceSource extends Iterator[Experience] {
  def schema: ExperienceSchema
}

/*********************************************************************************************************************/

object ExperienceTraceSource {
  def apply(traceSource: TraceSource): ExperienceTraceSource = new ExperienceTraceSource(traceSource)
}

class ExperienceTraceSource(traceSource: TraceSource) extends ExperienceSource {
  def schema = traceSource.schema

  var nextExp: Option[Experience] = getNext()

  private def getNext(): Option[Experience] = {
    traceSource.hasNext match {
      case true => traceSource.next match {
        case Left(query) => getNext()
        case Right(exp) => Some(exp)
      }
      case false => None
    }
  }

  def hasNext: Boolean = {
    nextExp match {
      case None => false
      case Some(_) => true
    }
  }

  def next: Experience = {
    nextExp match {
      case None => throw new java.io.IOException("no next experience")
      case Some(exp) => {
        nextExp = getNext()
        exp
      }
    }
  }
}

/*********************************************************************************************************************/
