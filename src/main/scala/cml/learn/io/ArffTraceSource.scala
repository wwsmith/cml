/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import scala.io.Source
import scala.collection.mutable.PriorityQueue

import cml.learn._

/*********************************************************************************************************************/

object ArffTraceSource {
  def main(args: Array[String]) = {
    if (args.length != 3) {
      println("usage: ArffTraceSource <trace file> <insert feature> <predict feature>")
      sys.exit(1)
    }
    val source = new ArffTraceSource(Source.fromFile(args(0)),args(1),args(2))
    while (source.hasNext) {
      val exp = source.next
      println(exp)
    }
  }
}

/*********************************************************************************************************************/

class ArffTraceSource(val source: Source,
                      val insertFeature: String,
                      val predictFeature: String) extends TraceSource {
  val expSource = new ArffExperienceSource(source)
  def schema = expSource.schema

  val insertIndex = schema.features.indexWhere(feature => feature.name match {
    case Symbol(`insertFeature`) => true
    case _ => false
  })
  if (insertIndex == -1) {
    throw new PredictException("no insert feature '%s' in schema %s".format(insertFeature,schema))
  }
  val predictIndex = schema.features.indexWhere(feature => feature.name match {
    case Symbol(`predictFeature`) => true
    case _ => false
  })
  if (predictIndex == -1) {
    throw new PredictException("no predict feature '%s' in schema %s".format(predictFeature,schema))
  }

  var queue = PriorityQueue[(Double,Either[Query,Experience])]()(Ordering.by(
    kv => kv._1  // increasing order (default is decreasing)
  ))

  for (exp <- expSource) {
    val expKey = exp(insertIndex) match {
      case Some(v) => v match {
        case i: Int => i.toDouble
        case l: Long => l.toDouble
        case f: Float => f.toDouble
        case d: Double => d
        case _ => throw new java.io.IOException("can't handle feature")
      }
      case None => throw new java.io.IOException("feature has no value")
    }
    queue += Tuple2(expKey,Right(exp))

    val query = Query(exp)
    val predKey = query(predictIndex) match {
      case Some(v) => v match {
        case i: Int => i.toDouble
        case l: Long => l.toDouble
        case f: Float => f.toDouble
        case d: Double => d
        case _ => throw new java.io.IOException("can't handle feature")
      }
      case None => throw new java.io.IOException("feature has no value")
    }
    queue += Tuple2(predKey,Left(query))
  }

  def hasNext: Boolean = {
    queue.size > 0
  }

  def next: Either[Query,Experience] = {
    queue.dequeue()._2
  }

}

/*********************************************************************************************************************/
