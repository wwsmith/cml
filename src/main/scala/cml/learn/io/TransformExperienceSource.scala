/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import cml.learn._

/*********************************************************************************************************************/

object TransformExperienceSource {
  def apply(dropFeatures: List[Symbol], outputFeature: Symbol, source: ExperienceSource): TransformExperienceSource = {
    var transform = Vector[Int]()
    for(index <- 0 until source.schema.features.length) {
      if ((source.schema.features(index).name != outputFeature) &&
        !dropFeatures.contains(source.schema.features(index).name)) {
        transform = transform :+ index
      }
    }
    for(index <- 0 until source.schema.features.length) {
      if (source.schema.features(index).name == outputFeature) {
        transform = transform :+ index
      }
    }
    new TransformExperienceSource(transform,source)
  }

  def apply(schema: ExperienceSchema, source: ExperienceSource): TransformExperienceSource = {
    var nameToIndex = source.schema.features.map(feature => feature.name).zipWithIndex.toMap
    try {
      val transform = schema.features.map(feature => nameToIndex(feature.name))
      new TransformExperienceSource(transform,source)
    } catch {
      case e: java.util.NoSuchElementException => {
        print(source.schema)
        print(schema)
        println(nameToIndex)
        throw e
      }
    }
  }
}

/*********************************************************************************************************************/

class TransformExperienceSource(val transform: Vector[Int], val source: ExperienceSource) extends ExperienceSource {

  var filterSchema = new ExperienceSchema()
  for(index <- transform) {
    filterSchema.features = filterSchema.features :+ source.schema.features(index)
  }

  def schema: ExperienceSchema = filterSchema

  def hasNext: Boolean = source.hasNext

  def next: Experience = {
    val exp = source.next
    new Experience(transform.map(index => exp(index)))
  }

}

/*********************************************************************************************************************/
