/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import scala.io.Source

import cml.learn._

/*********************************************************************************************************************/

/*
object TraceSource {
  def apply(path: String): TraceSource = {
    val exp = """\S+\.exp""".r
    val arff = """\S+\.arff""".r
    val earff = """\S+\.earff""".r
    path match {
      case exp() => new SimpleTraceSource(Source.fromFile(path))
      case earff() => new ExtendedArffTraceSource(Source.fromFile(path)
      case _ => throw new java.io.IOException("no source known for "+path)
    }
  }
}
*/

/*********************************************************************************************************************/

abstract class TraceSource extends Iterator[Either[Query,Experience]] {
  def schema: ExperienceSchema
}

/*********************************************************************************************************************/
