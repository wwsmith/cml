/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import cml.learn._

/*********************************************************************************************************************/

/**

Writes experiences in the Weka Attribute-Relation File Format.
  * See http://www.cs.waikato.ac.nz/ml/weka/arff.html

*/
class ArffExperienceWriter(val name: String,
			   val description: String,
			   schema: ExperienceSchema,
			   val writer: java.io.BufferedWriter) extends ExperienceWriter(schema) {

  writer.write(description.split("\n").mkString("% ","\n","\n"))
  writer.newLine()
  writeSchema(schema)
  writer.newLine()
  writer.write("@data")
  writer.newLine()

  private def writeSchema(schema: ExperienceSchema): Unit = {
    writer.write("@relation '"+name+"'")
    writer.newLine()
    for (feature <- schema.features) {
      feature match {
	case f: CategoryFeatureSchema => {
	  writer.write("@attribute "+feature.name.name+" ")
	  writer.write(f.categories.mkString("{",",","}"))
	}
	case f: StringFeatureSchema => writer.write("@attribute "+feature.name.name+" string")
	case f: BooleanFeatureSchema => writer.write("@attribute "+feature.name.name+" {True,False}")
	case f: IntFeatureSchema => writer.write("@attribute "+feature.name.name+" numeric")
	case f: LongFeatureSchema => writer.write("@attribute "+feature.name.name+" numeric")
	case f: FloatFeatureSchema => writer.write("@attribute "+feature.name.name+" numeric")
	case f: DoubleFeatureSchema => writer.write("@attribute "+feature.name.name+" numeric")
      }
      writer.newLine()
    }
  }

  def write(exp: Experience): Unit = {
    writer.write(exp.map(feature => feature match {
      case None => "?"
      case Some(value) => value.toString
    }).mkString("",",","\n"))
  }

  def close() = {
    writer.close()
  }

}
