/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.io

import scala.io.Source

import cml.learn._

/*********************************************************************************************************************/

object CarnacArffTraceSource {
  def main(args: Array[String]) = {
    if (args.length != 1) {
      println("usage: CarnacArffTraceSource <trace file>")
      sys.exit(1)
    }
    val source = new CarnacArffTraceSource(Source.fromFile(args(0)))
    while (source.hasNext) {
      val exp = source.next
      println(exp)
    }
  }
}

/*********************************************************************************************************************/

class CarnacArffTraceSource(val source: Source) extends TraceSource() {

  val lineIterator = new ArffLineIterator(source.getLines())

  val arffSchema = CarnacArffExperienceSource.readSchema(lineIterator)
  //println(schema)

  var nextOne: Option[Either[Query,Experience]] = read()

  def schema: ExperienceSchema = arffSchema

  def hasNext: Boolean = {
    nextOne match {
      case None => false
      case Some(_) => true
    }
  }

  def next: Either[Query,Experience] = {
    nextOne match {
      case None => throw new java.io.IOException("no next experience")
      case Some(qe) => {
        nextOne = read()
        qe
      }
    }
  }

  protected def read(): Option[Either[Query,Experience]] = {
    lineIterator.hasNext match {
      case false => None
      case true => {
        val Insert = """\+ (.*)""".r
        val Predict = """\? (.*)""".r
        val line = lineIterator.next
        line match {
          case Insert(valsStr) => Some(Right(getExperience(valsStr.split(","))))
          case Predict(valsStr) => Some(Left(Query(getExperience(valsStr.split(",")))))
          case line: String => throw new java.io.IOException("invalid line: "+line)
        }
      }
    }
  }

  protected def getExperience(values: Array[String]): Experience = {
    if (values.length != schema.features.length) {
      throw new java.io.IOException("wrong number of values in: "+values.mkString(","))
    }
    val features = schema.features.zip(values).map({case (fschema,strValue) => strValue match {
      case "?" => None
      case str => Some(fschema.value(str))
    }})
    val exp = new Experience(features)
    schema.validate(exp)
    exp
  }

}

/*********************************************************************************************************************/
