/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import scala.util.{Try,Success,Failure}

import cml.learn._

/*********************************************************************************************************************/

class InOrderPredictor(name: String,
		       schema: ExperienceSchema) extends EnsemblePredictor(name,schema) {

  override def toString(): String = {
    "InOrder"+super.toString()
  }

  def ensemblePredict(query: Query): Try[Prediction] = {
    // try each predictor in order until one gives a prediction
    for(predictor <- predictors) {
      predictor.predict(query) match {
        case Success(pred) => return Success(pred)
        case Failure(error) =>
      }
    }
    Failure(new PredictException("no predictors to Predict from"))
  }

}
