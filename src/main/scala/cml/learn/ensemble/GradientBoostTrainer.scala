/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import scala.util.{Try,Success,Failure}

import com.weiglewilczek.slf4s._

import cml.learn._

/*********************************************************************************************************************/

class GradientBoostTrainer(name: String,
                           schema: ExperienceSchema,
                           val learningRate: Float,
                           val numPredictors: Int,
                           val sampleFraction: Float,
                           val loss: RegressionLoss,
                           val trainer: Trainer) extends Trainer(name,schema) with Logging {

  val random = new scala.util.Random(47623) // set a seed for repeatability

  def this(name: String, schema: ExperienceSchema, trainer: Trainer) =
    this(name,schema,0.1f,200,1.0f,new cml.learn.SquaredErrorLoss(),trainer)

  def train(experiences: Vector[Experience]): Try[Predictor] = {
    val exps = experiences.map(exp => new WeightedExperience(exp,1.0f/experiences.size))

    val predictor = new AdditivePredictor("gradient boost predictor",schema)

    logger.info("creating initial tree")
    trainer.train(sample(exps)) match {
      case Success(f0) => {
        predictor.addPredictor(f0,learningRate)
        for (i <- 1 until numPredictors) {
          logger.info("creating tree %d".format(i))
          val rexps = getResiduals(predictor,exps)
          trainer.train(sample(rexps)) match {
            case Success(fi) => predictor.addPredictor(fi,learningRate)
            case Failure(e) => logger.warn("failed to train predictor %d".format(i))
          }
        }
        Success(predictor)
      }
      case Failure(e) => Failure(e)
    }
  }

  private def sample(experiences: Vector[Experience]): Vector[Experience] = {
    sampleFraction match {
      case 1.0f => experiences
      case frac => {
        random.shuffle(experiences).take((experiences.length * frac).toInt)
      }
    }
  }

  // set weights so that weight*output = residual ?
  // set output features to be the residuals
  private def getResiduals(predictor: Predictor, experiences: Vector[Experience]): Vector[Experience] = {
    val rexperiences = experiences.map(exp => {
      predictor.predict(Query(exp)) match {
	case Success(pred) => loss.residual(exp,pred)
	case Failure(error) => {
          // there should always be a prediction since the predictor was trained on these experiences
          throw new PredictException("can't compute residual - missing prediction")
        }
      }
    })
    predictor.clearStats()
    rexperiences
  }
}

/*********************************************************************************************************************/
