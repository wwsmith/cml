/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import scala.util.{Try,Success,Failure}

import com.weiglewilczek.slf4s._

import cml.learn._

/*********************************************************************************************************************/

abstract class EnsemblePredictor(name: String,
				 schema: ExperienceSchema) extends Predictor(name,schema) with Logging {
  protected var predictors = Vector[Predictor]()
  protected var weights = Vector[Float]()
  val numPredictions = 0

  override def toString(): String = {
    var mstr = "EnsemblePredictor "+name+":\n"
    for (predictor <- predictors) {
      val pLines = predictor.toString().split("\n")
      mstr += pLines.mkString("  ","\n  ","\n")
    }
    return mstr
  }

  def addPredictor(predictor: Predictor, weight: Float = 1.0f): Unit = {
    predictors = predictors :+ predictor
    weights = weights :+ weight
  }

  def numPredictors: Int = {
    predictors.length
  }

  override def clearStats(): Unit = {
    stats = PredictionStatistics(schema)
    for (predictor <- predictors) {
      predictor.clearStats()
    }
  }

  def insert(exp: Experience) = {
    //schema.validate(exp)
    for(predictor <- predictors) {
      predictor.insert(exp)
    }
  }

  def predict(query: Query): Try[Prediction] = {
    //schema.validate(query)
    val tryPred = ensemblePredict(query)
    stats.insert(query,tryPred)
    tryPred
  }

  protected def ensemblePredict(query: Query): Try[Prediction]

  protected def classify(): Boolean = {
    schema.features.last match {
      case s: CategoryFeatureSchema => true
      case s: StringFeatureSchema => true
      case _ => false
    }
  }

  protected def getConfInt(pred: Prediction): Double = {
    val lower = pred.interval match {
      case None => throw new PredictException("no prediction made")
      case Some(conf) => conf.lower match {
        case value: Int => value.toDouble
        case value: Long => value.toDouble
        case value: Float => value.toDouble
        case value: Double => value
        case _ => throw new ClassCastException("getConfInt found unsupported type for lower")
      }
    }

    val upper = pred.interval match {
      case None => throw new PredictException("no prediction made")
      case Some(conf) => conf.upper match {
        case value: Int => value.toDouble
        case value: Long => value.toDouble
        case value: Float => value.toDouble
        case value: Double => value
        case _ => throw new ClassCastException("getConfInt found unsupported type for upper")
      }
    }
    if (upper == lower) {
      logger.warn("upper and lower are the same - adjusting difference to 0.01")
      0.01
    } else {
      upper - lower
    }
  }

  protected def getPredCategory(pred: Prediction): Symbol = {
    pred.last match {
      case None => throw new PredictException("no prediction made")
      case Some(value: Symbol) => value
      case Some(value: String) => Symbol(value)
      case Some(_) => throw new ClassCastException("getPredCategory found unsupported type")
    }
  }

  protected def getPredValue(pred: Prediction): Double = {
    pred.last match {
      case None => throw new PredictException("no prediction made")
      case Some(anyValue) => anyValue match {
        case value: Int => value.toDouble
        case value: Long => value.toDouble
        case value: Float => value.toDouble
        case value: Double => value
        case _ => throw new ClassCastException("getPredValue found unsupported type")
      }
    }
  }

  protected def getLowerConfidence(pred: Prediction): Double = {
    pred.interval match {
      case None => throw new PredictException("no prediction made")
      case Some(conf) => conf.lower match {
        case value: Int => value.toDouble
        case value: Long => value.toDouble
        case value: Float => value.toDouble
        case value: Double => value
        case _ => throw new ClassCastException("getConfInt found unsupported type for lower")
      }
    }
  }

  protected def getUpperConfidence(pred: Prediction): Double = {
    pred.interval match {
      case None => throw new PredictException("no prediction made")
      case Some(conf) => conf.upper match {
        case value: Int => value.toDouble
        case value: Long => value.toDouble
        case value: Float => value.toDouble
        case value: Double => value
        case _ => throw new ClassCastException("getConfInt found unsupported type for upper")
      }
    }
  }

  override def shutdown(): Unit = {
    for(predictor <- predictors) {
      predictor.shutdown()
    }
  }
}

/*********************************************************************************************************************/
