/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import scala.util.{Try,Success,Failure}

import cml.learn._

/*********************************************************************************************************************/

// use the weights in the predictions
class WeightedPredictor(name: String, schema: ExperienceSchema) extends EnsemblePredictor(name,schema) {

  override def toString(): String = {
    "Weighted"+super.toString()
  }

  def ensemblePredict(query: Query): Try[Prediction] = {
    val preds = predictors.map(_.predict(query))
    val weightsPreds = weights.zip(preds).collect({
      case (weight,Success(pred)) => new WeightedPrediction(pred,weight)
    })
    weightsPreds.length match {
      case 0 => Failure(new PredictException("no predictions to weighted average"))
      case _ => formPrediction(query,weightsPreds)
    }
  }

  protected def formPrediction(query: Query,
                               weightsPreds: Vector[WeightedPrediction]): Try[Prediction] = {
    val totalWeight = weightsPreds.map(wp => wp.weight).sum
    val predValue = weightsPreds.map(wp => getPredValue(wp)).sum / totalWeight
    val lower = weightsPreds.map(wp => getLowerConfidence(wp)).sum / totalWeight
    val upper = weightsPreds.map(wp => getUpperConfidence(wp)).sum / totalWeight

    Success(createPrediction(query,predValue.toFloat,lower.toFloat,upper.toFloat))
  }

}

/*********************************************************************************************************************/
