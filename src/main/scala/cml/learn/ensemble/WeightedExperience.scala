/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import cml.learn.{Experience,FeatureSchema}

/*********************************************************************************************************************/

class WeightedExperience(exp: Experience, var weight: Float = 1.0f) extends Experience(exp) {
  override def toString(): String = {
    features.map(feature => feature match {
      case Some(v) => v.toString
      case None => "(none)"
    }).mkString("weighted experience (%f): (".format(weight),",",")\n")
  }
}

/*********************************************************************************************************************/
