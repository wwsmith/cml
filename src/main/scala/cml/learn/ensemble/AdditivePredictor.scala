/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import scala.util.{Try,Success,Failure}

import cml.learn._

/*********************************************************************************************************************/

// add the predictions together, use the weights in the predictions
class AdditivePredictor(name: String, schema: ExperienceSchema) extends EnsemblePredictor(name,schema) {

  override def toString(): String = {
    "Weighted"+super.toString()
  }

  override def insert(exp: Experience): Unit = {
    throw new PredictException("no common way to insert into an AdditivePredictor")
  }

  def ensemblePredict(query: Query): Try[Prediction] = {
    val preds = predictors.map(_.predict(query))
    val weightsPreds = weights.zip(preds).collect({
      case (weight,Success(pred)) => new WeightedPrediction(pred,weight)
    })
    weightsPreds.length match {
      case 0 => preds(0)
      case _ => formPrediction(query,weightsPreds)
    }
  }

  protected def formPrediction(query: Query,
                               weightsPreds: Vector[WeightedPrediction]): Try[Prediction] = {
    if (classify()) {
      val catConf = weightsPreds.groupBy(wp => wp.last).map({case (cat,wps) =>
        (cat,weightedConfidence(wps))}).collect({
          case (Some(v: Symbol),conf) => (v,conf)
          case (Some(v: String),conf) => (Symbol(v),conf)
        })

      val totalConf = catConf.foldLeft(0.0f)((total,cc) => total + cc._2)
      val bestCatConf = catConf.maxBy(cc => cc._2)

      Success(createPrediction(query,bestCatConf._1,bestCatConf._2/totalConf))
    } else {
      val predValue = weightsPreds.map(wp => wp.weight * getPredValue(wp)).sum
      // this assumes that later predictions get more and more accurate (tend toward 0)
      val lower = predValue + getLowerConfidence(weightsPreds.last)
      val upper = predValue + getUpperConfidence(weightsPreds.last)
      Success(createPrediction(query,predValue.toFloat,lower.toFloat,upper.toFloat))
    }
  }

  private def weightedConfidence(weightsPreds: Vector[WeightedPrediction]): Float = {
    val weightConf = weightsPreds.map(wp => (wp.weight,wp.confidence)).collect({
      case (weight,Some(conf)) => (weight,conf)
    })

    weightConf.foldLeft(0.0f)((total,wc) => total + wc._1 * wc._2) /
    weightConf.foldLeft(0.0f)((total,wc) => total + wc._2)
  }
}

/*********************************************************************************************************************/
