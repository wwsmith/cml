/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import scala.util.{Try,Success,Failure}
import scala.util.Random

import com.weiglewilczek.slf4s._

import cml.learn._

/*********************************************************************************************************************/


class BaggingTrainer(name: String,
                     schema: ExperienceSchema,
                     val sampleFraction: Double,
                     val replacement: Boolean,
                     val numPredictors: Int,
                     val trainer: Trainer) extends Trainer(name,schema) with Logging {

  val random = new Random(8572347)

  def train(experiences: Vector[Experience]): Try[Predictor] = {
    val predictor = new WeightedPredictor(name,schema)
    for (i <- 0 until numPredictors) {
      trainer.train(selectExperiences(experiences)) match {
        case Success(p) => predictor.addPredictor(p)
        case Failure(e) => logger.warn("failed to train a predictor: %s".format(e))
      }
    }
    Success(predictor)
  }

  protected def selectExperiences(experiences: Vector[Experience]): Vector[Experience] = {
    replacement match {
      case true => {
        var selected = Vector[Experience]()
        while (selected.length < experiences.length * sampleFraction) {
          selected = selected :+ experiences(random.nextInt(experiences.length))
        }
        selected
      }
      case false => {
        val shuffledExps = random.shuffle(experiences)
        shuffledExps.slice(0,scala.math.ceil(experiences.length * sampleFraction).toInt)
      }
    }
  }

}

/*********************************************************************************************************************/
