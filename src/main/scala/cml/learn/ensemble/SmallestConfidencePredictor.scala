/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ensemble

import scala.util.{Try,Success,Failure}

import cml.learn._

/*********************************************************************************************************************/

class SmallestConfidencePredictor(name: String,
				  schema: ExperienceSchema) extends EnsemblePredictor(name,schema) {

  override def toString(): String = {
    "SmallestConfidence"+super.toString()
  }

  def ensemblePredict(query: Query): Try[Prediction] = {
    val preds = predictors.map(_.predict(query)).filter(_.isSuccess).map(_.get)
    preds.length match {
      case 0 => Failure(new PredictException("no predictions to select from"))
      case _ => Success(preds.minBy(p => getConfInt(p)))
    }
  }

}

/*********************************************************************************************************************/
