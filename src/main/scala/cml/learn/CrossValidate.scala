/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

import scala.util.{Success,Failure}

import com.weiglewilczek.slf4s.Logging

import cml.learn.io._

/*********************************************************************************************************************/

class CrossValidate extends Logging {
  def validate(trainer: Trainer, experiences: Vector[Experience], numFolds: Int): PredictionStatistics = {
    val folds = experiences.grouped((experiences.length + numFolds - 1) / numFolds).toVector // round up

    val statsList = (0 until folds.length).map(index => {
      logger.info("fold %d".format(index+1))
      trainer.train((folds.take(index) ++ folds.drop(index+1)).flatten) match {
        case Success(pred) => {
          for (exp <- folds(index)) {
            pred.predict(Query(exp))
          }
          pred.stats
        }
        case Failure(e) => {
          logger.warn("  no predictor trained")
          new RegressionStatistics()
        }
      }
    })
    val sumStats = statsList.reduce((s1,s2) => s1.combineWith(s2))
    // divide by numFolds
    sumStats
  }
}

/*********************************************************************************************************************/
