/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

import scala.math._
import scala.util.{Try,Success,Failure}

/*********************************************************************************************************************/

object PredictionStatistics {
  def apply(schema: ExperienceSchema): PredictionStatistics = {
    schema.features.last match {
      case f: CategoryFeatureSchema => new ClassificationStatistics()
      case f: StringFeatureSchema => new ClassificationStatistics()
      case f: BooleanFeatureSchema => new ClassificationStatistics()
      case _ => new RegressionStatistics()
    }
  }
}

/*********************************************************************************************************************/

abstract class PredictionStatistics() {
  protected var numPredicted: Int = 0
  protected var numNotPredicted: Int = 0

  def insert(query: Query, pred: Try[Prediction]): Unit

  def combineWith(stats: PredictionStatistics): PredictionStatistics
}

/*********************************************************************************************************************/

class RegressionStatistics extends PredictionStatistics {

  private var numIntervals: Int = 0
  private var inInterval: Int = 0
  private var sumSquaredErrors: Double = 0.0
  private var sumErrors: Double = 0.0
  private var validPercentError: Boolean = true
  private var sumPercentErrors: Double = 0.0
  private var sumIntervals: Double = 0.0
  private var sumActual: Double = 0.0
  private var sumActualSquared: Double = 0.0
  private var sumExpected: Double = 0.0
  private var sumExpectedSquared: Double = 0.0
  private var sumActualExpected: Double = 0.0

  // accuracy is 1 if prediction = actual
  // accuracy is actual/prediction if action < prediction
  // accuracy is prediction/actual if action > prediction
  private var sumAccuracy: Double = 0.0

  override def toString(): String = {
    "PredictionStatistics:\n"+
    "  predicted: "+numPredicted+"\n"+
    "  didn't predict: "+numNotPredicted+"\n"+
    "  in interval: "+inInterval+"\n"+
    "  mean ci: "+sumIntervals/numPredicted+"\n"+
    "  mean actual: "+sumActual/numPredicted+"\n"+
    "  mean expected: "+sumExpected/numPredicted+"\n"+
    "  correlation coefficient: "+correlation+"\n"+
    "  coefficient of determination: "+determination+"\n"+
    "  root mean squared error: "+rootMeanSquaredError+"\n"+
    "  mean er: "+meanError+"\n"+
    "  mean perc er: "+meanPercentError+"\n"+
    "  mean accuracy: "+meanAccuracy+"\n"+
    "  mean er / mean act: "+sumErrors/sumActual+"\n"
  }

  def insert(query: Query, pred: Try[Prediction]): Unit = {
    pred match {
      case Failure(_) => numNotPredicted += 1
      case Success(p) => {
        numPredicted += 1

        query.last match {
          case None =>  // don't know the actual when used on live data
          case Some(_) => insertActual(query,p)
        }
      }
    }
  }

  def insertActual(query: Query, pred: Prediction): Unit = {
    val actual = query.last match {
      case None => throw new PredictException("no actual value found")
      case Some(value: Int) => value.toDouble
      case Some(value: Long) => value.toDouble
      case Some(value: Float) => value.toDouble
      case Some(value: Double) => value
      case _ => throw new PredictException("unsupported type for actual")
    }

    val expected = pred.last match {
      case None => throw new PredictException("no prediction found")
      case Some(value: Int) => value.toDouble
      case Some(value: Long) => value.toDouble
      case Some(value: Float) => value.toDouble
      case Some(value: Double) => value
      case _ => throw new PredictException("unsupported type for prediction")
    }

    pred.interval match {
      case None =>
      case Some(interval) => {
        numIntervals += 1
        val lower = interval.lower match {
          case value: Int => value.toDouble
          case value: Long => value.toDouble
          case value: Float => value.toDouble
          case value: Double => value
          case _ => throw new PredictException("unsupported type for lower")
        }
        val upper = pred.interval match {
          case None => throw new PredictException("no interval found")
          case Some(interval) => interval.upper match {
            case value: Int => value.toDouble
            case value: Long => value.toDouble
            case value: Float => value.toDouble
            case value: Double => value
            case _ => throw new PredictException("unsupported type for upper")
          }
        }
        sumIntervals += (upper - lower)
        if ((lower <= actual) && (actual <= upper)) {
          inInterval += 1
        }
      }
    }

    sumActual += actual
    sumActualSquared += actual*actual
    sumExpected += expected
    sumExpectedSquared += expected*expected
    sumActualExpected += actual*expected

    sumSquaredErrors += (actual - expected) * (actual - expected)
    sumErrors += scala.math.abs(actual - expected)
    if (scala.math.abs(actual) < 0.00001) {
      validPercentError = false
    }
    if (validPercentError) {
      sumPercentErrors += scala.math.abs(actual - expected) / actual * 100.0
    }

    if (scala.math.abs(expected-actual) < 0.00001) {
      sumAccuracy += 1.0
    } else if ((expected < 0) && (actual > 0)) { // need them both to have the same sign
      // accuracy is 0
    } else if ((expected > 0) && (actual < 0)) { // need them both to have the same sign
      // accuracy is 0
    } else if (expected > actual) {
      sumAccuracy += actual/expected
    } else {
      sumAccuracy += expected/actual
    }
  }

  def combineWith(stats: PredictionStatistics): PredictionStatistics = {
    stats match {
      case stats: RegressionStatistics => {
        val s = new RegressionStatistics()
        s.numPredicted = numPredicted + stats.numPredicted
        s.numNotPredicted = numNotPredicted + stats.numNotPredicted
        s.inInterval = inInterval + stats.inInterval
        s.sumSquaredErrors = sumSquaredErrors + stats.sumSquaredErrors
        s.sumErrors = sumErrors + stats.sumErrors
        if (validPercentError && stats.validPercentError) {
          s.validPercentError = true
        } else {
          s.validPercentError = false
        }
        s.sumPercentErrors = sumPercentErrors + stats.sumPercentErrors
        s.sumIntervals = sumIntervals + stats.sumIntervals
        s.sumActual = sumActual + stats.sumActual
        s.sumActualSquared = sumActualSquared + stats.sumActualSquared
        s.sumExpected = sumExpected + stats.sumExpected
        s.sumActualExpected = sumActualExpected + stats.sumActualExpected
        s.sumExpectedSquared = sumExpectedSquared + stats.sumExpectedSquared
        s.sumAccuracy = sumAccuracy + stats.sumAccuracy

        s
      }
      case _ => throw new PredictException("can only combine with RegressionStatistics")
    }
  }

  def meanError: Float = {
    if (numPredicted == 0) {
      return 0.0f
    } else {
      return (sumErrors/numPredicted).toFloat
    }
  }

  def meanPercentError: Float = {
    if ((numPredicted == 0) || !validPercentError) {
      return 0.0f
    } else {
      return (sumPercentErrors/numPredicted).toFloat
    }
  }

  def meanInterval: Float = {
    if (numPredicted == 0) {
      return 0.0f
    } else {
      return (sumIntervals/numPredicted).toFloat
    }
  }

  def meanAccuracy: Float = {
    if (numPredicted == 0) {
      return 0.0f
    } else {
      return (sumAccuracy/numPredicted).toFloat
    }
  }

  def percentInInterval: Float = {
    if (numPredicted == 0) {
      return 0.0f
    } else {
      return 100.0f * inInterval.toFloat/numPredicted
    }
  }

  /**
   * Pearson product-moment correlation coefficient
   */
  def correlation: Float = {
    val corr = (sumActualExpected - sumActual*sumExpected/numPredicted) /
    sqrt((sumActualSquared-pow(sumActual,2)/numPredicted) * (sumExpectedSquared-pow(sumExpected,2)/numPredicted))
    corr.toFloat
  }

  def rootMeanSquaredError: Float = {
    math.sqrt(sumSquaredErrors / numPredicted).toFloat
  }

  def determination: Float = {
    val actualMean = sumActual / numPredicted
    val sumSquaredResiduals = sumActualSquared - 2 * actualMean * sumActual + numPredicted * actualMean * actualMean
    (1.0 - sumSquaredErrors / sumSquaredResiduals).toFloat
  }

}

/*********************************************************************************************************************/

class ClassificationStatistics extends PredictionStatistics {
  private var numCorrect: Double = 0.0
  private var numConfidence: Double = 0.0
  private var sumConfidence: Double = 0.0

  def percentCorrect: Float = {
    (numCorrect / (numPredicted + numNotPredicted) * 100).toFloat
  }

  override def toString(): String = {
    "PredictionStatistics:\n" +
    "  predicted: %d%n".format(numPredicted.toInt) +
    "  didn't predict: %d%n".format(numNotPredicted.toInt) +
    "  correct: %d%n".format(numCorrect.toInt) +
    "  expected correct: %d%n".format((sumConfidence/numConfidence * numPredicted).toInt)
  }

  def insert(query: Query, pred: Try[Prediction]): Unit = {
    pred match {
      case Failure(_) => numNotPredicted += 1
      case Success(p) => {
        numPredicted += 1
        query.last match {
          case None =>  // don't know the actual when used on live data
          case Some(_) => insertActual(query,p)
        }
      }
    }
  }

  def insertActual(query: Query, pred: Prediction): Unit = {
    if (query.last == pred.last) {
      numCorrect += 1
    }
    pred.confidence match {
      case Some(conf) => {
        numConfidence += 1
        sumConfidence += conf
      }
      case None =>
    }
  }

  def combineWith(stats: PredictionStatistics): PredictionStatistics = {
    stats match {
      case stats: ClassificationStatistics => {
        val s = new ClassificationStatistics()
        s.numPredicted = numPredicted + stats.numPredicted
        s.numNotPredicted = numNotPredicted + stats.numNotPredicted
        s.numCorrect = numCorrect + stats.numCorrect
        s.numConfidence = numConfidence + stats.numConfidence
        s.sumConfidence = sumConfidence + stats.sumConfidence

        s
      }
      case _ => throw new PredictException("can only combine with ClassificationStatistics")
    }
  }
}

/*********************************************************************************************************************/
