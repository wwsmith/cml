/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

import scala.math._

/*********************************************************************************************************************/

object Distribution {
  def apply(experiences: Vector[Experience]): Distribution = {
    apply(experiences,experiences(0).length-1)
  }

  def apply(experiences: Vector[Experience], feature: Int): Distribution = {
    val gd = GaussianDistribution(values(experiences,feature))

    // gaussian has been doing better and don't have confidence intervals for log normal
    // so just use gaussian
    gd

    /*
    val lnd = LogNormalDistribution(values(experiences,feature))
    printf("  gaussian: expects %f, goodness %f%n",gd.expected,gd.goodness)
    printf("  log normal: expects %f, goodness %f%n",lnd.expected,lnd.goodness)
    if (gd.goodness <= lnd.goodness) {
      gd
    } else {
      lnd
    }
    */
  }

  private def values(experiences: Vector[Experience], feature: Int): Vector[Float] = {
    experiences.filter(exp => exp(feature) match {
      case None => false
      case Some(v) => true
    }).map(exp => value(exp,feature))
  }

  private def value(exp: Experience, feature: Int): Float = {
    exp(feature) match {
      case Some(value: Int) => value.toFloat
      case Some(value: Long) => value.toFloat
      case Some(value: Float) => value
      case Some(value: Double) => value.toFloat
      case _ => throw new ClassCastException("output isn't an int, long, float, or double")
    }
  }

  def mean(experiences: Vector[Experience], feature: Int): Float = {
    val vals = values(experiences,feature)
    vals.reduce(_ + _) / vals.length
  }

  def variance(experiences: Vector[Experience], feature: Int): Float = {
    variance(values(experiences,feature))
  }

  def variance(values: Vector[Float]): Float = {
    var n = 0
    var mean = 0.0f
    var m2 = 0.0f

    for(x <- values) {
      n += 1
      val delta = x - mean
      mean += delta / n
      m2 += delta * (x - mean)
    }

    m2 / (n - 1)
  }

  def erf(x: Double): Double = {
    val t = 1.0 / (1.0 + 0.5 * abs(x))
    val tau = t * exp(-pow(x,2) - 1.26551223 + 1.00002368*t + 0.37409196*pow(t,2) + 0.09678418*pow(t,3)
    - 0.18628806*pow(t,4) + 0.27886807*pow(t,5) - 1.13520398*pow(t,6) + 1.48851587*pow(t,7)
    - 0.82215223*pow(t,8) + 0.17087277*pow(t,9))
    if (x >= 0) {
      (1.0 - tau).toFloat
    } else {
      (tau - 1).toFloat
    }
  }

}

/*********************************************************************************************************************/

abstract class Distribution {
  def size: Int
  def expected: Float
  def confidence(levelOfConfidence: Int): (Float,Float)
  def goodness: Float // smaller is better


  protected def cumulativeAt(x: Float): Float

}

trait KolmgorovSmirnov {
  protected def kolmgorovSmirnovTest(values: Vector[Float], cumulativeAt: (Float) => Float): Float = {
    values.sorted.zipWithIndex.map(valueIndex => {
      (abs(cumulativeAt(valueIndex._1) - 1.0f/values.length * (valueIndex._2+1))).toFloat
    }).max
  }
}

/*********************************************************************************************************************/

object GaussianDistribution extends KolmgorovSmirnov {
  def apply(values: Vector[Float]): GaussianDistribution = {
    val mean = values.reduce(_ + _) / values.length
    val stdDev = sqrt(Distribution.variance(values)).toFloat

    GaussianDistribution(mean,stdDev,kolmgorovSmirnovTest(values,cumulativeAt(mean,stdDev)),values.size)
  }

  protected def cumulativeAt(mean: Float, stdDev: Float)(x: Float): Float = {
    (0.5 * (1 + Distribution.erf((x-mean) / sqrt(2*pow(stdDev,2))))).toFloat
  }

}

case class GaussianDistribution(val mean: Float, val stdDev: Float,
                                val goodness: Float, val size: Int) extends Distribution {

  def expected: Float = mean

  def confidence(levelOfConfidence: Int): (Float,Float) =
    (mean - getIntervalSizeEmpiricalRule(levelOfConfidence), mean + getIntervalSizeEmpiricalRule(levelOfConfidence))

  private def getIntervalSizeEmpiricalRule(levelOfConf: Int): Float = {
    if (stdDev == Float.NaN) {
      Float.MaxValue
    } else if (levelOfConf >= 99) {
      2.58f * stdDev
    } else if (levelOfConf >= 95) {
      1.96f * stdDev
     } else if (levelOfConf >= 90) {
      1.65f * stdDev
    } else if (levelOfConf >= 85) {
      1.44f * stdDev
    } else if (levelOfConf >= 80) {
      1.28f * stdDev
    } else {
      1.15f * stdDev // 75
    }
  }

  protected def valueAt(x: Float): Float = {
    ((1.0f / (stdDev * sqrt(2.0*scala.math.Pi))) * exp(-pow(x-mean,2)/(2*pow(stdDev,2)))).toFloat
  }

  protected def cumulativeAt(x: Float): Float = {
    (0.5 * (1 + Distribution.erf((x-mean) / sqrt(2*pow(stdDev,2))))).toFloat
  }

}

/*********************************************************************************************************************/

object LogNormalDistribution extends KolmgorovSmirnov {
  def apply(values: Vector[Float]): LogNormalDistribution = {
    val location = (values.map(x => log(x)).reduce(_ + _) / values.length).toFloat
    val scale = (values.map(x => pow(log(x)-location,2)).reduce(_ + _) / values.length).toFloat

    LogNormalDistribution(location,scale,kolmgorovSmirnovTest(values,cumulativeAt(location,scale)),values.size)
  }

  protected def cumulativeAt(location: Float, scale: Float)(x: Float): Float = {
    (0.5 + 0.5 * Distribution.erf((log(x)-location)/(sqrt(2)*scale))).toFloat
  }

}

case class LogNormalDistribution(val location: Float, val scale: Float,
                                 val goodness: Float, val size: Int) extends Distribution {

  def expected: Float = exp(location+0.5*pow(scale,2)).toFloat

  def confidence(levelOfConfidence: Int): (Float,Float) = (expected,expected)

  protected def valueAt(x: Float): Float = {
    ((1.0f / (x * scale * sqrt(2.0*scala.math.Pi))) * exp(-pow(log(x)-location,2)/(2*pow(scale,2)))).toFloat
  }

  protected def cumulativeAt(x: Float): Float = {
    (0.5 + 0.5 * Distribution.erf((log(x)-location)/(sqrt(2)*scale))).toFloat
  }

}

/*********************************************************************************************************************/

// Exponential, Weibull
