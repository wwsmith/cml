/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

/*********************************************************************************************************************/

// confidence % for classification
// interval for regression
class Prediction(features: Seq[Option[Any]],
                 val confidence: Option[Float] = None,
                 val interval: Option[Interval] = None) extends Experience(features) {
  override def toString(): String = {
    var pstr = features.map(feature => feature match {
      case Some(v) => v.toString
      case None => "(none)"
    }).mkString("Prediction: (",",","")

    confidence match {
      case Some(c) => pstr += " - %f%%)%n".format(c)
      case None => interval match {
        case Some(i) => pstr += " - %s)%n".format(i)
        case None => ")"
      }
    }
    pstr
  }

  override def toString(schema: ExperienceSchema): String = {
    var pstr = schema.features.zip(features).map({case (fschema,value: Option[Any]) => {
      value match {
        case None => fschema.name.name+": (none)"
        case Some(v) => fschema.name.name+": "+v.toString
      }
    }}).mkString("Prediction:\n  ","\n  ","")

    confidence match {
      case Some(c) => pstr += " - %f%%%n".format(c)
      case None => interval match {
        case Some(i) => pstr += " - %s%n".format(i)
        case None => ")"
      }
    }
    pstr
  }

}

/*********************************************************************************************************************/
