/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

/*********************************************************************************************************************/

abstract class FeatureSchema(val name: Symbol, val required: Boolean = false) {

  def featureType(): String

  def value(str: String): Any

  override def toString(): String = {
    required match {
      case true => name.name+": required "+featureType()
      case false => name.name+": optional "+featureType()
    }
  }

  def validate(feature: Option[Any]): Unit

}

/*********************************************************************************************************************/

class CategoryFeatureSchema(name: Symbol,
                            required: Boolean = false,
                            val categories: Set[Symbol]) extends FeatureSchema(name,required) {
  override def featureType(): String = {
    "category (%s)".format(categories.mkString(","))
  }

  override def value(str: String): Any = {
    Symbol(str)
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: Symbol => {
            if ((categories.size > 0) && !categories.contains(v2)) {
              throw new SchemaException("category "+v2+" for feature "+name+" is invalid")
            }
          }
          case _ => throw new SchemaException("feature "+name+" is not a symbol")
        }
      }
    }
  }
}

/*********************************************************************************************************************/

class StringFeatureSchema(name: Symbol,
                          required: Boolean = false,
                          val maxLength: Int = 0) extends FeatureSchema(name,required) {
  override def featureType(): String = {
    "string"
  }

  override def value(str: String): Any = {
    str
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: String => {
            if (maxLength > 0) {
              if (v2.length() > maxLength) {
                throw new SchemaException("string value for feature "+name+" is too long")
              }
            }
          }
          case _ => throw new SchemaException("feature "+name+" is not a string")
        }
      }
    }
  }
}

/*********************************************************************************************************************/

class BooleanFeatureSchema(name: Symbol,
                           required: Boolean = false) extends FeatureSchema(name,required) {
  override def featureType(): String = {
    "boolean"
  }

  override def value(str: String): Any = {
    str.toBoolean
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: Boolean => {}
          case _ => throw new SchemaException("feature "+name+" is not a boolean")
        }
      }
    }
  }
}

/*********************************************************************************************************************/

class IntFeatureSchema(name: Symbol,
                       required: Boolean = false) extends FeatureSchema(name,required) {
  override def featureType(): String = {
    "int"
  }

  override def value(str: String): Any = {
    str.toInt
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: Int => {}
          case _ => throw new SchemaException("feature "+name+" is not an integer")
        }
      }
    }
  }
}

/*********************************************************************************************************************/

class LongFeatureSchema(name: Symbol,
                        required: Boolean = false) extends FeatureSchema(name,required) {
  override def featureType(): String = {
    "long"
  }

  override def value(str: String): Any = {
    str.toLong
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: Long => {}
          case _ => throw new SchemaException("feature "+name+" is not a long")
        }
      }
    }
  }
}

/*********************************************************************************************************************/

class FloatFeatureSchema(name: Symbol,
                         required: Boolean = false) extends FeatureSchema(name,required) {
  override def featureType(): String = {
    "float"
  }

  override def value(str: String): Any = {
    str.toFloat
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: Float => {}
          case _ => throw new SchemaException("feature "+name+" is not a float")
        }
      }
    }
  }
}

/*********************************************************************************************************************/

class DoubleFeatureSchema(name: Symbol,
                          required: Boolean = false) extends FeatureSchema(name,required) {
  override def featureType(): String = {
    "double"
  }

  override def value(str: String): Any = {
    str.toDouble
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: Double => {}
          case _ => throw new SchemaException("feature "+name+" is not a double")
        }
      }
    }
  }
}

/*********************************************************************************************************************/

class DateFeatureSchema(name: Symbol,
                        required: Boolean = false,
                        dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss'Z'") extends FeatureSchema(name,required) {

  val format = new java.text.SimpleDateFormat(dateFormat)

  override def featureType(): String = {
    "date"
  }

  override def value(str: String): Any = {
    format.parse(str).getTime() // Long
  }

  override def validate(feature: Option[Any]): Unit = {
    feature match {
      case None => if (required) {
        throw new SchemaException("required feature "+name+" not present")
      }
      case Some(v) => {
        v match {
          case v2: Long => {}
          case _ => throw new SchemaException("feature "+name+" is not a long")
        }
      }
    }
  }
}

/*********************************************************************************************************************/
