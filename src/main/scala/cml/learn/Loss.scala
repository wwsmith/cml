/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

import com.weiglewilczek.slf4s._

/*********************************************************************************************************************/

/**
  A function to minimize when building a tree.
  */
trait Loss {

  // assume that losses are combined by just adding them

  // used when building trees
  def loss(experiences: Vector[Experience]): Float

  // to efficiently find a split point
  def incremental(): IncrementalLoss

}

trait IncrementalLoss {
  def add(experience: Experience): Unit
  def remove(experience: Experience): Unit
  def loss(): Float
}

trait RegressionLoss extends Loss {
  protected def value(experience: Experience): Double = {
    experience.last match {
      case Some(v: Int) => v.toDouble
      case Some(v: Long) => v.toDouble
      case Some(v: Float) => v.toDouble
      case Some(v: Double) => v
      case Some(v) => throw new PredictException("can't handle feature of type "+v.getClass.getName)
      case None => throw new PredictException("can't handle experience with missing output feature")
    }
  }

  // used by boosting
  def residual(experience: Experience, prediction: Prediction): Experience = {
    val features = experience.init :+ negativeGradient(experience.last,prediction.last)
    new Experience(features)
  }

  protected def negativeGradient(actual: Option[Any], estimate: Option[Any]): Option[Any]
}

/*********************************************************************************************************************/

// This is the same as the variance
class SquaredErrorLoss extends RegressionLoss {
  def loss(experiences: Vector[Experience]): Float = {
    var sum = 0.0
    var sumSqr = 0.0
    for (v <- experiences.map(exp => value(exp))) {
      sum += v
      sumSqr += scala.math.pow(v,2)
    }

    val count = experiences.length.toDouble
    val estimate = sum / count
    (1.0/count * (sumSqr - 2 * estimate * sum + count * estimate * estimate)).toFloat
  }

  def incremental(): IncrementalLoss = new SquaredErrorIncrementalLoss()

  protected def negativeGradient(actual: Option[Any], estimate: Option[Any]): Option[Any] = {
    (actual,estimate) match {
      case (None,_) => None
      case (_,None) => None
      case (Some(act: Int),Some(est: Int)) => Some(act - est)
      case (Some(act: Long),Some(est: Long)) => Some(act - est)
      case (Some(act: Float),Some(est: Float)) => Some(act - est)
      case (Some(act: Double),Some(est: Double)) => Some(act - est)
      case (_,_) => throw new PredictException("mismatched types")
    }
  }

  class SquaredErrorIncrementalLoss extends IncrementalLoss {
    var count = 0
    var sum = 0.0
    var sumSqr = 0.0

    def add(experience: Experience): Unit = {
      count += 1
      sum += value(experience)
      sumSqr += scala.math.pow(value(experience),2)
    }

    def remove(experience: Experience): Unit = {
      count -= 1
      sum -= value(experience)
      sumSqr -= scala.math.pow(value(experience),2)
    }

    def loss(): Float = {
      val estimate = sum / count  // mean
      (1.0/count * (sumSqr - 2 * estimate * sum + count * estimate * estimate)).toFloat
    }
  }
}

/*********************************************************************************************************************/

// AbsoluteError
// this is almost the same as standard deviation, but the median is used instead of the mean

/*********************************************************************************************************************/

// Huber


/*********************************************************************************************************************/

trait ClassificationLoss extends Loss {
  protected def value(experience: Experience): Symbol = {
    experience.last match {
      case Some(v: String) => Symbol(v)
      case Some(v: Symbol) => v
      case Some(v) => throw new PredictException("can't handle feature of type "+v.getClass.getName)
      case None => throw new PredictException("can't handle experience with missing output feature")
    }
  }
}

abstract class ClassificationIncrementalLoss extends IncrementalLoss {
  var occurMap = Map[Symbol,Integer]()
  var count = 0

  def add(experience: Experience): Unit = {
    val output = experience.last match {
      case Some(v: Symbol) => v
      case Some(v: String) => Symbol(v)
      case Some(v) => throw new PredictException("can't handle feature of type "+v.getClass.getName)
      case None => throw new PredictException("can't handle experience with missing output feature")
    }
    val occur = occurMap.getOrElse[Integer](output,0) + 1
    occurMap = occurMap + (output -> occur)
    count += 1
  }

  def remove(experience: Experience): Unit = {
    experience.last match {
      case Some(v: Symbol) => occurMap = occurMap + (v -> (occurMap(v) - 1))
      case Some(v: String) => occurMap = occurMap + (Symbol(v) -> (occurMap(Symbol(v)) - 1))
      case Some(v) => throw new PredictException("can't handle feature of type "+v.getClass.getName)
      case None => throw new PredictException("can't handle experience with missing output feature")
    }
    count -= 1
  }
}

class GiniIndex extends ClassificationLoss {
  def loss(experiences: Vector[Experience]): Float = {
    val values = experiences.map(exp => value(exp))
    val occurMap = values.groupBy(v => v).map({case (value,vals) => (value,vals.size)})
    val proportionMap = occurMap.map({case (value,count) => (value,count.toFloat/experiences.size)})
    proportionMap.foldLeft(0.0f)((index,vp) => index + vp._2 * (1.0f - vp._2))
  }

  def incremental(): IncrementalLoss = new GiniIndexIncrementalLoss()

  class GiniIndexIncrementalLoss extends ClassificationIncrementalLoss {
    def loss(): Float = {
      val proportionMap = occurMap.map({case (v,c) => (v,c.toFloat/count)})
      proportionMap.foldLeft(0.0f)((index,vp) => index + vp._2 * (1.0f - vp._2))
    }
  }
}


/*********************************************************************************************************************/

class Entropy extends ClassificationLoss {
  def loss(experiences: Vector[Experience]): Float = {
    val values = experiences.map(exp => value(exp))
    val occurMap = values.groupBy(v => v).map({case (value,vals) => (value,vals.size)})
    val proportionMap = occurMap.map({case (value,count) => (value,count.toFloat/experiences.size)})
    -proportionMap.foldLeft(0.0f)((entropy,vp) => entropy + vp._2 * scala.math.log(vp._2).toFloat)
  }

  def incremental(): IncrementalLoss = new EntropyIncrementalLoss()

  class EntropyIncrementalLoss extends ClassificationIncrementalLoss {
    def loss(): Float = {
      val proportionMap = occurMap.map({case (v,c) => (v,c.toFloat/count)})
      -proportionMap.foldLeft(0.0f)((entropy,vp) => entropy + vp._2 * scala.math.log(vp._2).toFloat)
    }
  }
}

/*********************************************************************************************************************/

class Misclassification extends ClassificationLoss {
  def loss(experiences: Vector[Experience]): Float = {
    val values = experiences.map(exp => value(exp))
    val maxOcc = values.groupBy(v => v).maxBy(_._2.size)._2.size

    1.0f - maxOcc.toFloat/experiences.size
  }

  def incremental(): IncrementalLoss = new MisclassificationIncrementalLoss()

  class MisclassificationIncrementalLoss extends ClassificationIncrementalLoss {
    def loss(): Float = {
      1.0f - occurMap.map({case (c,v) => v}).max / count
    }
  }
}

/*********************************************************************************************************************/
