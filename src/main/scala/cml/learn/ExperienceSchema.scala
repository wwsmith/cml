/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

import cml.learn.io._

/*********************************************************************************************************************/

class ExperienceSchema(val name: String = "") extends Seq[FeatureSchema] {
  var features = Vector[FeatureSchema]()

  def iterator: Iterator[FeatureSchema] = features.iterator

  def apply(idx: Int): FeatureSchema = features.apply(idx)

  def length: Int = features.length

  override def toString(): String = {
    "Schema "+name+":\n" + features.map(feature => feature.toString).mkString("  ","\n  ","\n")
  }

  def isValid(exp: Experience): Boolean = {
    try {
      validate(exp)
      return true
    } catch {
      case e: SchemaException => return false
    }
  }

  def validate(exp: Experience): Unit = {
    if (exp.length != features.length) {
      throw new SchemaException("found "+exp.length+" features, but expected "+features.length)
    }
    for(i <- 0 until features.length) {
      features(i).validate(exp(i))
    }
  }

  def withCategories(experiences: Vector[Experience]): ExperienceSchema = {
    val newSchema = new ExperienceSchema(name)
    newSchema.features = features.zipWithIndex.map({case (feature,index) => feature match {
      case fschema: CategoryFeatureSchema => {
        val categories = experiences.map(exp => exp(index)).collect({case Some(cat: Symbol) => cat}).toSet
        new CategoryFeatureSchema(fschema.name,fschema.required,categories)
      }
      case fschema => fschema
    }})
    newSchema
  }
}

/*********************************************************************************************************************/
