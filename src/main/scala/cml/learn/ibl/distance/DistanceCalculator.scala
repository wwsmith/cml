/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.PredictException
import cml.learn.Query
import cml.learn.io._

/*********************************************************************************************************************/

object Weights {
  def apply(weights: Map[String,Float], schema: ExperienceSchema): Vector[Float] = {
    vector(weights.map({case (k,v) => (Symbol(k),v)}),schema)
  }

  def vector(weights: Map[Symbol,Float], schema: ExperienceSchema): Vector[Float] = {
    schema.features.init.map(feature => weights.getOrElse(feature.name,1.0f))
  }
}

/*********************************************************************************************************************/

abstract class DistanceCalculator(val schema: ExperienceSchema,
                                  weights: Vector[Float],
                                  val scale: ScaleFactor) {
  def this(schema: ExperienceSchema) = this(schema,Vector[Float](),new UnitScaleFactor())
  def this(schema: ExperienceSchema, weights: Vector[Float]) = this(schema,weights,new UnitScaleFactor())
  def this(schema: ExperienceSchema, scale: ScaleFactor) = this(schema,Vector[Float](),scale)

  val featureWeights = if (weights.length == 0) {
    Vector.fill[Float](schema.features.length)(1.0f)
  } else {
    if (weights.length != schema.features.length-1) {
      println(schema)
      println(weights)
      throw new PredictException("length of feature weight vector doesn't match number of input features")
    }
    weights
  }

  var numInvocations = 0

  // True if d(x,y) = d(y,x)
  var symmetric = false

  // True if d(x,y) > 0 when x != y and d(x,x) = 0
  var nonNegative = false

  // True if d(x,y) <= d(x,z)+d(z,y)
  var satisfiesTriangleInequality = false

  // True if d(x,y) is always the same no matter what other experiences are in the experience base.
  //var constantDistances = false
  def constantDistances(): Boolean = {
    scale.constantDistances
  }

  def calculate(p1: Query, p2: Experience): Distance = {
    calculate(Experience(p1),p2)
  }

  def calculate(p1: Experience, p2: Experience): Distance

  override def toString(): String = {
    var dstr = "DistanceCalculator\n"
    dstr += schema.toString.split("\n").mkString("  ","\n  ","\n")
    dstr += "  feature weights:\n"
    for ((feature,weight) <- schema.features.init.zip(featureWeights)) {
      dstr += "    "+feature.name.name+": "+weight+"\n"
    }
    dstr += scale
    dstr
  }

}

/*********************************************************************************************************************/
