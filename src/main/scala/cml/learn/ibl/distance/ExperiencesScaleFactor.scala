/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import cml.learn._
import cml.learn.io.ExperienceSource

/*********************************************************************************************************************/

object ExperiencesScaleFactor {

  def apply(source: ExperienceSource): ScaleFactor = {
    apply(source.toVector)
  }

  def apply(experiences: Vector[Experience]): ScaleFactor = {
    if (experiences.length == 0) {
      throw new PredictException("need at least one experience to compute scale factors")
    }

    val ranges = (0 until experiences(0).length-1).toVector.map(index => {
      val values = experiences.map(exp => exp(index)).collect({
        case Some(value: Int) => value.toFloat
        case Some(value: Long) => value.toFloat
        case Some(value: Float) => value
        case Some(value: Double) => value.toFloat
      })

      values.length match {
        case 0 => (0.0f,0.0f)  // return something - will result in a factor of 1.0f below
        case _ => (values.min,values.max)
      }
    })

    val factors = ranges.map({case (min,max) => {
      if (min >= max) {
        1.0f
      } else {
        1.0f / (max - min)
      }
    }})

    new FixedScaleFactor(factors)
  }

}

/*********************************************************************************************************************/
