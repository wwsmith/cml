/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import cml.learn.Experience
import cml.learn.ExperienceSchema

/*********************************************************************************************************************/

class EuclideanDistanceCalculator(schema: ExperienceSchema,
                                  weights: Vector[Float],
                                  scale: ScaleFactor)
extends MinkowskyDistanceCalculator(schema,weights,scale,2.0f) {

  def this(schema: ExperienceSchema) = this(schema,Vector[Float](),new UnitScaleFactor())
  def this(schema: ExperienceSchema, weights: Vector[Float]) = this(schema,weights,new UnitScaleFactor())
  def this(schema: ExperienceSchema, scale: ScaleFactor) = this(schema,Vector[Float](),scale)
}
