/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import cml.learn.Experience
import cml.learn.ExperienceSchema

/*********************************************************************************************************************/

class SeenScaleFactor(val schema: ExperienceSchema) extends ScaleFactor(false) {
  var mins = Vector.fill[Float](schema.features.length)(Float.MaxValue)
  var maxs = Vector.fill[Float](schema.features.length)(Float.MinValue)

  def add(exp: Experience): Unit = {
    for(index <- 0 until exp.length) {
      exp(index) match {
        case Some(v: Int) => add(index,v.toFloat)
        case Some(v: Long) => add(index,v.toFloat)
        case Some(v: Float) => add(index,v.toFloat)
        case Some(v: Double) => add(index,v.toFloat)
        case None => // do nothing
        case _ => {
          throw new ClassCastException("add found unsupported type for value")
          0.0f
        }
      }
    }
  }

  private def add(index: Int, value: Float) {
    if (value < mins(index)) {
      mins = mins.updated(index,value)
    }
    if (value > maxs(index)) {
      maxs = maxs.updated(index,value)
    }
  }

  def factor(index: Int): Float = {
    maxs(index) - mins(index)
  }

  override def toString(): String = {
    "  seen scale factor\n"
  }
}
