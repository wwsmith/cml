/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import scala.math._

import cml.learn.Experience
import cml.learn.ExperienceSchema

/*********************************************************************************************************************/

class HeomDistanceCalculator(schema: ExperienceSchema,
                             weights: Vector[Float],
                             scale: ScaleFactor)
extends DistanceCalculator(schema,weights,scale) {
  symmetric = true
  nonNegative = true
  satisfiesTriangleInequality = true

  def this(schema: ExperienceSchema) = this(schema,Vector[Float](),new UnitScaleFactor())
  def this(schema: ExperienceSchema, weights: Vector[Float]) = this(schema,weights,new UnitScaleFactor())
  def this(schema: ExperienceSchema, scale: ScaleFactor) = this(schema,Vector[Float](),scale)

  override def toString(): String = {
    "HEOM "+super.toString()
  }

  override def calculate(p1: Experience, p2: Experience): Distance = {
    numInvocations += 1

    // assume that p1 and p2 are of the same schema

    var totalDistance = 0.0
    var totalWeight = 0.0
    for (i <- 0 until schema.features.length-1) {
      val nd = calculate(p1,p2,i)
      totalDistance += nd._1
      totalWeight += nd._2
    }
    val dist = new Distance(p1, p2, sqrt(totalDistance/totalWeight).toFloat)
    dist
  }

  private def calculate(exp1: Experience, exp2: Experience, index: Int): (Float, Float) = {
    val d = if (exp1(index) == None) {
      1.0f
    } else if (exp2(index) == None) {
      1.0f
    } else {
      (exp1(index), exp2(index)) match {
        case (None, None) => 0.0f
        case (Some(_), None) => 1.0f
        case (None, Some(_)) => 1.0f
        case (Some(v1: String), Some(v2: String)) => if (v1.equals(v2))  {
          0.0f
        } else {
          1.0f
        }
        case (Some(v1: Symbol), Some(v2: Symbol)) => if (v1.equals(v2))  {
          0.0f
        } else {
          1.0f
        }
        case (Some(v1: Boolean), Some(v2: Boolean)) => if (v1 == v2) {
          0.0f
        } else {
          1.0f
        }
        case (Some(v1: Int), Some(v2: Int)) => abs(v1.toFloat-v2.toFloat) * scale.factor(index)
        case (Some(v1: Long), Some(v2: Long)) => abs(v1.toFloat-v2.toFloat) * scale.factor(index)
        case (Some(v1: Float), Some(v2: Float)) => abs(v1-v2) * scale.factor(index)
        case (Some(v1: Double), Some(v2: Double)) => abs(v1.toFloat-v2.toFloat) * scale.factor(index)
        case (Some(v1: Any), Some(v2: Any)) => {
          throw new ClassCastException("unsupported type or types: "+v1)
        }
      }
    }
    val weight = featureWeights(index)
    //println(exp1.schema.features(index).name+": d = "+d+", weight = "+weight)

    // force ceiling for d to be 1.0
    val d2 = if (d > 1.0) {
      1.0
    } else {
      d
    }

    //pow(weight*d2,2).toFloat
    (weight * pow(d2,2).toFloat, weight)
  }
}
