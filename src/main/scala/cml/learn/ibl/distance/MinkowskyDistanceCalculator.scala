/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import scala.math._

import cml.learn.Experience
import cml.learn.ExperienceSchema

/*********************************************************************************************************************/

class MinkowskyDistanceCalculator(schema: ExperienceSchema,
                                  weights: Vector[Float],
                                  scale: ScaleFactor,
                                  val p: Float) extends DistanceCalculator(schema,weights,scale) {
  symmetric = true
  nonNegative = true
  satisfiesTriangleInequality = true

  def this(schema: ExperienceSchema, p: Float) = this(schema,Vector[Float](),new UnitScaleFactor(),p)
  def this(schema: ExperienceSchema, weights: Vector[Float], p: Float) = this(schema,weights,new UnitScaleFactor(),p)
  def this(schema: ExperienceSchema, scale: ScaleFactor, p: Float) = this(schema,Vector[Float](),scale,p)

  def calculate(p1: Experience, p2: Experience): Distance = {
    numInvocations += 1

    val dists = (0 until schema.features.length-1).map(i => calculate(p1,p2,i))
    new Distance(p1, p2, pow(dists.foldLeft(0.0)((d1,d2) => d1 + d2),1.0/p).toFloat)
  }

  def calculate(exp1: Experience, exp2: Experience, index: Int): Float = {
    val d = (exp1(index), exp2(index)) match {
      case (None, None) => 0.0f
      case (Some(_), None) => 1.0f
      case (None, Some(_)) => 1.0f
      case (Some(v1: Int), Some(v2: Int)) => abs(v1-v2).toFloat / scale.factor(index)
      case (Some(v1: Long), Some(v2: Long)) => abs(v1-v2).toFloat / scale.factor(index)
      case (Some(v1: Float), Some(v2: Float)) => abs(v1-v2) / scale.factor(index)
      case (Some(v1: Double), Some(v2: Double)) => abs(v1-v2).toFloat / scale.factor(index)
      case _ => {
        throw new ClassCastException("unsupported type")
      }
    }
    pow(d,p).toFloat
  }
}
