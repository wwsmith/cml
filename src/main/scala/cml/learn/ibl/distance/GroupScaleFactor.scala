/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import cml.learn.Experience
import cml.learn.ExperienceSchema

/*********************************************************************************************************************/

// this + Gaussian is the same as LocalGaussian?
class GroupScaleFactor(schema: ExperienceSchema) extends SeenScaleFactor(schema) {
  def add(experiences: List[Experience]): Unit = {
    mins = Vector.fill[Float](schema.features.length)(Float.MaxValue)
    maxs = Vector.fill[Float](schema.features.length)(Float.MinValue)
    for (exp <- experiences) {
      add(exp)
    }
  }

  override def toString(): String = {
    "  group scale factor\n"
  }
}
