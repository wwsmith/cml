/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.distance

import cml.learn.Experience

/*********************************************************************************************************************/

class Distance(val exp1: Experience, val exp2: Experience, val distance: Float) extends Ordered[Distance] {
  def compare(that: Distance): Int = {
    if (distance < that.distance) {
      -1
    } else if (distance > that.distance) {
      1
    } else {
      this.hashCode() - that.hashCode()
    }
  }

  override def toString(): String = {
    "Distance: %f%n    %s    %s".format(distance,exp1,exp2)
  }
}
