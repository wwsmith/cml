/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl

import scala.math.sqrt
import scala.util.{Try,Success,Failure}

import cml.learn._
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator
import cml.learn.ibl.distance.ExperienceBaseScaleFactor
import cml.learn.ibl.exb.ExperienceBase
import cml.learn.ibl.kernel._

/*********************************************************************************************************************/

class KernelPredictor(name: String,
                      expBase: ExperienceBase,
                      val distanceCalc: DistanceCalculator,
                      val kernel: Kernel) extends IblPredictor(name,expBase) {

  val classify = distanceCalc.schema.features.last match {
    case f: CategoryFeatureSchema => true
    case f: StringFeatureSchema => true
    case _ => false
  }

  override def toString(): String = {
    val dcLines = distanceCalc.toString().split("\n")
    val kLines = kernel.toString().split("\n")
    "Kernel Predictor\n" +
    "  output feature is "+distanceCalc.schema.features.last.name.name+"\n" +
    dcLines.mkString("  ","\n  ","\n") +
    kLines.mkString("  ","\n  ","\n")
  }

  override def predict(query: Query): Try[Prediction] = {
    val distances = expBase.nearestNeighbors(query,-1)
    predict(query,distances)
  }

  def predict(query: Query, distances: Vector[Distance]): Try[Prediction] = {
    val tryPred = classify match {
      case false => regression(query,distances)
      case true => classification(query,distances)
    }
    stats.insert(query,tryPred)
    tryPred
  }

  /**

    if feature i is an integer or float:

                sum(point_i*kernel(distance(query_i,point_i)))
    predict_i = ----------------------------------------------
                    sum(kernel(distance(query_i,point_i)))

    the kernel is assumed to be larger when the distance is smaller, giving
    more weight to nearby points

  */
  def regression(query: Query, distances: Vector[Distance]): Try[Prediction] = {
    var numerator = 0.0f
    var denominator = 0.0f
    for (dist <- distances) {
      // Assumes that experience1 in distances is the query
      val value = dist.exp2.last match {
        case None => return Failure(new PredictException("no output found in experience "+dist.exp2))
        case Some(value: Int) => value.toFloat
        case Some(value: Long) => value.toFloat
        case Some(value: Float) => value
        case Some(value: Double) => value.toFloat
        case Some(_) => throw new ClassCastException("output isn't an int, long, float, or double")
      }
      try {
	val kernelVal = kernel.calculateKernel(dist.distance,distances)
	numerator += value * kernelVal
	denominator += kernelVal
      } catch {
        case e: KernelException => return Failure(new PredictException(e.getMessage()))
      }
    }

    if (denominator == 0.0f) {
      return Failure(new PredictException("the most similar "+distances.length+
					  " experiences are not similar enough to predict from"))
    }
    if (denominator.isNaN) {
      return Failure(new PredictException("kernel values are bad"))
    }

    val predValue = numerator/denominator
    val (lower,upper) = intervalByVariance(predValue,distances,query.levelOfConfidence)
    val pred = createPrediction(query,predValue,lower,upper)

    Success(pred)
  }

  private def intervalByVariance(predValue: Float,
                                 distances: Vector[Distance],
                                 levelOfConfidence: Int): (Float,Float) = {
    
    // calculate the variance for the prediction interval
    var numerator = 0.0f
    var denominator = 0.0f
    for (dist <- distances) {
      val value = dist.exp2.last match {
        case Some(value: Int) => value.toFloat
        case Some(value: Long) => value.toFloat
        case Some(value: Float) => value
        case Some(value: Double) => value.toFloat
        case _ => throw new ClassCastException("output isn't an int, long, float, or double")
      }
      try {
	val kernelVal = kernel.calculateKernel(dist.distance,distances)
	numerator += (predValue-value)*(predValue-value) * kernelVal
	denominator += kernelVal
      } catch {
        case _: Throwable => 
        // just ignore it
      }
    }
    val stdDev = sqrt(numerator/denominator).toFloat
    val interval = getIntervalSizeEmpiricalRule(stdDev,levelOfConfidence)
    (predValue-interval, predValue+interval)
  }

  private def getIntervalSizeEmpiricalRule(stdDev: Float, levelOfConf: Int): Float = {
    if (stdDev == Float.NaN) {
      Float.MaxValue
    } else if (levelOfConf >= 99) {
      2.58f * stdDev
    } else if (levelOfConf >= 95) {
      1.96f * stdDev
    } else if (levelOfConf >= 90) {
      1.65f * stdDev
    } else if (levelOfConf >= 85) {
      1.44f * stdDev
    } else if (levelOfConf >= 80) {
      1.28f * stdDev
    } else {
      1.15f * stdDev // 75
    }
  }

  private def getIntervalSizeTchebysheff(stdDev: Float, levelOfConf: Int): Float = {
    if (stdDev == Float.NaN) {
      Float.MaxValue
    } else {
      val k = sqrt(1/(1-levelOfConf/100.0f)).toFloat
      k * stdDev
    }
  }

  def classification(query: Query, distances: Vector[Distance]): Try[Prediction] = {
    try {
      // Assumes that experience1 in distances is the query
      val symWeight = distances.map(dist => (outputSymbol(dist.exp2),kernel.calculateKernel(dist.distance,distances)))

      val weights = symWeight.foldLeft(Map[Symbol,Float]())((map,sw) => {
        map + (sw._1 -> (map.getOrElse(sw._1,0.0f) + sw._2))
      })
      val totalWeight = symWeight.foldLeft(0.0f)((weight,sw) => weight + sw._2)
      val best = weights.maxBy(_._2)

      val pred = createPrediction(query,best._1,best._2 / totalWeight)
      Success(pred)
    } catch {
      case e: ClassCastException => return Failure(e)
      case e: KernelException => return Failure(e)
    }
  }

  private def outputSymbol(exp: Experience): Symbol = {
    exp.last match {
      case None => throw new PredictException("no output found in experience "+exp)
      case Some(value: String) => Symbol(value)
      case Some(value: Symbol) => value
      case Some(_) => throw new ClassCastException("output isn't a symbol or string")
    }
  }
}
