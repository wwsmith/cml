/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl

import cml.learn.Experience
import cml.learn.Predictor
import cml.learn.ibl.exb.ExperienceBase

/*********************************************************************************************************************/

abstract class IblPredictor(name: String,
                            val expBase: ExperienceBase) extends Predictor(name,expBase.distanceCalc.schema) {

  def insert(exp: Experience): Unit = {
    expBase.insert(exp)
  }

  def shutdown(): Unit = {
    expBase.close()
  }

}

/*********************************************************************************************************************/
