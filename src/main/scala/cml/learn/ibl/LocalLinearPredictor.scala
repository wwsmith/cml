/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl

import scala.math._
import scala.util.{Try,Success,Failure}

import Jama._

import cml.learn._
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator
import cml.learn.ibl.distance.ExperienceBaseScaleFactor
import cml.learn.ibl.exb.ExperienceBase
import cml.learn.ibl.kernel._

/*********************************************************************************************************************/

class LocalLinearPredictor(name: String,
			   expBase: ExperienceBase,
			   val distanceCalc: DistanceCalculator,
			   val kernel: Kernel,
			   val k: Int) extends IblPredictor(name,expBase) {

  override def insert(exp: Experience): Unit = {
    expBase.insert(exp)
  }

  override def predict(query: Query): Try[Prediction] = {
    val distances = expBase.nearestNeighbors(query,k)
    val tryPred = predict(query,distances)
    stats.insert(query,tryPred)
    tryPred
  }

    /**

    If output feature j is an integer or float, see below.

    If output feature i is a string, use the Kernel method.

    the kernel is assumed to be larger when the distance is smaller, giving
    more weight to nearby points

    */
    /**
    weight_i = sqrt(kernel(distance(point_i,query)))

    z_i = weight_i * query_i

    Z = W * query (W_ii = weight_i)

    v = W * y (v_i = weight_i * point_i^j)

    predict^j = transpose(query) * inverse(tranpose(Z) * Z) * transpose(Z) * v

    */
  def predict(query: Query, distances: Vector[Distance]): Try[Prediction] = {
    val linearInputFeatures = distanceCalc.schema.features.zipWithIndex.filter(featureIndex => {
      if (distanceCalc.featureWeights(featureIndex._2) < 0.0001) {
	  false
	} else {
	  featureIndex._1 match {
	    case f: IntFeatureSchema => true
	    case f: LongFeatureSchema => true
	    case f: FloatFeatureSchema => true
	    case f: DoubleFeatureSchema => true
	    case _ => false
	  }
	}
    }).map(featureIndex => featureIndex._2)

    // Assumes that all of the experiences have values set for the linearInputFeatureNames
    
    // The X matrix has the input values for each neighbor. Each neighbor is a row with a 1 added in the
    // last column.

    val X = new Matrix(distances.length,linearInputFeatures.length+1);
    for(i <- 0 until distances.length) {
      // Assumes that experience1 in distances is the query
      for(j <- 0 until linearInputFeatures.length) {
	X.set(i,j,distances(i).exp2(j) match {
	  case Some(v: Int) => v.toFloat
	  case Some(v: Long) => v.toFloat
	  case Some(v: Float) => v
	  case Some(v: Double) => v.toFloat
	  case _ => return Failure(new PredictException("can't handle value"))
	})
      }
      X.set(i,linearInputFeatures.length,1) // append a 1 on each row to include a constant term
    }

    // scale all of the values to be between -1 and 1 to avoid ill-conditioned matrices
	/*
	Matrix inputMax = getMax(X);
	X = X.arrayRightDivide(inputMax);

	System.out.print("X after scale");
	X.print(10,4);
	*/

    // the W matrix has the feature weights

    val W = new Matrix(distances.length,distances.length)
    for(i <- 0 until distances.length) {
      W.set(i,i,sqrt(kernel.calculateKernel(distances(i).distance)).toFloat)
    }

    val Z = W.times(X)
    val Zt = Z.transpose()

    // v has the output feature values (don't need to scale?)

    val v = new Matrix(distances.length,1)
    for(i <- 0 until distances.length) {
      // Assumes that experience1 in distances is the query
      val fv: Float = distances(i).exp2.last match {
	case Some(f: Int) => f.toFloat
	case Some(f: Long) => f.toFloat
	case Some(f: Float) => f
	case Some(f: Double) => f.toFloat
	case _ => return Failure(new PredictException("couldn't handle feature type"))
      }
      v.set(i,0,W.get(i,i) * fv)
    }

    val b = solveByQR(Z,v)
    //val b = solveBySVD(Z,v)
    //val b = solveBySVD2(Z,v)

    var predValue = linearInputFeatures.zipWithIndex.map(indexes => {
      val fv: Float = query(indexes._2) match {
	case Some(f: Int) => f.toFloat
	case Some(f: Long) => f.toFloat
	case Some(f: Float) => f
	case Some(f: Double) => f.toFloat
	case _ => return Failure(new PredictException("couldn't handle feature type"))
      }
      b.get(indexes._1,0).toFloat * fv
      //b.get(indexes._2,0) * query.features(indexes._2) * inputMax.get(0,indexes._1)
    }).reduce((p1,p2) => p1+p2)
    predValue += b.get(linearInputFeatures.length,0).toFloat
    //predValue += b.get(linearInputFeatures.length,0) * inputMax.get(0,linearInputFeatures.length)

    // this probably isn't right
    var sumWeightedResidualsSquared = 0.0
    var sumWeights = 0.0
    for(i <- 0 until distances.length) {
      val residual = v.get(i,0) - estimate(X,b,i)
      sumWeightedResidualsSquared += W.get(i,i) * residual * residual
      sumWeights += W.get(i,i)
    }
    val mse = (sumWeightedResidualsSquared / sumWeights).toFloat
    val stdDev = sqrt(mse / (distances.length - 2)).toFloat
    val ci = getIntervalSizeEmpiricalRule(stdDev,query.levelOfConfidence)

    val pred = createPrediction(query,predValue,predValue-ci,predValue+ci)
    Success(pred)
  }

  private def getIntervalSizeEmpiricalRule(stdDev: Float, levelOfConf: Int): Float = {
    if (stdDev == Float.NaN) {
      Float.MaxValue
    } else if (levelOfConf >= 95) {
      1.96f * stdDev
    } else if (levelOfConf >= 90) {
      1.65f * stdDev
    } else if (levelOfConf >= 85) {
      1.44f * stdDev
    } else if (levelOfConf >= 80) {
      1.28f * stdDev
    } else {
      1.15f * stdDev // 75
    }
  }

  protected def solveByCholesky(Z: Matrix, v: Matrix): Matrix = {
    val Zt = Z.transpose()
    val cd = Zt.times(Z).chol()
    if (!cd.isSPD()) {
      throw new PredictException("Z*Zt is not symmetric positive definite")
    }
    val L = cd.getL()
    val Lt = L.transpose()

    val a = L.solve(Zt.times(v))
    Lt.solve(a)
  }

  protected def solveByQR(Z: Matrix, v: Matrix): Matrix = {
    val Zt = Z.transpose()
    val qr = Zt.times(Z).qr()
    val a = qr.getQ().solve(Zt.times(v))
      
    qr.getR().solve(a)
    //solveXInUXEqualY(qr.getR(),a)
  }

    /*
    protected Matrix solveXInUXEqualY(Matrix U, Matrix y) {
	if (U.getColumnDimension() != y.getRowDimension()) {
	    throw new RuntimeException("solving x in U * x = y requires that the number of columns in U "+
				       "equal the number of rows in y")
	}
	if (y.getColumnDimension() != 1) {
	    throw new RuntimeException("solving x in transpose(L) * x = y requires that y be a vector")
	}
	if (!isUpperTriangular(U)) {
	    throw new RuntimeException("matrix U is not upper triangular")
	}

	Matrix x = new Matrix(U.getColumnDimension(),1)
	for(int i=x.getRowDimension()-1i>=0i--) {
	    double sum = 0.0f
	    for(int j=i+1j<x.getRowDimension()j++) {
		sum += U.get(i,j) * x.get(j,0)
	    }
	    x.set(i,0,(y.get(i,0) - sum) / U.get(i,i))
	    System.out.println("set "+i+" to "+x.get(i,0))
	}
	return x
    }

    protected double epsilon = 0.0001

    protected boolean isUpperTriangular(Matrix m) {
	for(int i=0i<m.getRowDimension()i++) {
	    for(int j=0j<ij++) {
		if (Math.abs(m.get(i,j)) > epsilon) {
		    return false
		}
	    }
	}
	return true
    }
    */

  protected def solveBySVD(Z: Matrix, v: Matrix): Matrix = {
    val Zt = Z.transpose()
    val svd = Zt.times(Z).svd()
    val a = svd.getU().solve(Zt.times(v))
    val left = svd.getS().times(svd.getV().transpose())
    left.solve(a)
  }

  protected def solveBySVD2(Z: Matrix, v: Matrix): Matrix = {
    val svd = Z.svd()
    svd.getV().times(svd.getS().inverse()).times(svd.getU().transpose()).times(v)
  }

  protected def estimate(X: Matrix, b: Matrix, row: Int): Float = {
    var e = 0.0
    for(i <- 0 until b.getRowDimension()) {
      e += X.get(row,i) * b.get(i,0)
    }
    e.toFloat
  }
}
