/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl

import scala.math.sqrt
import scala.util.Try

import cml.learn._
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator
import cml.learn.ibl.distance.ExperienceBaseScaleFactor
import cml.learn.ibl.exb.ExperienceBase
import cml.learn.ibl.kernel._

/*********************************************************************************************************************/

class NearestNeighborsPredictor(name: String,
                                expBase: ExperienceBase,
                                distanceCalc: DistanceCalculator,
                                val k: Int,
                                kernel: Kernel = new UniformKernel())
extends KernelPredictor(name,expBase,distanceCalc,kernel) {

  override def toString(): String = {
    val dcLines = distanceCalc.toString().split("\n")
    val kLines = kernel.toString().split("\n")
    "Nearest Neighbors Predictor\n" +
    "  " + k + " neighbors\n" +
    dcLines.mkString("  ","\n  ","\n") + kLines.mkString("  ","\n  ","\n")
  }

  override def predict(query: Query): Try[Prediction] = {
    val distances = expBase.nearestNeighbors(query,k)
    predict(query,distances)
  }

}

/*********************************************************************************************************************/
