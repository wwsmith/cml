/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

import cml.learn._
import cml.learn.ibl.distance._
import cml.learn.ibl.exb.mtree._

/*********************************************************************************************************************/

class MTreeExperienceBase(distanceCalc: DistanceCalculator, maxSize: Int = -1)
extends ExperienceBase(distanceCalc,maxSize) with ExperienceBaseUtilities {

  if (distanceCalc.symmetric != true) {
    throw new PredictException("MTreeExperienceBase requires symmetric distance function")
  }
  if (distanceCalc.nonNegative != true) {
    throw new PredictException("MTreeExperienceBase requires nonNegative distances")
  }
  if (distanceCalc.satisfiesTriangleInequality != true) {
    throw new PredictException("MTreeExperienceBase requires a distance func that satisfies the triangle inequality")
  }
  if (distanceCalc.constantDistances != true) {
    throw new PredictException("MTreeExperienceBase requires constant distances")
  }

  val mtree = new MTree(distanceCalc)
  
  override def insert(exp: Experience): Unit = {
    mtree.insert(exp)
  }

  override def nearestNeighbors(query: Query, k: Int): Vector[Distance] = {
    mtree.neighbors(query,k).toVector
  }

  override def neighborsWithin(query: Query, distance: Float): Vector[Distance] = {
    mtree.range(query,distance).toVector
  }

  override def size(): Int = {
    mtree.size()
  }

  override def getFeatureRange(index: Int): Range = {
    getRange(index)
  }

  override def close(): Unit = {
  }

}
