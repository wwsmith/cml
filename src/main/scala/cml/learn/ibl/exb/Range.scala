/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

/*********************************************************************************************************************/

class Range(var minimum: Float = Float.MaxValue, var maximum: Float = Float.MinValue) {

  def add(value: Float): Unit = {
    if (value < minimum) {
      minimum = value
    }
    if (value > maximum) {
      maximum = value
    }
  }

  def size(): Float = {
    maximum - minimum
  }

  override def toString(): String = {
    toString("")
  }

  def toString(indent: String): String = {
    indent+"range: "+minimum+" - "+maximum
  }
}
