/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

import scala.math._

/*********************************************************************************************************************/

class Distribution {
  var stdDevSqr = 0.0f
  var sum = 0.0f
  var sumSqr = 0.0f
  var size = 0

  override def toString(): String = {
    toString("")
  }

  def toString(indent: String) = {
    indent+"distribution: "+" sum "+sum+", sumSqr "+sumSqr+", stdDevSqr "+stdDevSqr+", size: "+size
  }

  override def equals(that: Any) = that match {
    case dist: Distribution => {
      if (stdDevSqr != dist.stdDevSqr) {
	false
      } else if (sum != dist.sum) {
	false
      } else if (sumSqr != dist.sumSqr) {
	false
      } else if (size != dist.size) {
        false
      } else {
        true
      }
    }
    case _ => false
  }

  def mean(): Float = {
    sum/size
  }

  def standardDeviation(): Float = {
    sqrt(stdDevSqr).toFloat
  }

  def add(value: Float): Unit = {
    val mean = sum/size
    val deltaMean = (sum+value)/(size+1) - mean
    val stdDevSqrMod = (size*(stdDevSqr*size - 2*deltaMean*sum + 2*size*mean*deltaMean +
			      size*deltaMean*deltaMean + pow(value - mean - deltaMean,2)) - 
			(size+1)*(stdDevSqr*size)) / (size*(size+1))
    stdDevSqr += stdDevSqrMod.toFloat
    sum += value
    sumSqr += value*value
    size += 1
  }

  /**
   This method assumes that the value has been previously added.
   */
  def remove(value: Float): Unit = {
    if (size == 1) {
      stdDevSqr = 0.0f
      sum = 0.0f
      size = 0
    } else {
      val mean = sum/size
      val deltaMean = (sum-value)/(size-1) - mean
      val stdDevSqrMod = (size*(stdDevSqr*size - 2*deltaMean*sum + 2*size*mean*deltaMean +
				size*deltaMean*deltaMean - pow(value - mean - deltaMean,2)) - 
			  (size-1)*(stdDevSqr*size)) / (size*(size-1))
      stdDevSqr += stdDevSqrMod.toFloat
      sum -= value
      sumSqr -= value*value
      size -= 1
    }
  }

}
