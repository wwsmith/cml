/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

import cml.learn.Query
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.PredictException
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

abstract class ExperienceBase(val distanceCalc: DistanceCalculator,
                              val maxSize: Int = -1) {

  def insert(exp: Experience): Unit

  def nearestNeighbors(query: Query, k: Int): Vector[Distance]
  def neighborsWithin(query: Query, distance: Float): Vector[Distance]

  def size(): Int

  def getFeatureRange(index: Int): Range
  //def getFeatureRange(featureName: Symbol, topBottomPercentIgnore: Int): Range

  def close(): Unit

}

/*********************************************************************************************************************/
