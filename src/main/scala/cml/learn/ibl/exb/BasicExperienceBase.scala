/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

import cml.learn.Query
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

class BasicExperienceBase(distanceCalc: DistanceCalculator,
                          maxSize: Int = -1) extends ExperienceBase(distanceCalc,maxSize)
with ExperienceBaseUtilities {

  var experiences = Vector[Experience]()

  private def checkDataSize(): Unit = {
    if ((maxSize > 0) && (experiences.length > maxSize)) {
      deleteFifo()
    }
  }

  // FIFO replacement policy
  private def deleteFifo(): Unit = {
    experiences = experiences.take(maxSize)
  }

  override def insert(exp: Experience): Unit = {
    //println("insert "+exp)
    distanceCalc.schema.validate(exp)
    experiences = exp +: experiences   // prepend
    //addToDistributions(exp)
    addToRanges(exp)
    checkDataSize()
  }

  override def size(): Int = {
    return experiences.length
  }

  override def nearestNeighbors(query: Query, k: Int): Vector[Distance] = {
    //println("  getting "+k+" nearest neighbors")

    /* not bad */
    var distances = experiences.map(exp => distanceCalc.calculate(query,exp))
    distances = distances.sortWith((d1,d2) => (d1.distance < d2.distance))
    if (k > 0) {
      distances = distances.take(k)
    }

    /* slower
    var distances = Vector[Distance]()
    for (exp <- experiences) {
      distances = distances :+ distanceCalc.calculateDistance(query.toExperience,exp)
      distances = distances.sortWith((d1,d2) => (d1.distance < d2.distance))
      if ((k > 0) && (distances.length > k)) {
        distances = distances.take(k)
      }
    }
    */

    /* this is much slower than above
    val ordering = Ordering.fromLessThan[Distance]((d1,d2) => {
      if (d1.distance < d2.distance) {
        true
      } else if (d1.distance > d2.distance) {
        false
      } else {
        d1.hashCode < d2.hashCode
      }
    })
    var distances = scala.collection.immutable.TreeSet.empty(ordering)
    for (exp <- experiences) {
      distances += distanceCalc.calculateDistance(query.toExperience,exp)
    }
    distances.take(k)
    */
    /*
    for (exp <- experiences) {
      distances += distanceCalc.calculateDistance(query.toExperience,exp)

      if ((k > 0) && (distances.size > k)) {
        neighbors -= neighbors.last
      }
    }
    */

    distances.toVector
  }

  override def neighborsWithin(query: Query, distance: Float): Vector[Distance] = {
    var distances = experiences.map(exp => distanceCalc.calculate(query,exp))
    distances.filter(d => (d.distance < distance))
    distances.toVector

    /*
    var neighbors = Vector[Distance]()
    for (exp <- experiences) {
      val dist = distanceCalc.calculateDistance(query.toExperience,exp)
      if (dist.distance < distance) {
        neighbors = neighbors :+ dist
      }
    }
    neighbors
    */
  }

  override def getFeatureRange(index: Int): Range = {
    getRange(index)
  }

  override def close(): Unit = {
  }

}
