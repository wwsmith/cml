/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

import cml.learn._
import cml.learn.ibl.distance._
import cml.learn.ibl.exb.mtree.paged._

/*********************************************************************************************************************/

class PagedMTreeExperienceBase(distanceCalc: DistanceCalculator,
                               path: String)
extends ExperienceBase(distanceCalc) with ExperienceBaseUtilities {

  if (distanceCalc.symmetric != true) {
    throw new PredictException("MTreeExperienceBase requires symmetric distance function")
  }
  if (distanceCalc.nonNegative != true) {
    throw new PredictException("MTreeExperienceBase requires nonNegative distances")
  }
  if (distanceCalc.satisfiesTriangleInequality != true) {
    throw new PredictException("MTreeExperienceBase requires a distance func that satisfies the triangle inequality")
  }
  if (distanceCalc.constantDistances != true) {
    throw new PredictException("MTreeExperienceBase requires constant distances")
  }

  val mtree = path match {
    case "" => new MTree(new MemoryStorage(distanceCalc,5))
    case p => {
      new java.io.File(p).exists() match {
        case true => new MTree(new WriteThroughCachedStorage(DiskStorage.open(p)))
        case false => new MTree(new WriteThroughCachedStorage(DiskStorage.create(p,128,distanceCalc)))
      }
    }
  }
  
  override def insert(exp: Experience): Unit = {
    mtree.insert(exp)
  }

  override def nearestNeighbors(query: Query, k: Int): Vector[Distance] = {
    val n1 = mtree.neighbors(query,k).toVector
    n1
  }

  override def neighborsWithin(query: Query, distance: Float): Vector[Distance] = {
    mtree.range(query,distance).toVector
  }

  override def size(): Int = {
    mtree.size()
  }

  override def getFeatureRange(index: Int): Range = {
    getRange(index)
  }

  override def close(): Unit = {
    mtree.store.close()
  }

}
