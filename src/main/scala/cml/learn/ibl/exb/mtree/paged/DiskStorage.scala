/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import java.io.File
import java.io.RandomAccessFile

import scala.collection.immutable.TreeSet

import cml.learn.BooleanFeatureSchema
import cml.learn.DoubleFeatureSchema
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.FeatureSchema
import cml.learn.FloatFeatureSchema
import cml.learn.IntFeatureSchema
import cml.learn.LongFeatureSchema
import cml.learn.StringFeatureSchema
import cml.learn.ibl.distance._

/*********************************************************************************************************************/

object DiskStorage {
  def create(path: String, maxEntriesPerPage: Int, distCalc: DistanceCalculator): DiskStorage = {

    //println("creating with "+schema)

    val f = new File(path)
    if (f.exists()) {
      f.delete()
    }

    val file = new RandomAccessFile(path,"rw")

    val schemaBytes = getBytes(distCalc.schema)
    file.writeInt(schemaBytes.length)
    file.write(schemaBytes)

    val distCalcBytes = getBytes(distCalc)
    file.writeInt(distCalcBytes.length)
    file.write(distCalcBytes)

    file.writeInt(maxEntriesPerPage)
    file.writeLong(-1) // root id
    file.close()

    new DiskStorage(path)
  }

  private def getBytes(schema: ExperienceSchema): Array[Byte] = {
    var str = ""
    for (feature <- schema.features) {
      str += featureString(feature)
    }
    str.getBytes()
  }

  private def featureString(fschema: FeatureSchema): String = {
    val dataType = fschema.featureType()
    fschema.required match {
      case true => fschema.name.name+" "+dataType+" required\n"
      case false => fschema.name.name+" "+dataType+" optional\n"
    }
  }

  private def getBytes(distCalc: DistanceCalculator): Array[Byte] = {
    var str = distCalc match {
      case dc: ManhattanDistanceCalculator => "manhattan\n"
      case dc: EuclideanDistanceCalculator => "euclidean\n"
      case dc: MinkowskyDistanceCalculator => "minkowski\n"
      case dc: HeomDistanceCalculator => "heom\n"
      case dc: DistanceCalculator => throw new Exception("unsupported distance calculator "+dc)
    }

    for (weight <- distCalc.featureWeights) {
      str += "%f\n".format(weight)
    }

    distCalc.scale match {
      case sf: UnitScaleFactor => str += "unit\n"
      case sf: FixedScaleFactor => {
        str += "fixed\n"
        for (factor <- sf.factors) {
          str += "%f\n".format(factor)
        }
      }
      case sf: ScaleFactor => throw new Exception("unsupported scale factor "+sf)
    }

    distCalc match {
      case dc: MinkowskyDistanceCalculator => "%f\n".format(dc.p)
      case _ =>
    }

    str.getBytes()
  }

  def open(path: String): DiskStorage = {
    new DiskStorage(path)
  }

}

/*********************************************************************************************************************/

/*


data file format:
schema - SimpleExperienceReader/Writer
int - maximum entries per page
long - root id
page (# valid entries, an Int, then entries)
page

maybe a command to compact the file if a lot of pages are deleted
maybe a command to see how much a file can be compacted

*/

class DiskStorage(val path: String) extends Storage() {

  // rwd and rws are quite slow
  val file = new RandomAccessFile(path,"rw")
  val rollbackFile = new RandomAccessFile(path+".rollback","rw")
  var inTransaction = false

  val schema = readSchema()
  val distCalc = readDistanceCalculator()
  println(distCalc)

  val maxEntries = file.readInt()

  val rootIdPosition = file.getFilePointer()
  val firstPagePosition = rootIdPosition + 8

  val bytesPerPage = 4 + maxEntries * (bytesPerExperience(schema) + 4 + 8)

  if (rollbackFile.length() > 0) {
    println("rolling back potentially uncompleted transaction...")
    rollback()
    rollbackFile.setLength(0)
  }

  private def readSchema(): ExperienceSchema = {
    val size = file.readInt()
    val bytes = new Array[Byte](size)
    file.readFully(bytes)
    val str = new String(bytes)
    val lines = str.split("\n")

    var schema = new ExperienceSchema()
    schema.features = Vector[FeatureSchema]()
    for (i <- 0 until lines.length - 1) {
      schema.features = schema.features :+ feature(lines(i))
    }

    schema
  }

  private def feature(line: String): FeatureSchema = {
    val toks = line.split(" ")

    val required = toks(2) match {
      case "required" => true
      case "optional" => false
    }

    toks(1) match {
      case "boolean" => new BooleanFeatureSchema(Symbol(toks(0)),required)
      case "int" => new IntFeatureSchema(Symbol(toks(0)),required)
      case "long" => new LongFeatureSchema(Symbol(toks(0)),required)
      case "float" => new FloatFeatureSchema(Symbol(toks(0)),required)
      case "double" => new DoubleFeatureSchema(Symbol(toks(0)),required)
      case str => {
        if (str.startsWith("string")) {
          str.substring(6).toInt match {
            case 0 => {
              println("length of feature "+toks(0)+" is 0 setting it to 20")
              new StringFeatureSchema(Symbol(toks(0)),required,20)
            }
            case i => new StringFeatureSchema(Symbol(toks(0)),required,i)
          }
        } else {
          throw new Exception("unknown feature type: "+str)
        }
      }
    }

  }

  private def readDistanceCalculator(): DistanceCalculator = {
    val size = file.readInt()
    val bytes = new Array[Byte](size)
    file.readFully(bytes)
    val str = new String(bytes)
    val lines = str.split("\n")

    val dcType = lines(0)
    var pos = 1

    var weights = Vector[Float]()
    for(i <- 0 until schema.features.size) {
      weights = weights :+ lines(pos+i).toFloat
    }
    pos += schema.features.size

    pos += 1 // for the scale factor name
    val scale = lines(schema.features.size+1) match {
      case "unit" => new UnitScaleFactor()
      case "fixed" => {
        var factors = Vector[Float]()
        for(i <- 0 until schema.features.size) {
          factors = factors :+ lines(pos+i).toFloat
        }
        pos += schema.features.size
        new FixedScaleFactor(factors)
      }
    }

    dcType match {
      case "manhattan" => new ManhattanDistanceCalculator(schema,weights,scale)
      case "euclidean" => new EuclideanDistanceCalculator(schema,weights,scale)
      case "minkowsky" => new MinkowskyDistanceCalculator(schema,weights,scale,lines(pos).toInt)
      case "heom" => new HeomDistanceCalculator(schema,weights,scale)
      case dct => throw new Exception("unsupported distance calculator: "+dct)
    }
  }

  private def bytesPerExperience(schema: ExperienceSchema): Int = {
    schema.features.map(fs => bytesPerFeature(fs)).fold(0)((b1,b2) => b1 + b2)
  }

  private def bytesPerFeature(fschema: FeatureSchema): Int = {
    fschema match {
      case fs: StringFeatureSchema => 1 + fs.maxLength // seems to be 1 byte/char, but RAF says 2 bytes/char
      case fs: BooleanFeatureSchema => 1 + 1
      case fs: IntFeatureSchema => 1 + 4
      case fs: LongFeatureSchema => 1 + 8
      case fs: FloatFeatureSchema => 1 + 4
      case fs: DoubleFeatureSchema => 1 + 8
      case _ => throw new Exception("unexpected type for feature "+fschema.name)
    }
  }

  def distanceCalculator: DistanceCalculator = {
    distCalc
  }

  def maxEntriesPerPage: Int = {
    maxEntries
  }

  def read(id: Long): Page = {
    val pagePosition = firstPagePosition + bytesPerPage * id
    //println("reading page "+id+" at "+pagePosition)
    if ((id < 0) || (pagePosition + 4 > file.length())) { // 4 bytes for the int saying how many entries in page
      throw new Exception("page "+id+" is unknown")
    }
    file.seek(pagePosition)
    val numEntries = file.readInt()
    //println("  reading "+numEntries+" entries")
    var entries = TreeSet[Entry]()
    for (i <- 0 until numEntries) {
      entries += new Entry(readExperience(file),file.readFloat(),file.readLong())
    }
    val page = new Page(id,entries)
    //println("read "+page)
    page
  }

  def readRoot(): Option[Page] = {
    file.seek(rootIdPosition)
    //println("root id is "+file.readLong())

    file.seek(rootIdPosition)
    file.readLong() match {
      case -1 => None
      case id => Some(read(id))
    }
  }

  def write(page: Page): Page = {
    if (page.entries.size > maxEntries) {
      throw new Exception("page "+page.id+" has too many entries to be written")
    }
    page.id match {
      case -1 => {
        val newId = math.ceil((file.length() - firstPagePosition).toDouble / bytesPerPage).toInt
        write(new Page(newId,page.entries))
      }
      case id => {
        writeRollbackPage(page.id)
        writePage(page)
        page
      }
    }
  }

  def writeRoot(page: Page): Page = {
    writeRollbackRoot()
    val newPage = write(page)
    file.seek(rootIdPosition)
    file.writeLong(newPage.id)
    newPage
  }

  private def writePage(page: Page): Unit = {
    val pagePosition = firstPagePosition + bytesPerPage * page.id
    //println("writing (at "+pagePosition+") "+page)
    file.seek(pagePosition)
    // include a boolean to say if the page is valid?
    file.writeInt(page.entries.size)
    for (entry <- page.entries) {
      //println("    writing entry")
      writeExperience(entry.exp,file)
      file.writeFloat(entry.radius)
      file.writeLong(entry.id)
    }
  }

  private def writeExperience(exp: Experience, file: RandomAccessFile): Unit = {
    for (i <- 0 until exp.length) {
      exp(i) match {
        case None => {
          file.writeBoolean(false)
          schema.features(i) match {
            case fs: StringFeatureSchema => {
              file.write(fs.maxLength)
            }
            case fs: BooleanFeatureSchema => file.writeBoolean(true)
            case fs: IntFeatureSchema => file.writeInt(1)
            case fs: LongFeatureSchema => file.writeLong(1l)
            case fs: FloatFeatureSchema => file.writeFloat(0.0f)
            case fs: DoubleFeatureSchema => file.writeDouble(0.0)
          }
        }
        case Some(v: String) =>
          file.writeBoolean(true)
          schema.features(i) match {
            case fs: StringFeatureSchema => {
              file.writeBytes(("%"+fs.maxLength+"."+fs.maxLength+"s").format(v))
            }
            case _ => throw new Exception("expected a string schema for string value for feature "+
                                          schema.features(i).name)
          }
        case Some(v: Boolean) => {
          file.writeBoolean(true)
          file.writeBoolean(v)
        }
        case Some(v: Int) => {
          file.writeBoolean(true)
          file.writeInt(v)
        }
        case Some(v: Long) => {
          file.writeBoolean(true)
          file.writeLong(v)
        }
        case Some(v: Float) => {
          file.writeBoolean(true)
          file.writeFloat(v)
        }
        case Some(v: Double) => {
          file.writeBoolean(true)
          file.writeDouble(v)
        }
        case Some(_) => throw new Exception("can't handle type of feature "+schema.features(i).name)
      }
    }
  }

  private def readExperience(file: RandomAccessFile): Experience = {
    val features = schema.features.map(fschema => {
      val present = file.readBoolean()
      val value = fschema match {
        case fs: StringFeatureSchema => {
          val bytes = new Array[Byte](fs.maxLength)
          file.readFully(bytes)
          new String(bytes).trim()
        }
        case fs: BooleanFeatureSchema => file.readBoolean()
        case fs: IntFeatureSchema => file.readInt()
        case fs: LongFeatureSchema => file.readLong()
        case fs: FloatFeatureSchema => file.readFloat()
        case fs: DoubleFeatureSchema => file.readDouble()
      }
      present match {
        case true => Some(value)
        case false => None
      }
    })

    new Experience(features)
  }

  def begin(): Unit = {
    if (inTransaction) {
      // alternative is to wait, but exception is probably better for testing
      throw new Exception("trying to start a transaction, but there already is one")
    }
    inTransaction = true
  }

  def commit(): Unit = {
    file.getChannel().force(false)
    rollbackFile.setLength(0)
    inTransaction = false
  }


  private def writeRollbackRoot(): Unit = {
    if (inTransaction) {
      rollbackFile.writeByte(0)  // 0 means old root id
      file.seek(rootIdPosition)
      rollbackFile.writeLong(file.readLong())
      rollbackFile.getChannel().force(false)
    }
  }

  private def writeRollbackPage(id: Long): Unit = {
    if (inTransaction) {
      try {
        val page = read(id)
        rollbackFile.writeByte(2) // 2 means a new page atop an existing page
        rollbackFile.writeLong(id)
        writeRollbackPage(page)
      } catch {
        case e: Exception => {
          // an exception just means that the page doesn't exist yet
          rollbackFile.writeByte(1) // 1 means a new page at the end of the file
          rollbackFile.writeLong(id)
        }
      }
      rollbackFile.getChannel().force(false)
    }
  }

  private def writeRollbackPage(page: Page): Unit = {
    rollbackFile.writeInt(page.entries.size)
    for (entry <- page.entries) {
      writeExperience(entry.exp,rollbackFile)
      rollbackFile.writeFloat(entry.radius)
      rollbackFile.writeLong(entry.id)
    }
  }

  private def readRollbackPage(id: Long): Page = {
    val numEntries = rollbackFile.readInt()
    var entries = TreeSet[Entry]()
    for (i <- 0 until numEntries) {
      entries += new Entry(readExperience(rollbackFile),rollbackFile.readFloat(),rollbackFile.readLong())
    }
    val page = new Page(id,entries)
    page
  }

  private def rollback(): Unit = {
    try {
      val opType = rollbackFile.readByte()
      opType match {
        case 0 => {
          println("updated root id")
          val id = rollbackFile.readLong()
          println("  was "+id)
          rollback()
          // write the old value for the root id
          file.seek(rootIdPosition)
          file.writeLong(id)
        }
        case 1 => {
          println("new page")
          val id = rollbackFile.readLong()
          println("  with id "+id)
          rollback()
          // shrink the file
        }
        case 2 => {
          println("updated page")
          val id = rollbackFile.readLong()
          val page = readRollbackPage(id)
          println(page)
          rollback()
          // write the old value for the page
          write(page)
        }
        case optype => {
          println("unexpected operation type "+optype)
          sys.exit(1)
        }
      }
    } catch {
      case e: java.io.EOFException => {
        // this is fine, it just means that either:
        // * there isn't another rollback operation to do
        // * the current rollback operation didn't get fully written, so the operation it rolls back wasn't performed
      }
    }
  }

  def close(): Unit = {
    file.close()
    rollbackFile.close()
  }

}

