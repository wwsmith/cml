/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import scala.collection.immutable.HashMap

import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.FeatureSchema
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

abstract class CachedStorage(val backEnd: Storage,
                             val cacheSize: Int) extends Storage() {

  var rootId: Long = -1
  var cache = HashMap[Long,CacheEntry]()
  val maxEntries = backEnd.maxEntriesPerPage

  var hits = 0l
  var misses = 0l


  class CacheEntry(val page: Page, var dirty: Boolean = false, var used: Int = 0) {
  }

  protected def get(id: Long): Option[Page] = {
    cache.contains(id) match {
      case true => {
        val ce = cache(id)
        ce.used += 1
        hits += 1
        Some(ce.page)
      }
      case false => {
        misses += 1
        None
      }
    }    
  }

  protected def put(page: Page, dirty: Boolean = false): Unit = {
    cache += (page.id -> new CacheEntry(page, dirty))
    checkCache
  }

  private def checkCache(): Unit = {
    if (cache.size > cacheSize) {
      println("pruning cache...")
      var minUsed = Int.MaxValue
      var maxUsed = 0
      var minId = -1l
      for ((id,ce) <- cache) {
        if (ce.used < minUsed) {
          minUsed = ce.used
          minId = id
        }
        if (ce.used > maxUsed) {
          maxUsed = ce.used
        }
      }

      if (maxUsed > 100) {
        for ((id,ce) <- cache) {
          ce.used = 0
        }
      }

      if (cache(minId).dirty) {
        backEnd.write(cache(minId).page)
      }
      cache -= minId
      println("  pruned")
    }
  }

  def distanceCalculator: DistanceCalculator = {
    backEnd.distanceCalculator
  }

  def maxEntriesPerPage: Int = {
    maxEntries
  }

  override def read(id: Long): Page = {
    get(id) match {
      case Some(page) => page
      case None => {
        val page = backEnd.read(id)
        put(page)
        page
      }
    }    
  }

  override def readRoot(): Option[Page] = {
    rootId match {
      case -1 => {
        val root = backEnd.readRoot()
        root match {
          case None =>
          case Some(page) => rootId = page.id
        }
        root
      }
      case id => Some(read(rootId))
    }
  }

  override def close(): Unit = {
    println("hit rate: "+(hits.toFloat/(hits+misses)))

    for ((id,ce) <- cache) {
      if (ce.dirty) {
        backEnd.write(ce.page)
      }
    }

    backEnd.close()
  }

}
