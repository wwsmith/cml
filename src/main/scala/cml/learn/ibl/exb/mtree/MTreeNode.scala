/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree

import cml.learn.Query
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

abstract class MTreeNode(val tree: MTree, ref: Experience) {
  var reference: Experience = ref
  var parent: Option[MTreeInternalNode] = None
  val nodeId = MTreeNode.getNodeId()
  var radius = 0.0f

  // while we are doing some random selections...
  val random = new scala.util.Random(3456)

  def insert(exp: Experience): Unit
  def delete(exp: Experience): Boolean

  // for debugging
  def sanityCheck(): Unit
  def find(exp: Experience): Boolean
  def exhaustiveFind(exp: Experience): Boolean

  def size(): Int

  def neighbors(query: Query, k: Int, distances: Vector[Distance]): Vector[Distance]
  def range(query: Query, distance: Float, distances: Vector[Distance]): Vector[Distance]

  def all(): Vector[Experience]

  def toString(indent: String): String

  def overlap(exp1: Experience, radius1: Float, exp2: Experience, radius2: Float): Boolean = {
    val dist = tree.distanceCalc.calculate(exp1,exp2)
    (dist.distance <= radius1 + radius2)
  }
}

/*********************************************************************************************************************/

object MTreeNode {
  var curNodeId = 1

  def getNodeId(): Int = {
    curNodeId += 1
    curNodeId - 1
  }
}
