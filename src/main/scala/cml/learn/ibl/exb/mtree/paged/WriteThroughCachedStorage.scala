/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import scala.collection.immutable.HashMap

import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.FeatureSchema
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

class WriteThroughCachedStorage(backEnd: Storage,
                                cacheSize: Int = 1000) extends CachedStorage(backEnd,cacheSize) {

  override def write(page: Page): Page = {
    val newPage = backEnd.write(page)
    put(newPage)
    newPage
  }

  override def writeRoot(page: Page): Page = {
    val newPage = backEnd.writeRoot(page)
    put(newPage)
    rootId = newPage.id
    newPage
  }

  override def begin(): Unit = {
    backEnd.begin()
  }

  override def commit(): Unit = {
    backEnd.commit()
  }

}
