/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

abstract class Storage() {

  def distanceCalculator: DistanceCalculator
  def maxEntriesPerPage: Int

  def read(id: Long): Page
  def readRoot(): Option[Page]
  def write(page: Page): Page      // sets the id in page if it isn't already set
  def writeRoot(page: Page): Page  // writes a page that is the new root page

  // transactions
  def begin(): Unit
  def commit(): Unit

  def close(): Unit
}
