/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import scala.collection.immutable.HashMap

import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

class MemoryStorage(val distCalc: DistanceCalculator, val maxEntries: Int) extends Storage() {

  var rootId: Long = -1
  var curId: Long = 1
  var pages = HashMap[Long,Page]()

  def distanceCalculator: DistanceCalculator = {
    distCalc
  }

  def maxEntriesPerPage: Int = {
    maxEntries
  }

  def read(id: Long): Page = {
    if (!pages.contains(id)) {
      throw new Exception("page "+id+" is unknown")
    }
    pages(id)
  }

  def readRoot(): Option[Page] = {
    rootId match {
      case -1 => None
      case id => Some(read(id))
    }
  }

  def write(page: Page): Page = {
    page.id match {
      case -1 => {
        val newPage = new Page(curId,page.entries)
        pages += (curId -> newPage)
        curId += 1
        newPage
      }
      case id => {
        pages += (page.id -> page)
        page
      }
    }
  }

  def writeRoot(page: Page): Page = {
    val newPage = write(page)
    rootId = newPage.id
    newPage
  }

  def begin(): Unit = {
  }

  def commit(): Unit = {
  }

  def close(): Unit = {
  }
}
