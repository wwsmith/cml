/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree

import cml.learn.Query
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

class MTreeLeafNode(tree: MTree, ref: Experience) extends MTreeNode(tree,ref) {

  var experiences = Vector[Experience](reference)
  //println("new leaf node "+nodeId)

  override def insert(exp: Experience): Unit = {
    insert(exp,true)
  }

  def insert(exp: Experience, update: Boolean = true): Unit = {
    //printf("  insert into child node %d with %d experiences%n",nodeId,experiences.length)
    experiences = experiences :+ exp
    if (experiences.length <= tree.maxSize) {
      if (update) {
        updateRadius()
      }
    } else {
      split()
    }
  }

  private def split(): Unit = {
    //printf("  splitting leaf node %d%n",nodeId)
    //val pair = selectReferencesRandom()
    val pair = selectReferencesFarthest()
    //val pair = selectReferencesMinimizeMaximumRadius()
    split(pair)
  }

  private def selectReferencesRandom(): (Experience, Experience) = {
    (experiences(0),experiences(1))
  }

  private def selectReferencesFarthest(): (Experience, Experience) = {
    var farthestDistance = Float.MinValue
    var bestPair = (experiences(0),experiences(1))
    for (i <- 0 until experiences.length - 1) {
      for (j <- i+1 until experiences.length) {
	val dist = tree.distanceCalc.calculate(experiences(i),experiences(j))
        if (dist.distance > farthestDistance) {
          farthestDistance = dist.distance
          bestPair = (experiences(i),experiences(j))
        }
      }
    }
    bestPair
  }

  private def selectReferencesMinimizeMaximumRadius(): (Experience, Experience) = {
    var bestRadius = Float.MaxValue
    var bestPair = (experiences(0),experiences(1))
    for (i <- 0 until experiences.length - 1) {
      for (j <- i+1 until experiences.length) {
        val radii = experiences.map(exp => {
          math.max(tree.distanceCalc.calculate(experiences(i),exp).distance,
                   tree.distanceCalc.calculate(experiences(j),exp).distance)
        })
        val largestRadius = radii.reduceLeft((r1,r2) => math.max(r1,r2))
        if (largestRadius < bestRadius) {
          bestPair = (experiences(i),experiences(j))
        }
      }
    }
    bestPair
  }

  private def split(pair: Tuple2[Experience,Experience]): Unit = {
    reference = pair._1
    radius = 0.0f
    val oldExperiences = experiences
    experiences = Vector[Experience](reference)
    
    val sibling = new MTreeLeafNode(tree,pair._2)

    for(exp <- oldExperiences) {
      if ((exp != reference) && (exp != sibling.reference)) {
        val dist1 = tree.distanceCalc.calculate(exp,reference)
        val dist2 = tree.distanceCalc.calculate(exp,sibling.reference)
        if (dist1.distance < dist2.distance) {
          insert(exp,false)
        } else if (dist2.distance < dist1.distance) {
          sibling.insert(exp,false)
        } else if (experiences.length <= sibling.experiences.length) {
          insert(exp,false)
        } else {
          sibling.insert(exp,false)
        }
      }
    }

    updateRadius()
    sibling.updateRadius()

    parent match {
      case None => {
        val root = new MTreeInternalNode(tree,reference)
        tree.root = Some(root)
        root.insert(this)
        root.insert(sibling)
      }
      case Some(p) => p.insert(sibling)
    }
  }


  override def delete(exp: Experience): Boolean = {
    val origLength = experiences.length
    experiences = experiences.filter(e => e != exp)
    if (origLength > experiences.length) {
      selectReference()
      true
    } else {
      false
    }
  }

  private def selectReference(): Unit = {
    //selectReferenceRandom()
    selectReferenceMinimizeRadius()
    parent match {
      case None =>
      case Some(p) => p.updateReference()
    }
  }

  private def selectReferenceRandom(): Unit = {
    reference = experiences(random.nextInt(experiences.length))
    updateRadius()
  }

  private def updateRadius(): Unit = {
    val oldRadius = radius
    val radii = experiences.map(exp => tree.distanceCalc.calculate(reference,exp).distance)
    radius = radii.reduceLeft((d1,d2) => math.max(d1,d2))
    if (radius != oldRadius) {
      parent match {
        case None =>
        case Some(p) => p.updateRadius()
      }
    }
  }

  private def selectReferenceMinimizeRadius(): Unit = {
    radius = Float.MaxValue
    for (exp1 <- experiences) {
      val radii = experiences.map(exp2 => tree.distanceCalc.calculate(exp1,exp2).distance)
      val largestRadius = radii.reduceLeft((r1,r2) => math.max(r1,r2))
      if (largestRadius < radius) {
        reference = exp1
        radius = largestRadius
      }
    }
  }

  def sanityCheck(): Unit = {
    if (!sanityCheckRadius()) {
      println(tree)
      sys.exit(1)
    }
  }

  private def sanityCheckRadius(): Boolean = {
    var success = true
    if (radius > 1000000) {
      println("radius is huge: "+this)
      success = false
    }

    for (exp <- experiences) {
      val dist = tree.distanceCalc.calculate(reference,exp)
      if (dist.distance > radius) {
	println("experience "+exp+" with distance "+dist.distance+
		" not within radius "+radius+" of "+reference+"on node "+nodeId)
	success = false
      }
    }
    success
  }

  def find(exp: Experience): Boolean = {
    exhaustiveFind(exp)
  }

  def exhaustiveFind(exp: Experience): Boolean = {
    for (e <- experiences) {
      if (e == exp) {
        return true
      }
    }
    return false
  }

  def size(): Int = {
    experiences.length
  }

  def neighbors(query: Query, k: Int, distances: Vector[Distance]): Vector[Distance] = {
    val nodeDistances = experiences.map(exp => tree.distanceCalc.calculate(query,exp))
    val dists = (distances ++ nodeDistances).sortWith((d1,d2) => d1.distance < d2.distance)
    dists.take(k)
  }

  def range(query: Query, distance: Float, distances: Vector[Distance]): Vector[Distance] = {
    var dists = distances
    for (exp <- experiences) {
      val dist = tree.distanceCalc.calculate(query,exp)
      if (dist.distance < distance) {
        dists = dists :+ dist
      }
    }
    dists
  }

  override def all(): Vector[Experience] = {
    experiences
  }

  override def toString(): String = {
    toString("")
  }

  def toString(indent: String): String = {
    var str = indent+"leaf node "+nodeId+" with "+size+" experiences, radius "+radius+"\n"
    for (exp <- experiences) {
      str = str + indent+"  experience "+exp.hashCode()+"\n"
    }
    str
  }

}
