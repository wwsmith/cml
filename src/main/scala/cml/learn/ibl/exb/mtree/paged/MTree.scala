/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import scala.collection.immutable.SortedSet
import scala.collection.immutable.TreeSet
import scala.collection.immutable.TreeMap

import cml.learn.Query
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

class MTree(val store: Storage) {

  def insert(exp: Experience): Unit = {
    store.begin()
    store.readRoot() match {
      case None => {
        store.writeRoot(new Page(-1,TreeSet[Entry](new Entry(exp))))
      }
      case Some(r) => {
        insert(new Entry(exp,0.0f,r.id),exp) match { // a fake entry is passed in
          case (None, None) =>
          case (Some(entry), None) => 
          case (None, Some(newEntry)) => {
            throw new Exception("this case shouldn't occur!")
          }
          case (Some(entry), Some(newEntry)) => {
            store.writeRoot(new Page(-1,TreeSet[Entry](entry,newEntry)))
          }
        }
      }
    }
    store.commit()
  }

  // entry parameter is the entry for this page in the parent page
  // first return value is a modified version of entry, or None if it wasn't modified
  // second return is a new Entry to add to the parent page, if applicable
  private def insert(entryInParent: Entry, exp: Experience): (Option[Entry], Option[Entry]) = {
    val page = store.read(entryInParent.id)
    page.internal() match {
      case true => {
        val lowestPenalty = page.entries.map(e => penalty(exp,e)).reduce((p1,p2) => if (p1 <= p2) p1 else p2)
        val curExp = lowestPenalty.entry.exp
        val curRadius = lowestPenalty.entry.radius
        insert(lowestPenalty.entry,exp) match {
          case (None, None) => (None, None)
          case (Some(entry), None) => {
            store.write(new Page(page.id,page.entries - lowestPenalty.entry + entry))
            val radius = store.distanceCalculator.calculate(entryInParent.exp,entry.exp).distance + entry.radius
            if (radius > entryInParent.radius) {
              (Some(new Entry(entryInParent.exp,radius,entryInParent.id)), None)
            } else {
              (None, None)
            }
          }
          case (None, Some(newEntry)) => {
            throw new Exception("this case shouldn't occur!")
          }
          case (Some(entry),Some(newEntry)) => {
            val newPage = new Page(page.id,page.entries - lowestPenalty.entry + entry + newEntry)
            if (newPage.entries.size > store.maxEntriesPerPage) {
              split(newPage)
            } else {
              store.write(newPage)
              val d = store.distanceCalculator.calculate(entryInParent.exp,exp).distance
              if (d > entryInParent.radius) {
                (Some(new Entry(entryInParent.exp,d,entryInParent.id)), None)
              } else {
                (None, None)
              }
            }
          }
        }
      }
      case false => {
        val newPage = new Page(page.id,page.entries + new Entry(exp))
        if (newPage.entries.size > store.maxEntriesPerPage) {
          split(newPage)
        } else {
          store.write(newPage)
          val d = store.distanceCalculator.calculate(entryInParent.exp,exp).distance
          if (d > entryInParent.radius) {
            (Some(new Entry(entryInParent.exp,d,entryInParent.id)), None)
          } else {
            (None, None)
          }
        }
      }
    }
  }

  private def penalty(exp: Experience, entry: Entry): Penalty = {
    val d = store.distanceCalculator.calculate(exp,entry.exp).distance
    new Penalty(math.max(d-entry.radius, 0.0f), d, entry)
  }

  class Penalty(val radiusExpand: Float, val distance: Float, val entry: Entry) extends Ordered[Penalty] {
    def compare(that: Penalty): Int = {
      if (radiusExpand < that.radiusExpand) {
        -1
      } else if (radiusExpand > that.radiusExpand) {
        1
      } else {
        if (distance < that.distance) {
          -1
        } else if (distance > that.distance) {
          1
        } else {
          this.hashCode() - that.hashCode()
        }
      }
    }
  }

  private def split(page: Page): (Option[Entry], Option[Entry]) = {
    val pair = selectReferences(page.entries)

    var e1 = TreeSet[Entry]()
    var e2 = TreeSet[Entry]()
    var r1 = 0.0f
    var r2 = 0.0f
    for (e <- page.entries) {
      val cr1 = store.distanceCalculator.calculate(e.exp,pair._1).distance + e.radius
      val cr2 = store.distanceCalculator.calculate(e.exp,pair._2).distance + e.radius
      if (cr1 < cr2) {
        e1 += e
        r1 = math.max(r1,cr1)
      } else if (cr2 < cr1) {
        e2 += e
        r2 = math.max(r2,cr2)
      } else {
        if (e1.size <= e2.size) {
          e1 += e
          r1 = math.max(r1,cr1)
        } else {
          e2 += e
          r2 = math.max(r2,cr2)
        }
      }
    }

    val p1 = store.write(new Page(page.id,e1))
    val p2 = store.write(new Page(-1,e2))

    (Some(new Entry(pair._1,r1,p1.id)), Some(new Entry(pair._2,r2,p2.id)))
  }

  private def selectReference(entries: SortedSet[Entry]): Experience = {
    selectReferenceRandom(entries)
  }

  private def selectReferenceRandom(entries: SortedSet[Entry]): Experience = {
    entries.head.exp
  }

  private def selectReferenceMinimizeRadius(entriesSet: SortedSet[Entry]): Experience = {
    val entries = entriesSet.toArray
    var bestRadius = Float.MaxValue
    var bestReference = entries(0).exp
    for (i <- 0 until entries.size - 1) {
      val radius = entries.map(e => math.max(entries(i).radius,store.distanceCalculator.calculate(entries(i).exp,e.exp).distance+e.radius)).reduce((r1,r2)=>math.max(r1,r2))
      if (radius < bestRadius) {
        bestRadius = radius
        bestReference = entries(i).exp
      }
    }
    bestReference
  }

  private def selectReferences(entries: SortedSet[Entry]): (Experience, Experience) = {
    selectReferencesFarthest(entries)
  }

  private def selectReferencesRandom(entries: SortedSet[Entry]): (Experience, Experience) = {
    (entries.head.exp,entries.last.exp)
  }

  private def selectReferencesFarthest(entriesSet: SortedSet[Entry]): (Experience, Experience) = {
    val entries = entriesSet.toArray
    var farthestDistance = Float.MinValue
    var bestPair = (entries(0).exp,entries(1).exp)
    for (i <- 0 until entries.length - 1) {
      for (j <- i+1 until entries.length) {
	val distance = store.distanceCalculator.calculate(entries(i).exp,entries(j).exp).distance
        if (distance > farthestDistance) {
          farthestDistance = distance
          bestPair = (entries(i).exp,entries(j).exp)
        }
      }
    }
    bestPair
  }

  private def selectReferencesMinimizeMaximumRadius(entriesSet: SortedSet[Entry]): (Experience, Experience) = {
    val entries = entriesSet.toArray
    var bestRadius = Float.MaxValue
    var bestPair = (entries(0).exp,entries(1).exp)
    for (i <- 0 until entries.length - 1) {
      for (j <- i+1 until entries.length) {
        val radii = entries.map(entry => {
          math.max(store.distanceCalculator.calculate(entries(i).exp,entry.exp).distance + entries(i).radius,
                   store.distanceCalculator.calculate(entries(j).exp,entry.exp).distance + entries(j).radius)
        })
        val largestRadius = radii.reduceLeft((r1,r2) => math.max(r1,r2))
        if (largestRadius < bestRadius) {
          bestPair = (entries(i).exp,entries(j).exp)
        }
      }
    }
    bestPair
  }

  def delete(exp: Experience): Unit = {
    store.readRoot() match {
      case None => new Exception("no experiences to delete from")
      case Some(root) => delete(root,exp)
    }
  }

  private def delete(page: Page, exp: Experience): Unit = {
    println("delete not yet implemented")
  }

  def neighbors(query: Query, k: Int): SortedSet[Distance] = {
    val distances = TreeSet[Distance]()
    store.readRoot() match {
      case None => distances
      case Some(root) => neighbors(root,query,k,distances)
    }
  }

  private def neighbors(page: Page, query: Query, k: Int, soFar: SortedSet[Distance]): SortedSet[Distance] = {
    var sf = soFar
    page.internal() match {
      case true => {
        var searchOrder = TreeMap[Distance,Entry]()
        for (entry <- page.entries) {
          searchOrder += (store.distanceCalculator.calculate(query,entry.exp) -> entry)
        }
        for ((d,e) <- searchOrder) {
          sf.lastOption match {
            case None => {
              sf = neighbors(store.read(e.id),query,k,sf)
            }
            case Some(distance) => {
              if ((sf.size < k) || (d.distance - e.radius < distance.distance)) {
                sf = neighbors(store.read(e.id),query,k,sf)
              }
            }
          }
        }
        sf
      }
      case false => {
        for (entry <- page.entries) {
          sf += store.distanceCalculator.calculate(query,entry.exp)
        }
        sf.take(k)
      }
    }
  }

  def exhaustiveNeighbors(query: Query, k: Int): SortedSet[Distance] = {
    val distances = TreeSet[Distance]()
    store.readRoot() match {
      case None => distances
      case Some(root) => exhaustiveNeighbors(root,query,k,distances)
    }
  }

  private def exhaustiveNeighbors(page: Page,
                                  query: Query,
                                  k: Int,
                                  soFar: SortedSet[Distance]): SortedSet[Distance] = {
    var sf = soFar
    page.internal() match {
      case true => {
        for (entry <- page.entries) {
          sf = exhaustiveNeighbors(store.read(entry.id),query,k,sf)
        }
        sf
      }
      case false => {
        for (entry <- page.entries) {
          sf += store.distanceCalculator.calculate(query,entry.exp)
        }
        sf.take(k)
      }
    }
  }


  def range(query: Query, distance: Float): SortedSet[Distance] = {
    store.readRoot() match {
      case None => TreeSet[Distance]()
      case Some(root) => range(root,query,distance)
    }
  }

  def range(page: Page, query: Query, distance: Float): SortedSet[Distance] = {
    page.internal() match {
      case true => {
        val candidates =
          page.entries.filter(entry =>
            store.distanceCalculator.calculate(query,entry.exp).distance - entry.radius < distance)
        var withinRange = SortedSet[Distance]()
        for (entry <- candidates) {
          withinRange ++= range(store.read(entry.id),query,distance)
        }
        //candidates.map(entry => range(store.read(entry.id),query,distance)).flatten
        withinRange
      }
      case false => {
        /*
        var dvec = Vector[Distance]()
        for (entry <- page.entries) {
          val d = store.distanceCalculator.calculate(query,entry.exp).distance
          if (d <= distance) {
            dvec = dvec :+ d
          }
        }
        dvec
        */
        page.entries.map(entry =>
          store.distanceCalculator.calculate(query,entry.exp)).filter(d => d.distance < distance)
      }
    }
  }

  def sanityCheck(): Boolean = {
    store.readRoot() match {
      case None => true
      case Some(root) => {
        sanityCheckRadius(root)
      }
    }
  }

  def sanityCheckRadius(parent: Page): Boolean = {
    var ret = true
    for (e <- parent.entries) {
      e.id match {
        case -1 =>
        case id => {
          val child = store.read(id)
          val calcRadius = child.entries.map(ce => store.distanceCalculator.calculate(e.exp,ce.exp).distance + ce.radius).reduce((r1,r2) => math.max(r1,r2))
          if (math.abs(e.radius -calcRadius) > 0.00000001) {
            println("child "+id+" in page "+parent.id+", calculated radius "+calcRadius+" doesn't match stored radius "+e.radius)
            ret = false
          }
        }
        for (e <- parent.entries) {
          val child = store.read(e.id)
          if (sanityCheckRadius(child) == false) {
            ret = false
          }
        }
      }
    }
    ret
  }

  def size(): Int = {
    store.readRoot() match {
      case None => 0
      case Some(root) => size(root)
    }
  }

  def size(page: Page): Int = {
    page.internal() match {
      case true => {
        page.entries.map(entry => size(store.read(entry.id))).fold(0)((e1,e2) => e1 + e2)
      }
      case false => {
        page.entries.size
      }
    }
  }

  def all(): Vector[Experience] = {
    store.readRoot() match {
      case None => Vector[Experience]()
      case Some(root) => all(root)
    }
  }

  def all(page: Page): Vector[Experience] = {
    page.internal() match {
      case true => {
        var entries = Vector[Entry]()
        for (e <- page.entries) {
          entries = entries :+ e
        }
        entries.map(entry => all(store.read(entry.id))).flatten
      }
      case false => {
        var exps = Vector[Experience]()
        for (entry <- page.entries) {
          exps = exps :+ entry.exp
        }
        exps
      }
    }
  }

  override def toString(): String = {
    toString("")
  }

  def toString(indent: String): String = {
    indent+" toString not implemented"
  }
}
