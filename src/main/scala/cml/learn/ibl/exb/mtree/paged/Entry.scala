/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import cml.learn.Experience

/*********************************************************************************************************************/

// Entries in leaf Pages have a radius of 0.0f and a (Page) id of -1

class Entry(val exp: Experience,
            val radius: Float = 0.0f,
            val id: Long = -1) extends Ordered[Entry] {
  def compare(that: Entry): Int = {
    this.id match {
      case -1 => { // entry is in a leaf page
        if (this.exp == that.exp) {
          0
        } else {
          this.hashCode() - that.hashCode()
        }
      }
      case id => { // entry is in an internal page
        if (this.id < that.id) {
          -1
        } else if (this.id > that.id) {
          1
        } else {
          0
        }
      }
    }
  }
}
