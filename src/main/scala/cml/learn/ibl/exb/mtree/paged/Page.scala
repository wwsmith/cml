/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree.paged

import scala.collection.immutable.SortedSet
import scala.collection.immutable.TreeSet

/*********************************************************************************************************************/

// leaf pages have entries with id == -1

class Page(val id: Long = -1,
           val entries: SortedSet[Entry] = TreeSet[Entry]()) {
  def internal(): Boolean = {
    if (entries.size == 0) {
      throw new Exception("no entries in page")
    }
    if (entries.head.id == -1) {
      false
    } else {
      true
    }
  }

  def leaf(): Boolean = {
    !internal()
  }

  override def toString(): String = {
    toString("")
  }

  def toString(indent: String): String = {
    var str = indent+"Page "+id+"\n"
    str += indent+"  "+entries.size+" Entries:"
    for (entry <- entries) {
      str += " "+entry.id
    }
    str
  }
}

/*
class Page(val id: Long = -1,
           val entries: Vector[Entry] = Vector[Entry]()) {
  def internal(): Boolean = {
    if (entries.length == 0) {
      throw new Exception("no entries in page")
    }
    if (entries(0).id == -1) {
      false
    } else {
      true
    }
  }

  def leaf(): Boolean = {
    !internal()
  }
}
*/
