/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree

import cml.learn.Query
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

class MTreeInternalNode(tree: MTree, ref: Experience) extends MTreeNode(tree,ref) {

  var children = Vector[MTreeNode]()
  //println("new internal node "+nodeId)

  override def insert(exp: Experience): Unit = {
    //printf("insert into internal node %d with %d children%n",nodeId,children.length)
    val dists = children.map(child => (child,tree.distanceCalc.calculate(exp,child.reference)))
    val closest = dists.reduceLeft((t1,t2) => {
      if (t2._2.distance < t1._2.distance) {
        t2
      } else {
        t1
      }
    })

    closest._1.insert(exp)
  }

  def insert(node: MTreeNode, update: Boolean = true): Unit = {
    children = children :+ node
    //printf("  insert node %d into internal node %d resulting in %d children%n",node.nodeId,nodeId,children.length)
    node.parent = Some(this)

    if (children.length <= tree.maxSize) {
      if (update) {
        updateRadius()
      }
    } else {
      split()
    }
  }

  def delete(node: MTreeNode): Unit = {
    children = children.filter(n => n != node)
    //printf("  delete node %d from internal node %d resulting in %d children%n",node.nodeId,nodeId,children.length)
    if (children.length == 0) {
      parent match {
        case None => // ?
        case Some(p) => p.delete(this)
      }
    } else {
      updateReference()
    }
  }

  def updateRadius(): Unit = {
    val oldRadius = radius
    val radii = children.map(child =>
      tree.distanceCalc.calculate(reference,child.reference).distance + child.radius)
    radius = radii.reduceLeft((r1,r2) => math.max(r1,r2))
    if (radius != oldRadius) {
      parent match {
        case None =>
        case Some(p) => p.updateRadius()
      }
    }
  }

  private def split(): Unit = {
    //printf("  splitting internal node %d%n",nodeId)
    //val pair = selectReferencesRandom()
    val pair = selectReferencesFarthest()
    //val pair = selectReferencesMinimizeMaximumRadius()
    split(pair)
  }

  private def selectReferencesRandom(): (Experience, Experience) = {
    (children(0).reference,children(1).reference)
  }

  private def selectReferencesFarthest(): (Experience, Experience) = {
    var farthestDistance = Float.MinValue
    var bestPair = (children(0).reference,children(1).reference)
    for (i <- 0 until children.length - 1) {
      for (j <- i+1 until children.length) {
	val dist = tree.distanceCalc.calculate(children(i).reference,children(j).reference)
        if (dist.distance > farthestDistance) {
          farthestDistance = dist.distance
          bestPair = (children(i).reference,children(j).reference)
        }
      }
    }
    bestPair
  }

  private def selectReferencesMinimizeMaximumRadius(): (Experience, Experience) = {
    var bestRadius = Float.MaxValue
    var bestPair = (children(0).reference,children(1).reference)
    for (i <- 0 until children.length - 1) {
      for (j <- i+1 until children.length) {
        val radii = children.map(node => {
          math.max(tree.distanceCalc.calculate(children(i).reference,node.reference).distance + children(i).radius,
                   tree.distanceCalc.calculate(children(j).reference,node.reference).distance + children(j).radius)
        })
        val largestRadius = radii.reduceLeft((r1,r2) => math.max(r1,r2))
        if (largestRadius < bestRadius) {
          bestPair = (children(i).reference,children(j).reference)
        }
      }
    }
    bestPair
  }

  private def split(pair: (Experience,Experience)): Unit = {
    reference = pair._1
    radius = 0.0f
    val oldChildren = children
    children = Vector[MTreeNode]()

    //println("    creating sibling")
    val sibling = new MTreeInternalNode(tree,pair._2)

    for(child <- oldChildren) {
      val dist1 = tree.distanceCalc.calculate(child.reference,reference)
      val dist2 = tree.distanceCalc.calculate(child.reference,sibling.reference)
      if (dist1.distance < dist2.distance) {
        insert(child,false)
      } else if (dist2.distance < dist1.distance) {
        sibling.insert(child,false)
      } else if (children.length <= sibling.children.length) {
        insert(child,false)
      } else {
        sibling.insert(child,false)
      }
    }

    updateRadius()
    sibling.updateRadius()

    //printf("  siblings: %d with %d children, %d with %d children%n",
    //       nodeId,children.length,sibling.nodeId,sibling.children.length)

    parent match {
      case None => {
        //println("    creating new root node")
        val root = new MTreeInternalNode(tree,pair._1)
        tree.root = Some(root)
        root.insert(this)
        root.insert(sibling)
      }
      case Some(p) => {
        p.insert(sibling)
      }
    }
  }

  override def delete(exp: Experience): Boolean = {
    for (child <- children) {
      val dist = tree.distanceCalc.calculate(exp,child.reference)
      if (dist.distance < child.radius) {
        if (child.delete(exp)) {
          return true
        }
      }
    }
    return false
  }

  def updateReference(): Unit = {
    //selectReferenceRandom()
    selectReferenceMinimizeRadius()
    parent match {
      case None =>
      case Some(p) => p.updateReference()
    }
  }

  private def selectReferenceRandom(): Unit = {
    reference = children(random.nextInt(children.length)).reference
    updateRadius()
  }

  private def selectReferenceMinimizeRadius(): Unit = {
    radius = Float.MaxValue
    for (child1 <- children) {
      val radii = children.map(child2 => {
        val dist = tree.distanceCalc.calculate(child1.reference,child2.reference)
        child1.radius + dist.distance + child2.radius
      })
      val largestRadius = radii.reduceLeft((r1,r2) => math.max(r1,r2))
      if (largestRadius < radius) {
        reference = child1.reference
        radius = largestRadius
      }
    }
  }

  def sanityCheck(): Unit = {
    if (!sanityCheckRadius() || !sanityCheckParent()) {
      println(tree)
      sys.exit(1)
    }
    for (child <- children) {
      child.sanityCheck()
    }
  }

  private def sanityCheckRadius(): Boolean = {
    var success = true
    if (radius > 1000000) {
      println("radius is huge: "+this)
      success = false
    }

    for (child <- children) {
      val dist = tree.distanceCalc.calculate(reference,child.reference)
      if (dist.distance + child.radius > radius) {
	println("child "+child.nodeId+" with radius "+child.radius+" is "+
		dist.distance+" from reference and not within radius "+radius+
		" of parent "+nodeId)
	success = false
      }
    }
    success
  }

  private def sanityCheckParent(): Boolean = {
    parent match {
      case None => true
      case Some(p) => p.hasChild(this)
    }
  }

  private def hasChild(node: MTreeNode): Boolean = {
    for (child <- children) {
      if (node == child) {
        return true
      }
    }
    return false
  }

  override def find(exp: Experience): Boolean = {
    for (child <- children) {
      val dist = tree.distanceCalc.calculate(exp,child.reference)
      if (child.find(exp)) {
        return true
      }
    }
    return false
  }

  override def exhaustiveFind(exp: Experience): Boolean = {
    for (child <- children) {
      if (child.find(exp)) {
        return true
      }
    }
    return false
  }

  override def size(): Int = {
    children.map(child => child.size()).reduceLeft((size1,size2) => size1 + size2)
  }

  def neighbors(query: Query, k: Int, distances: Vector[Distance]): Vector[Distance] = {
    /*
    val sortedChildren = children.sort((c1,c2) => {
      val d1 = tree.distanceCalc.calculate(query,c1.reference).distance
      val d2 = tree.distanceCalc.calculate(query,c2.reference).distance
      d1 < d2
    })
    */

    val cds = children.map(child => (child,tree.distanceCalc.calculate(query,child.reference))).sortWith((p1,p2) =>
      (p1._2.distance < p2._2.distance))
    var dists = distances
    for (cd <- cds) {
      if (distances.length < k) {
        dists = cd._1.neighbors(query,k,dists)
      } else if (dists.last.distance > cd._2.distance - cd._1.radius) {
        dists = cd._1.neighbors(query,k,dists)
      }
    }

    /*
    var dists = distances
    for (child <- children) {
      val dist = tree.distanceCalc.calculate(query,child.reference)
      if ((distances.length < k) || (dist.distance - child.radius < distances.last.distance)) {
        dists = child.neighbors(query,k,dists)
      }
    }
    */

    dists
  }

  def range(query: Query, distance: Float, distances: Vector[Distance]): Vector[Distance] = {
    var dists = distances
    for (child <- children) {
      val dist = tree.distanceCalc.calculate(query,child.reference)
      if (dist.distance - child.radius < distance) {
        dists = child.range(query,distance,dists)
      }
    }
    dists
  }

  override def all(): Vector[Experience] = {
    var experiences = Vector[Experience]()
    for (child <- children) {
      experiences = experiences ++ child.all()
    }
    experiences
    //children.map(child => child.all()).reduceLeft((vec1,vec2) => vec1 ++ vec2)
  }

  override def toString(): String = {
    toString("")
  }

  def toString(indent: String): String = {
    var str = indent+"internal node "+nodeId+" with "+size+" children, radius "+radius+"\n"
    for (child <- children) {
      str += child.toString(indent+"  ")
    }
    str
  }

}
