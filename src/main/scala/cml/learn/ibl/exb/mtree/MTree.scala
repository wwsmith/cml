/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.mtree

import cml.learn.Query
import cml.learn.Experience
import cml.learn.ExperienceSchema
import cml.learn.ibl.distance.Distance
import cml.learn.ibl.distance.DistanceCalculator

/*********************************************************************************************************************/

class MTree(val distanceCalc: DistanceCalculator, val maxSize: Int = 5) {

  var root: Option[MTreeNode] = None

  def insert(exp: Experience): Unit = {
    root match {
      case None => {
        val r = new MTreeLeafNode(this,exp)
        root = Some(r)
      }
      case Some(r) => r.insert(exp)
    }
  }

  def delete(exp: Experience): Unit = {
    root match {
      case None =>
      case Some(r) => r.delete(exp)
    }
  }

  def neighbors(query: Query, k: Int): Vector[Distance] = {
    val distances = Vector[Distance]()
    root match {
      case None => Vector[Distance]()
      case Some(r) => r.neighbors(query,k,distances)
    }
  }

  def range(query: Query, distance: Float): Vector[Distance] = {
    val distances = Vector[Distance]()
    root match {
      case None => Vector[Distance]()
      case Some(r) => r.range(query,distance,distances)
    }
  }

  def size(): Int = {
    root match {
      case None => 0
      case Some(r) => r.size()
    }
  }

  def all(): Vector[Experience] = {
    root match {
      case None => Vector[Experience]()
      case Some(r) => r.all()
    }
  }

  override def toString(): String = {
    toString("")
  }

  def toString(indent: String): String = {
    root match {
      case None => ""
      case Some(r) => r.toString(indent)
    }
  }
}
