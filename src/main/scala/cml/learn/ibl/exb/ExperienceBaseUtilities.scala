/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

import cml.learn.Experience

/*********************************************************************************************************************/

trait ExperienceBaseUtilities {

  var distributions = Vector[Distribution]()

  private def getDistribution(index: Int): Distribution = {
    if (distributions.length < index) {
      new Distribution()
    } else {
      distributions(index)
    }
  }

  def addToDistributions(exp: Experience): Unit = {
    if (distributions.length == 0) {
      distributions = Vector.fill[Distribution](exp.length)(new Distribution())
    }
    for (i <- List.range(0,exp.length)) {
      exp(i) match {
        case Some(value: Int) => distributions(i).add(value.toFloat)
        case Some(value: Long) => distributions(i).add(value.toFloat)
        case Some(value: Float) => distributions(i).add(value.toFloat)
        case Some(value: Double) => distributions(i).add(value.toFloat)
        case _ => // do nothing
      }
    }
  }

  def removeFromDistributions(exp: Experience): Unit = {
    for (i <- List.range(0,exp.length)) {
      exp(i) match {
        case Some(value: Int) => distributions(i).remove(value.toFloat)
        case Some(value: Long) => distributions(i).remove(value.toFloat)
        case Some(value: Float) => distributions(i).remove(value.toFloat)
        case Some(value: Double) => distributions(i).remove(value.toFloat)
        case _ => // do nothing
      }
    }
  }

  /**********/

  var ranges = Vector[Range]()

  def getRange(index: Int): Range = {
    if (ranges.length < index) {
      new Range()
    } else {
      ranges(index)
    }
  }

  def addToRanges(exp: Experience): Unit = {
    if (ranges.length == 0) {
      ranges = Vector.fill[Range](exp.length)(new Range())
    }
    for(i <- List.range(0,exp.length)) {
      exp(i) match {
        case Some(value: Int) => ranges(i).add(value.toFloat)
        case Some(value: Long) => ranges(i).add(value.toFloat)
        case Some(value: Float) => ranges(i).add(value.toFloat)
        case Some(value: Double) => ranges(i).add(value.toFloat)
        case _ => // ignore String, Boolean, or anything else
      }
    }
  }

  def removeFromRanges(exp: Experience): Unit = {
  }
}
