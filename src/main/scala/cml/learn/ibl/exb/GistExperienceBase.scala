/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb

import cml.learn._
import cml.learn.ibl.distance._
import cml.learn.ibl.exb.gist._
import cml.gist._

/*********************************************************************************************************************/

class GistExperienceBase(distanceCalc: DistanceCalculator)
extends ExperienceBase(distanceCalc) with ExperienceBaseUtilities {

  if (distanceCalc.symmetric != true) {
    throw new PredictException("GistExperienceBase requires symmetric distance function")
  }
  if (distanceCalc.nonNegative != true) {
    throw new PredictException("GistExperienceBase requires nonNegative distances")
  }
  if (distanceCalc.satisfiesTriangleInequality != true) {
    throw new PredictException("GistExperienceBase requires a distance func that satisfies the triangle inequality")
  }
  if (distanceCalc.constantDistances != true) {
    throw new PredictException("GistExperienceBase requires constant distances")
  }

  // native mtree iplementation uses 5
  val mtree = new MTree(100,new MemoryDataStorage[ExpDatum](),new MemoryGistStorage[RadiusPredicate](),distanceCalc)
  
  override def insert(exp: Experience): Unit = {
    val datum = new ExpDatum(exp,distanceCalc.schema)
    mtree.insert(new RadiusPredicate(datum,0.0f),datum)
  }

  override def nearestNeighbors(query: Query, k: Int): Vector[Distance] = {
    var distances = Vector[Distance]()
    for ((k,v) <- mtree.best(new RadiusPredicate(new ExpDatum(Experience(query),distanceCalc.schema),0.0f),k)) {
      distances = distances :+ new Distance(Experience(query),v.exp,k.distance)
    }
    distances.toVector
  }

  override def neighborsWithin(query: Query, distance: Float): Vector[Distance] = {
    mtree.search(new RadiusPredicate(new ExpDatum(Experience(query),distanceCalc.schema),distance)).map(datum =>
      distanceCalc.calculate(Experience(query),datum.exp)).toVector
  }

  override def size(): Int = {
    mtree.size()
  }

  override def getFeatureRange(index: Int): Range = {
    getRange(index)
  }

  override def close(): Unit = {
  }

}
