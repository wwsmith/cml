/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.gist

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream

import cml.gist.Predicate

/*********************************************************************************************************************/

class RadiusPredicate(var reference: ExpDatum, var radius: Float) extends Predicate {
  def numBytes(): Int = {
    reference.numBytes() + 4
  }

  def toBytes(): Array[Byte] = {
    val baos = new ByteArrayOutputStream()
    val dos = new DataOutputStream(baos)
    val referenceBytes = reference.toBytes()
    dos.write(referenceBytes,0,referenceBytes.length)
    dos.writeFloat(radius)
    dos.close() // ?
    baos.toByteArray()
  }

  def fromBytes(bytes: Array[Byte]): Unit = {
    reference.fromBytes(bytes)
    var radiusBytes = new Array[Byte](4)
    radiusBytes.update(0,bytes(bytes.length-4))
    radiusBytes.update(1,bytes(bytes.length-3))
    radiusBytes.update(2,bytes(bytes.length-2))
    radiusBytes.update(3,bytes(bytes.length-1))
    val bais = new ByteArrayInputStream(radiusBytes)
    val dis = new DataInputStream(bais)
    radius = dis.readFloat()
    dis.close()
  }
}
