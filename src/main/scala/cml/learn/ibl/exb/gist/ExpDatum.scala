/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.gist

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream

import cml.gist.Datum
import cml.learn._

/*********************************************************************************************************************/

class ExpDatum(var exp: Experience, val schema: ExperienceSchema, id: Long = -1) extends Datum(id) {

  def numBytes(): Int = {
    numBytes(schema)
  }

  def numBytes(schema: ExperienceSchema): Int = {
    schema.features.map(fs => numBytes(fs)).fold(0)((b1,b2) => b1 + b2)
  }

  def numBytes(fschema: FeatureSchema): Int = {
    fschema match {
      case fs: StringFeatureSchema => fs.maxLength * 2 // 2 bytes per character?
      case fs: BooleanFeatureSchema => 1
      case fs: IntFeatureSchema => 4
      case fs: LongFeatureSchema => 8
      case fs: FloatFeatureSchema => 4
      case fs: DoubleFeatureSchema => 8
    }
  }

  def toBytes(): Array[Byte] = {
    val baos = new ByteArrayOutputStream()
    val dos = new DataOutputStream(baos)

    for (index <- 0 until exp.length) {
      writeFeature(exp(index),schema(index),dos)
    }

    dos.close() // ?
    baos.toByteArray()
  }

  def writeFeature(feature: Option[Any], schema: FeatureSchema, dos: DataOutputStream): Unit = {
    feature match {
      case None => {
        dos.writeBoolean(false)
        schema match {
          case fs: StringFeatureSchema => dos.writeUTF(" "*fs.maxLength)
          case fs: BooleanFeatureSchema => dos.writeBoolean(true)
          case fs: IntFeatureSchema => dos.writeInt(1)
          case fs: LongFeatureSchema => dos.writeLong(1)
          case fs: FloatFeatureSchema => dos.writeFloat(1.0f)
          case fs: DoubleFeatureSchema => dos.writeDouble(1.0)
        }
      }
      case Some(v) => {
        dos.writeBoolean(true)
        v match {
          case s: String => {
            val numChars = numBytes(schema) / 2
            if (s.length < numChars) {
              dos.writeChars(s)
              dos.writeChars(" "*(numChars - s.length))
            } else if (s.length > numChars) {
              dos.writeChars(s.substring(0,numChars))
            } else {
              dos.writeChars(s)
            }
          }
          case b: Boolean => dos.writeBoolean(b)
          case i: Int => dos.writeInt(i)
          case l: Long => dos.writeLong(l)
          case f: Float => dos.writeFloat(f)
          case d: Double => dos.writeDouble(d)
        }
      }
    }
  }

  def fromBytes(bytes: Array[Byte]): Unit = {
    val bais = new ByteArrayInputStream(bytes)
    val dis = new DataInputStream(bais)

    val features = schema.features.map(fschema => {
      dis.readBoolean() match {
        case false => None
        case true => {
          fschema match {
            case fs: StringFeatureSchema => {
              val numChars = numBytes(fschema) / 2
              var carr = Array[Char]()
              for (i <- 0 until numChars) {
                carr.update(i,dis.readChar)
              }
              Some(new String(carr).trim())
            }
            case fs: BooleanFeatureSchema => Some(dis.readBoolean())
            case fs: IntFeatureSchema => Some(dis.readInt())
            case fs: LongFeatureSchema => Some(dis.readLong())
            case fs: FloatFeatureSchema => Some(dis.readFloat())
            case fs: DoubleFeatureSchema => Some(dis.readDouble())
          }
        }
      }
    })
    exp = new Experience(features)
  }

}
