/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.exb.gist

import cml.learn.ibl.distance.DistanceCalculator
import cml.gist._

/*********************************************************************************************************************/

class MTree(branchFactor: Int,
            dataStore: DataStorage[ExpDatum],
            gistStore: GistStorage[RadiusPredicate],
            distCalc: DistanceCalculator)
extends Gist[ExpDatum,RadiusPredicate,ExpandPenalty,FloatDistance](branchFactor, dataStore, gistStore) {

  val random = new scala.util.Random(3456)

  def consistent(p1: RadiusPredicate, p2: RadiusPredicate): Boolean = {
    distCalc.calculate(p1.reference.exp,p2.reference.exp).distance < p1.radius + p2.radius
  }

  def distance(p1: RadiusPredicate, p2: RadiusPredicate): FloatDistance = {
    new FloatDistance(distCalc.calculate(p1.reference.exp,p2.reference.exp).distance - p1.radius - p2.radius)
  }

  def union(predicates: Vector[RadiusPredicate]): RadiusPredicate = {
    randomUnion(predicates)
  }

  private def randomUnion(predicates: Vector[RadiusPredicate]): RadiusPredicate = {
    calculatePredicate(predicates(0),predicates)
  }

  private def minimizeRadiusUnion(predicates: Vector[RadiusPredicate]): RadiusPredicate = {
    predicates.map(p => calculatePredicate(p,predicates)).reduce((p1,p2) =>
      if (p1.radius < p2.radius) {
        p1
      } else {
        p2
      })
  }

  private def calculatePredicate(from: RadiusPredicate, tos: Vector[RadiusPredicate]): RadiusPredicate = {
    new RadiusPredicate(from.reference,tos.map(to => radius(from,to)).fold(0.0f)((r1,r2) => math.max(r1,r2)))
  }

  private def radius(from: RadiusPredicate, to: RadiusPredicate): Float = {
    distCalc.calculate(from.reference.exp,to.reference.exp).distance + from.radius + to.radius
  }

  def penalty(p1: RadiusPredicate, p2: RadiusPredicate): ExpandPenalty = {
    val dist = distCalc.calculate(p1.reference.exp,p2.reference.exp).distance
    val radiusExpand = dist + math.max(p1.radius,p2.radius)
    new ExpandPenalty(radiusExpand,dist)
  }

  def pickSplit(entries: Vector[Entry[RadiusPredicate]]):
                (Vector[Entry[RadiusPredicate]], Vector[Entry[RadiusPredicate]]) = {
    val epair = pickRandomReferences(entries)
    //val epair = pickFarthestReferences(entries)

    var e1 = Vector[Entry[RadiusPredicate]]()
    var e2 = Vector[Entry[RadiusPredicate]]()
    for (entry <- entries) {
      val p1 = penalty(entry.key,epair._1.key)
      val p2 = penalty(entry.key,epair._2.key)
      if (p1 < p2) {
        e1 = e1 :+ entry
      } else if (p2 < p1) {
        e2 = e2 :+ entry
      } else {
        if (e1.length < e2.length) {
          e1 = e1 :+ entry
        } else {
          e2 = e2 :+ entry
        }
      }
    }

    // can only retry with random references
    if ((e1.length < branchFactor/4) || (e2.length < branchFactor/4)) {
      //println("    retrying split")
      pickSplit(entries)
    } else {
      (e1,e2)
    }
  }

  private def pickRandomReferences(entries: Vector[Entry[RadiusPredicate]]):
                (Entry[RadiusPredicate], Entry[RadiusPredicate]) = {
    //(entries(0),entries(1))
    val index1 = random.nextInt(entries.length)
    var index2 = random.nextInt(entries.length)
    while (index2 == index1) {
       index2 = random.nextInt(entries.length)
    }
    (entries(index1),entries(index2))
  }

  private def pickFarthestReferences(entries: Vector[Entry[RadiusPredicate]]):
                (Entry[RadiusPredicate], Entry[RadiusPredicate]) = {
    var farthestDistance = Float.MinValue
    var bestPair = (entries(0),entries(1))
    for (i <- 0 until entries.length - 1) {
      for (j <- i+1 until entries.length) {
        val dist = distCalc.calculate(entries(i).key.reference.exp,entries(j).key.reference.exp)
        if (dist.distance > farthestDistance) {
          farthestDistance = dist.distance
          bestPair = (entries(i),entries(j))
        }
      }
    }
    bestPair
  }

  private def pickReferencesMinimizeMaximumRadius(entries: Vector[Entry[RadiusPredicate]]):
                (Entry[RadiusPredicate], Entry[RadiusPredicate]) = {
    var bestRadius = Float.MaxValue
    var bestPair = (entries(0),entries(1))
    for (i <- 0 until entries.length - 1) {
      for (j <- i+1 until entries.length) {
        var radius = Float.MinValue
        for (entry <- entries) {
          val r = math.min(distCalc.calculate(entry.key.reference.exp,entries(i).key.reference.exp).distance +
                           entry.key.radius + entries(i).key.radius,
                           distCalc.calculate(entry.key.reference.exp,entries(j).key.reference.exp).distance +
                           entry.key.radius + entries(j).key.radius)
          if (r > radius) {
            radius = r
          }
        }
        if (radius < bestRadius) {
          bestPair = (entries(i),entries(j))
        }
      }
    }
    bestPair
  }


}

