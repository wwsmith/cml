/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.kernel

import scala.math._

import cml.learn.ibl.distance.Distance

/*********************************************************************************************************************/

class EpanechnikovKernel(scale: Float = 1.0f,
			 kernelWidth: Float = 1.0f,
			 local: Boolean = false) extends Kernel(scale,kernelWidth,local) {
  override def toString(): String = {
    "EpanechnikovKernel\n" +
    "  width: "+kernelWidth+"\n" +
    "  scale: "+scale+"\n" +
    "  local: "+local+"\n"
  }

  override def calculateKernel(distance: Float, distances: Vector[Distance]): Float = {
    val kw = calculateKernelWidth(distances)
    if (distance > 1.0f) {
      0.0f
    } else {
      scale * 3.0f/4.0f * (1 - pow(distance/kw,2)).toFloat
    }
  }
}
