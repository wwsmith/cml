/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.ibl.kernel

import scala.math._

import cml.learn.PredictException
import cml.learn.ibl.distance.Distance

/*********************************************************************************************************************/

abstract class Kernel(val scale: Float,
                      val kernelWidth: Float,
		      val local: Boolean) {
  def calculateKernel(distance: Float, distances: Vector[Distance] = Vector[Distance]()): Float

  protected def calculateKernelWidth(distances: Vector[Distance]): Float = {
    if (local && (distances.size > 0)) {
      val maxDistance = distances.map(d => d.distance).reduce((d1,d2) => max(d1,d2))
      if (maxDistance == 0.0f) {
	Float.MinValue
      } else {
	maxDistance
      }
      // an alternative would be to use the average distance, but some kernel functions are 0 for d > 1.0
      kernelWidth * maxDistance
    } else {
      kernelWidth
    }
  }
}

/*********************************************************************************************************************/
