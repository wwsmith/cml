/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.tree

import scala.util.{Try,Success,Failure}

import cml.learn._

/*********************************************************************************************************************/

class TreeTrainer(name: String, schema: ExperienceSchema, val builder: Builder, val pruneFraction: Float)
    extends Trainer(name,schema) {
  def train(experiences: Vector[Experience]): Try[Predictor] = {
    val rand = new scala.util.Random(394814)  // pick a seed so the partition is repeatable
    val (buildExps,pruneExps) = experiences.partition(exp => rand.nextFloat() > pruneFraction)

    if (buildExps.size < builder.minNodeSize) {
      Failure(new TrainException("%d training experiences, but minimum node size is %d".format(buildExps.size,
        builder.minNodeSize)))
    } else {
      val root = builder.build(buildExps)
      //println(root)
      val prunedRoot = builder.prune(root,pruneExps)
      //println(prunedRoot)

      Success(new TreePredictor(name,schema,prunedRoot))
    }
  }

}

/*********************************************************************************************************************/
