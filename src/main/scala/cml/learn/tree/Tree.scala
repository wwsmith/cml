/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.tree

import scala.util.{Try,Success,Failure}

import com.weiglewilczek.slf4s._

import cml.learn._

/*********************************************************************************************************************/

abstract class Node(val distribution: Distribution,
                    val experiences: Option[Vector[Experience]]) {
  // the distribution is used for prediction in leaf nodes and pruning in internal nodes
  // the experiences are used when building a tree

  def countNodes(): Int
  def predict(query: Query): Try[Distribution]
  //Try[Either[Distribution,???]]
}

class InternalNode(distribution: Distribution,
                   experiences: Option[Vector[Experience]],
                   val children: Vector[(Edge,Node)]) extends Node(distribution,experiences) with Logging {
  def this(distribution: Distribution, children: Vector[(Edge,Node)]) = 
    this(distribution,None,children)
  def this(experiences: Vector[Experience], children: Vector[(Edge,Node)]) = 
    this(Distribution(experiences),Some(experiences),children)

  override def toString: String = {
    children.map({case (edge,node) => edge.toString.split("\n").mkString("  ","\n  ","\n") +
                                      node.toString.split("\n").mkString("    ","\n    ","\n")}).reduce(_ + _)
  }

  def countNodes(): Int = {
    children.map({case (edge,node) => node.countNodes()}).fold(0)(_ + _)
  }

  def predict(query: Query): Try[Distribution] = {
    val satisfy = children.filter({case (edge,node) => edge.satisfies(query)})
    if (satisfy.length != 1) {
      Failure(new PredictException(satisfy.length+" child nodes satisfy"))
    } else {
      satisfy(0)._2.predict(query)
    }
  }
}

class LeafNode(distribution: Distribution,
               experiences: Option[Vector[Experience]]) extends Node(distribution, experiences) {
  def this(distribution: Distribution) = this(distribution,None)
  def this(experiences: Vector[Experience]) = this(Distribution(experiences),Some(experiences))

  override def toString: String = {
    "leaf node with %d experiences and expected value of %f%n".format(distribution.size,distribution.expected)
  }

  override def countNodes(): Int = {
    1
  }

  override def predict(query: Query): Try[Distribution] = {
    Success(distribution)
  }
}

/*********************************************************************************************************************/

abstract class Edge(val feature: Int) extends Logging {
  def satisfies(query: Query): Boolean
}

/*********************************************************************************************************************/

class CategoryEdge(feature: Int, val categories: Set[Symbol]) extends Edge(feature) {
  override def toString: String = {
    "feature %d in: (%s)%n".format(feature,categories.map(sym => sym.name).mkString("",",",""))
  }

  def satisfies(query: Query): Boolean = {
    query(feature) match {
      case Some(v: Symbol) => categories.contains(v)
      case None => false
      case Some(v) => {
        logger.error("expected feature "+feature+" to be a category")
        false
      }
    }
  }
}

/*********************************************************************************************************************/

class BooleanEdge(feature: Int, val bool: Boolean) extends Edge(feature) {
  override def toString: String = {
    "feature %d =  %s%n".format(feature,bool)
  }

  def satisfies(query: Query): Boolean = {
    query(feature) match {
      case Some(v: Boolean) => v == bool
      case None => false
      case Some(v) => {
        logger.error("expected feature "+feature+" to be a boolean")
        false
      }
    }
  }
}

/*********************************************************************************************************************/

class IntEdge(feature: Int, val min: Int, val max: Int) extends Edge(feature) {
  override def toString: String = {
    min match {
      case Int.MinValue => max match {
        case Int.MaxValue => "any feature %d%n".format(feature)
        case v2: Int => "feature %d <= %d%n".format(feature,v2)
      }
      case v1: Int => max match {
        case Int.MaxValue => "%d < feature %d%n".format(v1,feature)
        case v2: Int => "%d < feature %d <= %d%n".format(v1,feature,v2)
      }
    }
  }

  def satisfies(query: Query): Boolean = {
    query(feature) match {
      case Some(v: Int) => if ((min < v) && (v <= max)) {
        true
      } else {
        false
      }
      case None => false
      case Some(v) => {
        logger.error("expected feature "+feature+" to be an int")
        false
      }
    }
  }
}

/*********************************************************************************************************************/

class LongEdge(feature: Int, val min: Long, val max: Long) extends Edge(feature) {
  override def toString: String = {
    min match {
      case Long.MinValue => max match {
        case Long.MaxValue => "any feature %d%n".format(feature)
        case v2: Long => "feature %d <= %d%n".format(feature,v2)
      }
      case v1: Long => max match {
        case Long.MaxValue => "%d < feature %d%n".format(v1,feature)
        case v2: Long => "%d < feature %d <= %d%n".format(v1,feature,v2)
      }
    }
  }

  def satisfies(query: Query): Boolean = {
    query(feature) match {
      case Some(v: Long) => if ((min < v) && (v <= max)) {
        true
      } else {
        false
      }
      case None => false
      case Some(v) => {
        logger.error("expected feature "+feature+" to be a long")
        false
      }
    }
  }
}

/*********************************************************************************************************************/

class FloatEdge(feature: Int, val min: Float, val max: Float) extends Edge(feature) {
  override def toString: String = {
    min match {
      case Float.MinValue => max match {
        case Float.MaxValue => "any feature %d%n".format(feature)
        case v2: Float => "feature %d <= %f%n".format(feature,v2)
      }
      case v1: Float => max match {
        case Float.MaxValue => "%f < feature %d%n".format(v1,feature)
        case v2: Float => "%f < feature %d <= %f%n".format(v1,feature,v2)
      }
    }
  }

  def satisfies(query: Query): Boolean = {
    query(feature) match {
      case Some(v: Float) => if ((min < v) && (v <= max)) {
        true
      } else {
        false
      }
      case None => false
      case Some(v) => {
        logger.error("expected feature "+feature+" to be a float")
        false
      }
    }
  }
}

/*********************************************************************************************************************/

class DoubleEdge(feature: Int, val min: Double, val max: Double) extends Edge(feature) {
  override def toString: String = {
    min match {
      case Double.MinValue => max match {
        case Double.MaxValue => "any feature %d%n".format(feature)
        case v2: Double => "feature %d <= %f%n".format(feature,v2)
      }
      case v1: Double => max match {
        case Double.MaxValue => "%f < feature %d%n".format(v1,feature)
        case v2: Double => "%f < feature %d <= %f%n".format(v1,feature,v2)
      }
    }
  }

  def satisfies(query: Query): Boolean = {
    query(feature) match {
      case Some(v: Double) => if ((min < v) && (v <= max)) {
        true
      } else {
        false
      }
      case None => false
      case Some(v) => {
        logger.error("expected feature "+feature+" to be a double")
        false
      }
    }
  }
}

/*********************************************************************************************************************/

class NoValueEdge(feature: Int) extends Edge(feature) {
  override def toString: String = {
    "feature %d has no value%n".format(feature)
  }

  def satisfies(query: Query): Boolean = {
    query(feature) match {
      case Some(_) => false
      case None => true
    }
  }
}

/*********************************************************************************************************************/
