/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.tree.regression.construct

import cml.learn._
import cml.learn.io._
import cml.learn.tree.regression._

/*********************************************************************************************************************/

class AdaBoost() {
/*
  def construct(experiences: Vector[Experience]): Node = {
    var splits = Vector[Vector[InternalNode]]()
    for(i <- 0 until parent.experiences(0).features.length-1) {
      splits = splits :+ splitExperiences(parent.experiences,i)
    }

    val (entropy, split) = splits.map(split => entropyFromNodes(split)).zip(splits).reduce((es1,es2) =>
      if (es1._1 <= es2._1) {
        es1
      } else {
        es2
      })

    val root = new RootNode(experiences)
    root.children = split
    root
  }
*/
}


/*********************************************************************************************************************/
