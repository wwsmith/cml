/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.tree

import scala.collection.immutable.Queue
import scala.collection.JavaConversions._
import scala.util.{Success,Failure}
import java.util.Properties

import com.weiglewilczek.slf4s._

import cml.learn._
import cml.learn.io._
import cml.learn.tree._

/*********************************************************************************************************************/

// group categories based on similarity of outputs?

// randomized subset of category groupings is another approach

/*********************************************************************************************************************/

class ExhaustiveBinaryCategoryBuilder(loss: Loss,
                                      maxDepth: Int,
                                      minLossFraction: Float,
                                      minNodeSize: Int,
                                      schema: ExperienceSchema)
    extends BinaryCategoryBuilder(loss,maxDepth,minLossFraction,minNodeSize,Int.MaxValue,schema) {
}

/*********************************************************************************************************************/

class BinaryCategoryBuilder(loss: Loss,
                            maxDepth: Int,
                            minLossFraction: Float,
                            minNodeSize: Int,
                            val maxCombinations: Int = 8192,
                            schema: ExperienceSchema)
    extends Builder(loss,maxDepth,minLossFraction,minNodeSize,schema) {

  protected def getSubsets(categories: Set[Symbol],
                           subsets: Vector[Set[Symbol]] = Vector[Set[Symbol]](),
                           size: Int = 1): Vector[Set[Symbol]] = {
    val newSubsets = (subsets ++ categories.subsets(size)).take(maxCombinations)
    if (size >= categories.size / 2) {
      newSubsets
    } else if (subsets.size == maxCombinations) {
      newSubsets
    } else {
      getSubsets(categories,newSubsets,size+1)
    }
  }

  protected def splitCategory(experiences: Vector[Experience],
                              feature: Int): Vector[(Edge,Vector[Experience])] = {
    val (hasValue, noValue) = experiences.partition(exp => exp(feature) match {
      case Some(_) => true
      case None => false
    })

    val categories = hasValue.map(exp => exp(feature) match {
      case Some(s: Symbol) => s
      case _ => throw new PredictException("expected a Symbol for feature "+feature)
    }).toSet
    if (categories.size == 0) {
      throw new PredictException("no categories specified for feature %d".format(feature))
    }
    logger.debug("  %d categories to split%n".format(categories.size))

    val subsets = getSubsets(categories)

    logger.debug("examining %d combinations of categories for feature %d".format(subsets.length,feature))

    val lossSplit = subsets.map(subset => {
      val (in, out) = hasValue.partition(exp => exp(feature) match {
        case Some(v: Symbol) => subset.contains(v)
        case _ => throw new PredictException("expected a Symbol for feature "+feature)
      })
      val lss = (in.length * loss.loss(in) + out.length * loss.loss(out)) / (in.length + out.length)
      var split = Vector[(Edge,Vector[Experience])]()
      split = split :+ (new CategoryEdge(feature,subset),in)
      split = split :+ (new CategoryEdge(feature,categories.diff(subset)),out)
      if (noValue.size > 0) {
        split = split :+ (new NoValueEdge(feature),noValue)
      }
      (lss,split)
    })

    val validLossSplit = lossSplit.filter({case (lss,split) =>
      split.map({case (edge,exps) => exps.size}).min < minNodeSize
    })

    val split = validLossSplit.size match {
      case 0 => Vector[(Edge,Vector[Experience])]()
      case _ => {
        val (minLoss,bestSplit) = lossSplit.minBy({case(lss,split) => lss})
        bestSplit
      }
    }

    if (split.length == 0) {
      logger.debug("split category feature %d with %d categories didn't find a valid split".format(feature,
												   categories.size))
    } else {
      logger.debug("split category feature %d with %d categories (%d and %d)".format(feature,
										     categories.size,
	                                                                             split(0)._2.length,
										     split(1)._2.length))
    }
    split
  }
}

/*********************************************************************************************************************/

class OneNodePerCategoryBuilder(loss: Loss,
                                maxDepth: Int,
                                minLossFraction: Float,
                                minNodeSize: Int,
                                schema: ExperienceSchema)
    extends Builder(loss,maxDepth,minLossFraction,minNodeSize,schema) {

  protected def splitCategory(experiences: Vector[Experience],
                              feature: Int): Vector[(Edge,Vector[Experience])] = {
    val categories = schema(feature) match {
      case cfs: CategoryFeatureSchema => cfs.categories
      case _ => throw new PredictException("expected feature "+feature+" to be categories")
    }
    if (categories.size == 0) {
      throw new PredictException("no categories specified for feature %d".format(feature))
    }

    /* options:
       1) create edges for categories that have experiences (split rejected later if any < minNodeSize)
       2) create edges for categories that there are sufficient experiences in (may not predict whole training set)
       3) create edges for all categories (split rejected later if any < minNodeSize)
    */

    val (hasValue, noValue) = experiences.partition(exp => exp(feature) match {
      case Some(_) => true
      case None => false
    })

    val firstSplit = categories.toVector.map(category => {
      val exps = hasValue.filter(exp => exp(feature) match {
	case Some(v: Symbol) => v == category
        case _ => throw new PredictException("expected a Symbol for feature "+feature)
      })
      (new CategoryEdge(feature,Set(category)),exps)
    })

    // option 1)
    val split = noValue.size match {
      case 0 => firstSplit.filter({case (edge,exps) =>  exps.length > 0})
      case _ => firstSplit.filter({case (edge,exps) =>  exps.length > 0}) :+ (new NoValueEdge(feature),noValue)
    }

    // debugging
    if (split.length < 2) {
      logger.debug("split category feature %d didn't find enough nodes".format(feature))
    } else {
      val minSize = split.map({case (edge,exps) => exps.length}).reduce((l1,l2) => math.min(l1,l2))
      if (minSize < minNodeSize) {
        logger.debug("split category feature %d didn't find big enough nodes".format(feature))
      } else {
        logger.debug("split category feature %d into %d nodes".format(feature,split.size))
      }
    }
    split
  }
}

/*********************************************************************************************************************/

// 10-fold cross-validation is standard
// folds should have the same distribution of the target variable
//   sort by output feature and take every kth?

abstract class Builder(val loss: Loss,
		       val maxDepth: Int,
		       val minLossFraction: Float,
		       val minNodeSize: Int,
                       val schema: ExperienceSchema) extends Logging {

  def construct(experiences: Vector[Experience], pruneFraction: Float = 0.25f): Node = {
    val rand = new scala.util.Random(37914)  // pick a seed so the partition is repeatable
    val (buildExps,pruneExps) = experiences.partition(exp => rand.nextFloat() > pruneFraction)

    val root = build(buildExps)
    val prunedRoot = prune(root,pruneExps)
    prunedRoot
  }

  def build(experiences: Vector[Experience]): Node = {
    logger.debug("building tree from %d experiences".format(experiences.length))
    logger.debug(schema.toString)
    val root = split(experiences,loss.loss(experiences),1)
    logger.info("built tree with %d leaf nodes".format(root.countNodes))
    root
  }

  // prune with a (potentially) different set of experiences than used to build
  def prune(root: Node, experiences: Vector[Experience]): Node = {
    experiences.length match {
      case 0 => root
      case _ => {
        logger.debug("pruning tree using %d experiences".format(experiences.length))
        val prunedRoot = pruneNode(root,experiences)
        logger.info("pruned to %d leaf nodes".format(prunedRoot.countNodes))
        prunedRoot
      }
    }
  }

  protected def pruneNode(node: Node, experiences: Vector[Experience]): Node = {
    if (experiences.length == 0) {
      node
    } else {
      node match {
        case n: LeafNode => n
        case n: InternalNode => {
          val sumError: Double = experiences.map(exp => {
            val pred = node.distribution.expected
            val error: Double = exp.last match {
              case Some(act: Int) => math.abs(act - pred)
              case Some(act: Long) => math.abs(act - pred)
              case Some(act: Float) => math.abs(act - pred)
              case Some(act: Double) => math.abs(act - pred)
              case Some(act) =>  throw new PredictException("can't handle feature of type "+act.getClass.getName)
              case None => 0.0
            }
            error
          }).reduce(_ + _)

          val sumChildError = experiences.map(exp => node.predict(Query(exp)) match {
            case Success(dist) => {
              val v: Double = exp.last match {
                case Some(act: Int) => math.abs(act - dist.expected)
                case Some(act: Long) => math.abs(act - dist.expected)
                case Some(act: Float) => math.abs(act - dist.expected)
                case Some(act: Double) => math.abs(act - dist.expected)
                case Some(act) =>  throw new PredictException("can't handle feature of type "+act.getClass.getName)
                case None => 0.0
              }
              v
            }
            case Failure(e) => 0.0
          }).reduce(_ + _)

          val exps = n.experiences match {
            case Some(exps: Vector[Experience]) => exps
            case None => throw new PredictException("expected node to have experiences")
          }
          if (sumError < sumChildError) {
	    new LeafNode(exps)
	  } else {
            new InternalNode(exps,n.children.map({case (edge,child) =>
              (edge,pruneNode(child,experiences.filter(exp => edge.satisfies(Query(exp)))))}))
	  }
        }
      }
    }
  }

  /*******************************************************************************************************************/

  // not tail recursive, but haven't had stack overflows so far
  private def split(experiences: Vector[Experience], topLoss: Float, depth: Int): Node = {
    val thisLoss = loss.loss(experiences)
    if (thisLoss / topLoss < minLossFraction) {
      logger.debug("node is too specific")
      new LeafNode(experiences)
    } else if (depth >= maxDepth) {
      logger.debug("split reached maximum depth")
      new LeafNode(experiences)
    } else if (experiences.length < 2 * minNodeSize) {
      logger.debug("split reached minimum size")
      new LeafNode(experiences)
    } else {
      logger.debug("split with %d experiences".format(experiences.length))

      var splits = Vector.range(0,experiences(0).length-1).map(i => splitExperiences(experiences,i))
      splits = splits.filter(splt => splt.length > 1)
      splits = splits.filter(splt => splt.map(_._2.length >= minNodeSize).reduce(_ && _))

      if (splits.length == 0) {
	logger.debug("split found no valid splits")
	new LeafNode(experiences)
      } else {
	val (childLoss, splt) = splits.map(splitLoss(_)).zip(splits).reduce((fs1,fs2) =>
	  if (fs1._1 <= fs2._1) {
            fs1
	  } else {
            fs2
	  })

	// keep going with the best split
        val children = splt.map({case (edge,exps) => (edge,split(exps,topLoss,depth+1))})
        new InternalNode(experiences,children)
      }
    }
  }

  private def splitLoss(split: Vector[(Edge,Vector[Experience])]): Float = {
    split.map({case (edge,exps) => exps.length * loss.loss(exps)}).reduce(_ + _) /
    split.map({case (edge,exps) => exps.length}).reduce(_ + _)
  }


  protected def splitExperiences(experiences: Vector[Experience], feature: Int): Vector[(Edge,Vector[Experience])] = {
    schema(feature) match {
      case s: CategoryFeatureSchema => splitCategory(experiences,feature)
      case s: BooleanFeatureSchema => splitBoolean(experiences,feature)
      case s: IntFeatureSchema => splitInt(experiences,feature)
      case s: LongFeatureSchema => splitLong(experiences,feature)
      case s: FloatFeatureSchema => splitFloat(experiences,feature)
      case s: DoubleFeatureSchema => splitDouble(experiences,feature)
      case s => throw new PredictException("can't handle feature of type "+s.getClass.getName)
    }
  }

  protected def splitCategory(experiences: Vector[Experience],
                              feature: Int): Vector[(Edge,Vector[Experience])]

  protected def splitBoolean(experiences: Vector[Experience],
                             feature: Int): Vector[(Edge,Vector[Experience])] = {
    val (hasValue, noValue) = experiences.partition(exp => exp(feature) match {
      case Some(_) => true
      case None => false
    })

    val (t,f) = hasValue.partition(exp => exp(feature) match {
      case Some(v: Boolean) => v
      case _ => throw new PredictException("expected a boolean value for feature "+feature)
    })

    var split = Vector[(Edge,Vector[Experience])]()
    if (t.length >= minNodeSize && f.length >= minNodeSize) {
      split = split :+ (new BooleanEdge(feature,true),t)
      split = split :+ (new BooleanEdge(feature,false),f)
    }
    if (noValue.size > 0) {
      split = split :+ (new NoValueEdge(feature),noValue)
    }
    split
  }

  // vectors returned will both be at least minNodeSize
  //   returns left, right, no value
  protected def numberSplit(experiences: Vector[Experience],
                            feature: Int): Option[(Vector[Experience],Vector[Experience],Vector[Experience])] = {
    val (hasValue, noValue) = experiences.partition(exp => exp(feature) match {
      case Some(_) => true
      case None => false
    })

    val sortedExperiences = hasValue.sortBy(exp => exp(feature) match {
      case Some(v: Int) => v.toDouble
      case Some(v: Long) => v.toDouble
      case Some(v: Float) => v.toDouble
      case Some(v: Double) => v
      case Some(v) => throw new PredictException("expected feature to be a number")
      case None => throw new PredictException("missing value for feature "+schema(feature).name.name)
    })

    val left = loss.incremental()
    for (exp <- sortedExperiences.take(minNodeSize)) {
      left.add(exp)
    }
    val right = loss.incremental()
    for (exp <- sortedExperiences.drop(minNodeSize)) {
      right.add(exp)
    }

    var best = -1.0f
    var bestIndex = -1

    for (i <- minNodeSize until sortedExperiences.length - minNodeSize) {
      // only consider this as a split point if the succeeding value is different
      if (sortedExperiences(i)(feature) != sortedExperiences(i-1)(feature)) {
        //val fitness = left.loss() + right.loss()
	val fitness = (i * left.loss() + (experiences.length-i) * right.loss()) / experiences.length
	if ((bestIndex == -1) || (fitness < best)) {
	  best = fitness
	  bestIndex = i
	}
      }
      right.remove(sortedExperiences(i))
      left.add(sortedExperiences(i))
    }

    if (bestIndex == -1) {
      None
    } else if ((noValue.size > 0) && (noValue.size < minNodeSize)) {
      None
    } else {
      val (leftExps, rightExps) = sortedExperiences.splitAt(bestIndex)
      Some(leftExps,rightExps,noValue)
    }
  }

  protected def splitInt(experiences: Vector[Experience], feature: Int): Vector[(Edge,Vector[Experience])] = {
    numberSplit(experiences,feature) match {
      case None => Vector[(IntEdge,Vector[Experience])]()
      case Some((left,right,neither)) => {
        val splitValue = (left.last(feature),right.head(feature)) match {
          case (Some(v1: Int),Some(v2:Int)) => v1 + (v2-v2)/2
          case (Some(v1),Some(v2)) => throw new PredictException("expected an Int feature")
          case _ => throw new PredictException("missing feature")
        }

        var split = Vector[(Edge,Vector[Experience])]()
        split = split :+ (new IntEdge(feature,Int.MinValue,splitValue),left)
        split = split :+ (new IntEdge(feature,splitValue,Int.MaxValue),right)
        if (neither.size > 0) {
          split = split :+ (new NoValueEdge(feature),neither)
        }
        split
      }
    }
  }

  protected def splitLong(experiences: Vector[Experience], feature: Int): Vector[(Edge,Vector[Experience])] = {
    numberSplit(experiences,feature) match {
      case None => Vector[(LongEdge,Vector[Experience])]()
      case Some((left,right,neither)) => {
        val splitValue = (left.last(feature),right.head(feature)) match {
          case (Some(v1: Long),Some(v2:Long)) => v1 + (v2-v2)/2
          case (Some(v1),Some(v2)) => throw new PredictException("expected a Long feature")
          case _ => throw new PredictException("missing feature")
        }

        var split = Vector[(Edge,Vector[Experience])]()
        split = split :+ (new LongEdge(feature,Long.MinValue,splitValue),left)
        split = split :+ (new LongEdge(feature,splitValue,Long.MaxValue),right)
        if (neither.size > 0) {
          split = split :+ (new NoValueEdge(feature),neither)
        }
        split
      }
    }
  }

  protected def splitFloat(experiences: Vector[Experience], feature: Int): Vector[(Edge,Vector[Experience])] = {
    numberSplit(experiences,feature) match {
      case None => Vector[(FloatEdge,Vector[Experience])]()
      case Some((left,right,neither)) => {
        val splitValue = (left.last(feature),right.head(feature)) match {
          case (Some(v1: Float),Some(v2:Float)) => v1 + (v2-v2)/2
          case (Some(v1),Some(v2)) => throw new PredictException("expected a Float feature")
          case _ => throw new PredictException("missing feature")
        }

        var split = Vector[(Edge,Vector[Experience])]()
        split = split :+ (new FloatEdge(feature,Float.MinValue,splitValue),left)
        split = split :+ (new FloatEdge(feature,splitValue,Float.MaxValue),right)
        if (neither.size > 0) {
          split = split :+ (new NoValueEdge(feature),neither)
        }
        split
      }
    }
  }

  protected def splitDouble(experiences: Vector[Experience], feature: Int): Vector[(Edge,Vector[Experience])] = {
    numberSplit(experiences,feature) match {
      case None => Vector[(FloatEdge,Vector[Experience])]()
      case Some((left,right,neither)) => {
        val splitValue = (left.last(feature),right.head(feature)) match {
          case (Some(v1: Double),Some(v2:Double)) => v1 + (v2-v2)/2
          case (Some(v1),Some(v2)) => throw new PredictException("expected a Double feature")
          case _ => throw new PredictException("missing feature")
        }

        var split = Vector[(Edge,Vector[Experience])]()
        split = split :+ (new DoubleEdge(feature,Double.MinValue,splitValue),left)
        split = split :+ (new DoubleEdge(feature,splitValue,Double.MaxValue),right)
        if (neither.size > 0) {
          split = split :+ (new NoValueEdge(feature),neither)
        }
        split
      }
    }
  }

}

/*********************************************************************************************************************/
