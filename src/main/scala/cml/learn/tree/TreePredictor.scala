/****************************************************************************/
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn.tree

import scala.util.{Try,Success,Failure}

import cml.learn._
import cml.learn.feature.FeatureImportance
import cml.learn.tree._

/*********************************************************************************************************************/

class TreePredictor(name: String,
		    schema: ExperienceSchema,
                    val tree: Node) extends Predictor(name,schema) with FeatureImportance {
  override def toString(): String = {
    val treeStr = "Tree Predictor\n"
    if (tree.countNodes() > 64) {
      treeStr + "   large tree\n"
    } else {
      treeStr + tree.toString().split("\n").mkString("  ","\n  ","\n")
    }
  }

  def importance(): Vector[Float] = {
    normalize(importance(tree,1.0f))
  }

  private def importance(node: cml.learn.tree.Node, weight: Float): Vector[Float] = {
    node match {
      case internal: cml.learn.tree.InternalNode => {
        val imp = Vector.fill(schema.features.length)(0.0f).updated(internal.children(0)._1.feature,weight)
        internal.children.map(child => importance(child._2,weight/2)).fold(imp)((acc,one) =>
          acc.zip(one).map({case (a,b) => a+b}))
      }
      case leaf: cml.learn.tree.LeafNode => Vector.fill(schema.features.length)(0.0f)
    }
  }


  def insert(exp: Experience): Unit = {
  }

  def predict(query: Query): Try[Prediction] = {
    // some errors when predicting are expected, real errors are thrown
    val tryPred = tree.predict(query) match {
      case Success(dist) => {
        val (lower,upper) = dist.confidence(query.levelOfConfidence)
        Success(createPrediction(query,dist.expected,lower,upper))
      }
      case Failure(e) => Failure(e)
    }
    stats.insert(query,tryPred)
    tryPred
  }

  def shutdown(): Unit = {
  }

}

/*********************************************************************************************************************/
