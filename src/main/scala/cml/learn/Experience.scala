/****************************************************************************/
/* Copyright 2015 Warren Smith                                              */
/* Copyright 2015 University of Texas                                       */
/*                                                                          */
/* Licensed under the Apache License, Version 2.0 (the "License");          */
/* you may not use this file except in compliance with the License.         */
/* You may obtain a copy of the License at                                  */
/*                                                                          */
/*     http://www.apache.org/licenses/LICENSE-2.0                           */
/*                                                                          */
/* Unless required by applicable law or agreed to in writing, software      */
/* distributed under the License is distributed on an "AS IS" BASIS,        */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. */
/* See the License for the specific language governing permissions and      */
/* limitations under the License.                                           */
/****************************************************************************/

package cml.learn

/*********************************************************************************************************************/

object Experience {
  def apply(query: Query): Experience = {
    new Experience(query.features)
  }
}

/*********************************************************************************************************************/

class Experience(protected val features: Seq[Option[Any]]) extends Seq[Option[Any]] {

  def iterator: Iterator[Option[Any]] = features.iterator

  def apply(idx: Int): Option[Any] = features.apply(idx)

  def length: Int = features.length

  override def toString(): String = {
    features.map(feature => feature match {
      case Some(v) => v.toString
      case None => "(none)"
    }).mkString("Experience: (",",",")\n")
  }

  def toString(schema: ExperienceSchema): String = {
    "Experience:\n" + 
    schema.features.zip(features).map({case (fschema,value: Option[Any]) => {
      value match {
        case None => fschema.name.name+": (none)"
        case Some(v) => fschema.name.name+": "+v.toString
      }
    }}).mkString("  ","\n  ","\n")
  }

}

/*********************************************************************************************************************/
