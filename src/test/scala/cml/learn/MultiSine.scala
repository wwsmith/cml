
package cml.learn

import java.io._
import scala.util.Random

import cml.learn.ibl._
import cml.learn.ibl.distance._
import cml.learn.ibl.exb._
import cml.learn.io._
import cml.learn.tree.regression._

/*********************************************************************************************************************/

object MultiSine {
  def main(args: Array[String]) = {
    val schema = getSchema()
    val exps = generateWorkload(schema)
    printf("%d experiences%n",exps.length)

    val rand = new Random(54385)
    val rands = exps.map(exp => rand.nextFloat())

    val (train, test) = exps.partition(exp => rand.nextFloat() > 0.25f)

    printf("%d training experiences%n",train.length)
    printf("%d testing experiences%n",test.length)

    writeArff(exps,"multisine.arff")
    writeArff(train,"multisine-train-75.arff")
    writeArff(test,"multisine-test-25.arff")

    writeExperiences(train,"multisine-actual.dat")

    predictKNN(train,test,schema)
    predictTree(train,test,schema)
    predictGradientBoost(train,test,schema)
  }

  def predictKNN(train: Vector[Experience], test: Vector[Experience], schema: ExperienceSchema) = {
    val distanceCalc = new EuclideanDistanceCalculator(schema)
    val expBase = new BasicExperienceBase(distanceCalc)
    val predictor = new NearestNeighborsPredictor("tree",expBase,distanceCalc,5)

    predictor.train(train)

    val predictions = test.map(exp => predictor.predict(exp.toQuery()) match {
      case Left(error) => null
      case Right(pred) => pred
    })
    writePredictions(predictions,"multisine-knn.dat")

    println("NearestNeighborsPredictor:")
    print(predictor.stats.toString)
  }

  def predictTree(train: Vector[Experience], test: Vector[Experience], schema: ExperienceSchema) = {
    val predictor = new TreePredictor("tree",schema,Int.MaxValue,1)
    predictor.train(train)

    val predictions = test.map(exp => predictor.predict(exp.toQuery()) match {
      case Left(error) => null
      case Right(pred) => pred
    })
    writePredictions(predictions,"multisine-rtree.dat")

    println("TreePredictor:")
    print(predictor.stats.toString)
  }

  def predictGradientBoost(train: Vector[Experience], test: Vector[Experience], schema: ExperienceSchema) = {
    val predictor = new GradientBoostPredictor("tree",schema,200,1,1,1.0f)
    predictor.train(train)

    val predictions = test.map(exp => predictor.predict(exp.toQuery()) match {
      case Left(error) => null
      case Right(pred) => {
	pred
      }
    })
    writePredictions(predictions,"multisine-gradient.dat")

    println("GradientBoostPredictor:")
    print(predictor.stats.toString)
  }

  def getSchema(): ExperienceSchema = {
    var schema = new ExperienceSchema()
    schema.features = schema.features :+ new FloatFeatureSchema(Symbol("x"),true)
    schema.features = schema.features :+ new FloatFeatureSchema(Symbol("y"),true)
    schema
  }

  def generateWorkload(schema: ExperienceSchema): Vector[Experience] = {
    var workload = Vector[Experience]()
    //testing for(x <- 0.0 to math.Pi*0.5 by math.Pi/720.0) {
    for(x <- 0.0 to math.Pi*8.0 by math.Pi/90.0) {
      val y = 3.0f + 2.0*math.sin(x) + math.sin(x*2.0)
      val exp = new Experience(schema)
      exp.features = exp.features.updated(0,Some(x.toFloat))
      exp.features = exp.features.updated(1,Some(y.toFloat))
      workload = workload :+ exp
    }
    workload
  }

  private def writeArff(experiences: Vector[Experience], fileName: String): Unit = {
    val writer = new ArffExperienceWriter("multisine","",experiences(0).schema,
					  new BufferedWriter(new FileWriter(fileName)))
    for (exp <- experiences) {
      writer.write(exp)
    }
    writer.close()
  }

  def writeExperiences(experiences: Vector[Experience], fileName: String) = {
    val writer = new BufferedWriter(new FileWriter(fileName))
    for(exp <- experiences) {
      for(feature <- exp.features) {
	feature match {
	  case Some(value) => writer.write(" "+value.toString)
	  case None => writer.write(" none none none")
	}
      }
      writer.newLine()
    }
    writer.close()
  }

  def writePredictions(predictions: Vector[Prediction], fileName: String) = {
    val writer = new BufferedWriter(new FileWriter(fileName))
    for(pred <- predictions) {
      for(feature <- pred.features) {
	feature match {
	  case Some(value) => writer.write(" "+value.toString)
	  case None => writer.write(" none")
	}
      }
      pred.confidence match {
	case Some(conf) => writer.write(" "+conf.lower.toString+" "+conf.upper.toString)
	case None => writer.write(" none none")
      }
      writer.newLine()
    }
    writer.close()
  }

}
