
# execute: scala karnak.learn.MultiSine
# start gnuplot and then execute: load "multisine.gp"

set multiplot layout 1,3 title "MultiSine Test Data"

set title "Nearest Neighbors"

plot "multisine-actual.dat" with lines title "actual", \
     "multisine-knn.dat" with points title "prediction", \
     "multisine-knn.dat" using 1:4 with lines title "upper bound", \
     "multisine-knn.dat" using 1:3 with lines title "lower bound"

set title "Regression Tree"

plot "multisine-actual.dat" with lines title "actual", \
     "multisine-rtree.dat" with points title "prediction", \
     "multisine-rtree.dat" using 1:4 with lines title "upper bound", \
     "multisine-rtree.dat" using 1:3 with lines title "lower bound"

set title "Gradient Boosting"

plot "multisine-actual.dat" with lines title "actual", \
     "multisine-gradient.dat" with points title "prediction", \
     "multisine-gradient.dat" using 1:4 with lines title "upper bound", \
     "multisine-gradient.dat" using 1:3 with lines title "lower bound"

unset multiplot
