
# Carnac Machine Learning Library

This library provides implementations of several machine learning algorithms as well as supporting algorithms
(such as for search). Versions of this library is used as the back end for prediction services deployed in
[XSEDE](https://www.xsede.org) and elsewhere.

## Building

The library can be build using the Scala Build Tool:

    $ sbt compile

## Examples

Example code is located in the src/example/scala/cml/learn directory and can be executed with sbt. For example:

    # sbt "runMain cml.learn.AutosKnn"
